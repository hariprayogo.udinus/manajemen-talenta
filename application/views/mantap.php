<!DOCTYPE html>
<html>
<head>
	<title>Ukuran Keberhasilan/ Indikator Kinerja Individu, Target, dan Perspektif:</title>
</head>
<body>
  <style> 
    .ex1 {
      height: 450px;
      width: 95%;
      overflow-y: scroll;
    }
  </style>
<div class="row">
	<div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-aqua2 animate__animated animate__fadeIn">
      	<img class="flex-container right" src="<?= BASE_ASSET; ?>img/penilaian-bg.png" >
        <h3 class="widget-user-username animate__animated animate__fadeInDown"><font color="#231955"> <b><?= $asn_talenta->nama ?></b></font></h3>
        <h5 class="widget-user-desc animate__animated animate__fadeInUp"><font color="#231955"><b><?= $asn_talenta->jabatan ?></b></font></h5>
        <h4 class="widget-user-desc label label-warning animate__animated animate__fadeIn"><?= $asn_talenta->pangkat.' | '.$asn_talenta->golongan ?></h4>
      </div>
      <div class="widget-user-img">
        <img class="img-circle" src="<?= $asn_talenta->avatar ?>">
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-sm-4">
            <div class="description-block animate__animated animate__fadeIn">
          
              <h3 class="description-header"><h3><i class="fa fa-line-chart"></i> | <b style="color:crimson">KINERJA</b></h3></h3><hr>
              <span  class="badge bg-green-gradient animate__animated animate__flipInX"><h1><b id="nilai-kinerja"><?= $asn_talenta->kinerja ?></b></h1></span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <!-- /.col -->
          <div class="col-sm-4">
            <div class="description-block animate__animated animate__fadeIn">
              <h3 class="description-header"><h3><i class="fa fa-child"></i> | <b style="color:darkviolet">KOMPETENSI</b></h3></h3><hr>
              <span  class="badge bg-green-gradient animate__animated animate__flipInX"><h1><b id="nilai-kompetensi"><?= $asn_talenta->kompetensi ?></b></h1></span>
            </div>
            <!-- /.description-block -->
          </div>
          <div class="col-sm-4">
            <div class="description-block animate__animated animate__fadeIn">
              <h3 class="description-header"><h3><i class="fa fa-certificate"></i> | <b style="color:darkviolet">KUADRAN</b></h3></h3><hr>
              <span  class="badge animate__animated animate__flipInX" style="background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #0070a6), color-stop(1, #00c1ca)) !important;"><h1><b id="nilai-kompetensi"> <?= $asn_talenta->kuadran ?> </b></h1></span>
            </div>
            <!-- /.description-block -->
          </div>

          <?php //echo $GetNip="<span id='id-nip'></span>"; ?>

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <br><hr>
      <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom bg-gray">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_pengembangankompetensi" data-toggle="tab">Pengembangan Kompetensi</a></li>
            <li><a href="#tab_ujikompetensi" data-toggle="tab">Uji Kompetensi</a></li>
            <li><a href="#tab_catpotensi" data-toggle="tab">CAT Potensi</a></li>
            <li><a href="#tab_riwayatjabatan" data-toggle="tab">Riwayat Jabatan</a></li>
            <li><a href="#tab_pendidikan" data-toggle="tab">Pendidikan</a></li>
            
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_pengembangankompetensi">
              <div class="box">
                <div style="overflow-y: scroll;height: 420px;">
                                            <div class="col-sm-12 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="kompetensi">
                                                <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                <?php 
                                                $jml_seminar    = 0;
                                                $jml_diklat     = 0;
                                                
                                                $i=0; foreach ($ws_asn['kompetensi'] as $key => $value) {
                                                     $arr_seminar    = array('Pra Jabatan/Latsar', 'Diklat Kepemimpinan Tk.I/SEPATI', 'Diklat Kepemimpinan Tk.II/SPAMEN/SESPA/SESPANAS', 'Diklat Kepemimpinan Tk.III/SPAMA/SEPADYA', 'Diklat Kepemimpinan Tk.IV/ADUM/SEPALA', 'Diklat Penjenjangan', 'Fungsional', 'Teknis');
                                                     $arr_diklat     = array('Workshop/Lokakarya', 'Seminar', 'Magang', 'Kompetensi', 'Kursus');
                                         
                                                     if (in_array($value->jenis_pengembangan_kompetensi, $arr_diklat)) {
                                                         $jml_seminar    = $jml_seminar+1;
                                                     } else if (in_array($value->jenis_pengembangan_kompetensi, $arr_seminar)) {
                                                         $jml_diklat     = $jml_diklat+1;
                                                     }
                                         
                                                     if ($jml_diklat < 3) {
                                                         $bobot_diklat = round((0.6*7), 2);
                                                     } else if ($jml_diklat > 5) {
                                                         $bobot_diklat = 7;
                                                     } else {
                                                         $bobot_diklat = round((0.8*7), 2);
                                                     }
                                         
                                                     if ($jml_seminar < 5) {
                                                         $bobot_seminar = round((0.6*3), 2);
                                                     } else if ($jml_seminar > 10) {
                                                         $bobot_seminar = 3;
                                                     } else {
                                                         $bobot_seminar = round((0.8*3), 2);
                                                     }

                                                } ?>



                                                    <div class="small-box" style="background-color: #f7d0ed;">
        <div class="inner">
            <h3><?= $jml_diklat ?></h3>
            <p>Diklat</p>
        </div>
        <div class="icon">
            <i class="fa fa-graduation-cap"></i>
        </div>
        <a href="#" class="small-box-footer">
        Jumlah Diklat Diikuti <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div>    
                                                <div class="col-sm-3">
                                                <div class="small-box" style="background-color: #cfbaff;">
        <div class="inner">
            <h3><?= $jml_seminar ?></h3>
            <p>Seminar</p>
        </div>
        <div class="icon">
            <i class="fa fa-star"></i>
        </div>
        <a href="#" class="small-box-footer">
        Jumlah Seminar Diikuti <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div>    
                                                <div class="col-sm-3">
                                                <div class="small-box" style="background-color: #f7d0ed;">
        <div class="inner">
            <h3><?= isset($bobot_diklat) ? $bobot_diklat : 0 ?></h3>
            <p>Bobot Diklat</p>
        </div>
        <div class="icon">
            <i class="fa fa-graduation-cap"></i>
        </div>
        <a href="#" class="small-box-footer">
        Bobot Diklat <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div> 
                                                <div class="col-sm-3">   
                                                <div class="small-box" style="background-color: #cfbaff;">
        <div class="inner">
            <h3><?= isset($bobot_seminar) ? $bobot_seminar : 0 ?></h3>
            <p>Bobot Seminar</p>
        </div>
        <div class="icon">
            <i class="fa fa-star"></i>
        </div>
        <a href="#" class="small-box-footer">
        Bobot Seminar <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                          </div>                    
                        </div>
                        <table class="table animate__animated animate__fadeIn table-bordered table-striped">
											    <thead>
											        <tr>
											            <th>#</th>
											            <th>Jenis</th>
											            <th>Nama</th>
											            <th>Lembaga Penyelenggara</th>
											            <th>Waktu Pelaksanaan</th>
											            <th>Jumlah Jam</th>
											            <th>File Sertifikat</th>
											        </tr>
											    </thead>
											    <tbody>
											        <?php 
											            $jml_seminar    = 0;
											            $jml_diklat     = 0;
											        ?>
											        <?php $i=0; foreach ($ws_asn['kompetensi'] as $key => $value) { $i++;?>
											        <?php
											            $arr_seminar    = array('Pra Jabatan/Latsar', 'Diklat Kepemimpinan Tk.I/SEPATI', 'Diklat Kepemimpinan Tk.II/SPAMEN/SESPA/SESPANAS', 'Diklat Kepemimpinan Tk.III/SPAMA/SEPADYA', 'Diklat Kepemimpinan Tk.IV/ADUM/SEPALA', 'Diklat Penjenjangan', 'Fungsional', 'Teknis');
											            $arr_diklat     = array('Workshop/Lokakarya', 'Seminar', 'Magang', 'Kompetensi', 'Kursus');

											            if (in_array($value->jenis_pengembangan_kompetensi, $arr_diklat)) {
											                $jml_seminar    = $jml_seminar+1;
											            } else if (in_array($value->jenis_pengembangan_kompetensi, $arr_seminar)) {
											                $jml_diklat     = $jml_diklat+1;
											            }

											            if ($jml_diklat < 3) {
											                $bobot_diklat = round((0.6*7), 2);
											            } else if ($jml_diklat > 5) {
											                $bobot_diklat = 7;
											            } else {
											                $bobot_diklat = round((0.8*7), 2);
											            }

											            if ($jml_seminar < 5) {
											                $bobot_seminar = round((0.6*3), 2);
											            } else if ($jml_seminar > 10) {
											                $bobot_seminar = 3;
											            } else {
											                $bobot_seminar = round((0.8*3), 2);
											            }
											        ?>
											        <tr>
											            <td><?= $i ?></td>
											            <td><?= $value->jenis_pengembangan_kompetensi ?></td>
											            <td><?= $value->nama_pengembangan_kompetensi ?></td>
											            <td><?= $value->penyelenggara ?></td>
											            <td><?= date('d M Y', strtotime($value->tanggal_mulai)).'-'.date('d M Y', strtotime($value->tanggal_mulai)) ?></td>
											            <td><?= $value->jumlah_jam ?></td>
											            <td>
											                <a href="<?= $value->kompetensi_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
											            </td>
											        </tr>
											        <?php } ?>
											    </tbody>
											</table>


                    </div>
                      
                  </div><br>
                                            
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab_pendidikan">
              <div class="box">
                <!-- /.box-header -->
                <div class="box-body animate__animated animate__fadeInUp animate__faster" style="overflow-y: scroll;height: 200px;">
                	<div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
		                <table class="table table-bordered animate__animated animate__fadeIn table-bordered table-striped">
	                      <thead>
	                          <tr>
	                              <th>#</th>
	                              <th>Jenjang Pendidikan</th>
	                              <th>Almamater</th>
	                              <th>Ijazah</th>
	                              <th>Transkrip Nilai</th>
	                          </tr>
	                      </thead>
	                      <tbody>
	                          <?php $i=0; foreach ($ws_asn['pendidikan'] as $key => $value) { $i++;?>
	                          <tr>
	                              <td><?= $i ?></td>
	                              <td><span class="label label-info"><?= $value->tingkat_pendidikan ?></span>&nbsp;<?= $value->pendidikan ?></td>
	                              <td><?= $value->nama_sekolah ?></td>
	                              <td>
	                                  <a href="<?= $value->file_ijazah ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
	                              </td>
	                              <td>
	                                  <a href="<?= $value->file_transkrip ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
	                              </td>
	                          </tr>
	                          <?php } ?>
	                      </tbody>
	                  </table>
	                </div><br>
                  <div class="col-sm-4">
                          <div class="small-box" style="background-color: #cfbaff;">
                              <div class="inner">
                                  <?php  
                                  	$riwayat_pendidikan   = $ws_asn['pendidikan'];
                                 
                                      $jenjang_terakhir   = $riwayat_pendidikan[array_key_last($riwayat_pendidikan)]->tingkat_pendidikan;
                                     
                                      if ($jenjang_terakhir == 'S-3') {
                                          $nilai_pendidikan  = 100;
                                      } elseif ($jenjang_terakhir == 'S-2') {
                                          $nilai_pendidikan  = 90;
                                      } elseif ($jenjang_terakhir == 'S-1') {
                                          $nilai_pendidikan  = 80;
                                      } elseif ($jenjang_terakhir == 'Diploma IV') {
                                          $nilai_pendidikan  = 80;
                                      } elseif ($jenjang_terakhir == 'Diploma III') {
                                          $nilai_pendidikan  = 70;
                                      } else {
                                          $nilai_pendidikan  = 50;
                                      }
                                  
                                      $bobot_pendidikan    = (15/100)*$nilai_pendidikan;
                                  ?>
                                  <h3><?= $nilai_pendidikan ?></h3>
                                  <p>Jenjang Pendidikan</p>
                              </div>
                              <div class="icon">
                                  <i class="fa fa-graduation-cap"></i>
                              </div>
                              <a href="#" class="small-box-footer">
                              Nilai Jenjang Pendidikan ASN <i class="fa fa-arrow-circle-left"></i>
                              </a>
                          </div>
                          <div class="small-box" style="background-color: #f7d0ed;">
                              <div class="inner">
                                  <h3><?= $bobot_pendidikan ?> %</h3>
                                  <p>Bobot</p>
                              </div>
                              <div class="icon">
                                  <i class="fa fa-star"></i>
                              </div>
                              <a href="#" class="small-box-footer">
                              Hasil Bobot Nilai Pendidikan <i class="fa fa-arrow-circle-left"></i>
                              </a>
                          </div>

                      </div>
                </div>
                <!-- /.box-body -->
              </div>
            </div>

            <div class="tab-pane" id="tab_riwayatjabatan">
              <div class="box">
                <!-- /.box-header -->
                <div class="box-body animate__animated animate__fadeInUp animate__faster" style="overflow-y: scroll;height: 200px;">
                	<div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                    <div class="box-body" id="riwayatjabatan">
                    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Jabatan</th>
                                    <th>TMT SK</th>
                                    <th>Lokasi Kerja</th>
                                    <th>File SK</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($ws_asn['jabatan'] as $key => $value) { $i++;?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $value->jabatan_baru ?></td>
                                    <td><?= date('d M Y', strtotime($value->tmt_sk)) ?></td>
                                    <td><?= $value->unit_kerja_baru ?></td>
                                    <td>
                                        <a href="<?= $value->file_jabatan ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                    
                </div><br><br>
                  <div class="col-sm-4">
                    <div class="small-box" style="background-color: #f7d0ed;">
	                      <div class="inner">
	                          <h3><?= $asn_talenta->kelas_jabatan ?></h3>
	                          <p>Kelas Jabatan</p>
	                      </div>
	                      <div class="icon">
	                          <i class="fa fa-graduation-cap"></i>
	                      </div>
	                      <a href="#" class="small-box-footer">
	                      Nilai Kelas Jabatan <i class="fa fa-arrow-circle-left"></i>
	                      </a>
	                  </div>
                    <div class="small-box" style="background-color: #cfbaff;">
                        <div class="inner">
                            <h3><?= $asn_talenta->nilai_jabatan ?></h3>
                            <p>Nilai Jabatan</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-star"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                        Nilai Jabatan <i class="fa fa-arrow-circle-left"></i>
                        </a>
                    </div>
                    <div class="small-box" style="background-color: #cfbaff;">
                        <div class="inner">
                            <?php $bobot_jabatan    = round(($asn_talenta->nilai_jabatan/3555)*15, 2); ?>
                            <h3><?= $bobot_jabatan.' %'; ?></h3>
                            <p>Bobot</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-star"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                        Bobot Nilai Kelas Jabatan <i class="fa fa-arrow-circle-left"></i>
                        </a>
                    </div>

                  </div>
                </div>
                <!-- /.box-body -->
              </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_ujikompetensi">
                        <!-- BAR CHART -->
					    <div class="box box-success">
					        <div class="box-header with-border">
					          <h3 class="box-title"><b>Penilaian Uji Kompetensi ASN </b></h3>
					          <div class="row">
									    <div class="animate__animated animate__fadeInUp animate__faster col-lg-6 col-xs-6" id="kemampuanberpikir">
									      <!-- small box -->
									      <div class="small-box bg-aqua">
									        <div class="inner">
									          <h3 style="font-size: 50px"><?= ($penilaian) ? (($penilaian->klasifikasi_uji_potensi != null) ? $penilaian->klasifikasi_uji_potensi : 'Belum ada penilaian') : 'Belum ada penilaian' ?></h3>
									          <p>Klasifikasi CAT Potensi <i class="fa fa-arrow-circle-left"></i></p>
									        </div>
									        <div class="icon">
									          <i class="fa fa-comments"></i>
									        </div>
									        <a class="small-box-footer">
									        </a>
									      </div>
									    </div>
									    <!-- ./col -->
									    <div class="animate__animated animate__fadeInUp animate__fast col-lg-6 col-xs-6">
									      <!-- small box -->
									      <div class="small-box bg-green">
									        <div class="inner">
									          <h3 style="font-size: 50px"><?= ($penilaian) ? (($penilaian->nilai_akhir_uji_kompetensi != null) ? $penilaian->nilai_akhir_uji_kompetensi : 'Belum ada penilaian') : 'Belum ada penilaian' ?></h3>

									          <p>Nilai Akhir CAT Potensi <i class="fa fa-arrow-circle-left"></i></p>
									        </div>
									        <div class="icon">
									          <i class="fa fa-search"></i>
									        </div>
									        <a class="small-box-footer">
									      </a>
									      </div>
									    </div>
										</div>
					          
					        </div>
					        <!-- /.box-body -->
					    </div>
            </div>
            <div class="tab-pane" id="tab_catpotensi">
            	<div class="row">
						    <div class="animate__animated animate__fadeInUp animate__faster col-lg-6 col-xs-6" id="kemampuanberpikir">
						      <!-- small box -->
						      <div class="small-box bg-aqua">
						        <div class="inner">
						          <h3 style="font-size: 50px"><?= ($penilaian) ? (($penilaian->klasifikasi_cat_potensi != null) ? $penilaian->klasifikasi_cat_potensi : 'Belum ada penilaian') : 'Belum ada penilaian' ?></h3>
						          <p>Klasifikasi CAT Potensi <i class="fa fa-arrow-circle-left"></i></p>
						        </div>
						        <div class="icon">
						          <i class="fa fa-comments"></i>
						        </div>
						        <a class="small-box-footer">
						        </a>
						      </div>
						    </div>
						    <!-- ./col -->
						    <div class="animate__animated animate__fadeInUp animate__fast col-lg-6 col-xs-6">
						      <!-- small box -->
						      <div class="small-box bg-green">
						        <div class="inner">
						          <h3 style="font-size: 50px"><?= ($penilaian) ? (($penilaian->nilai_akhir_individu != null) ? $penilaian->nilai_akhir_individu : 'Belum ada penilaian') : 'Belum ada penilaian' ?></h3>

						          <p>Nilai Akhir CAT Potensi <i class="fa fa-arrow-circle-left"></i></p>
						        </div>
						        <div class="icon">
						          <i class="fa fa-search"></i>
						        </div>
						        <a class="small-box-footer">
						      </a>
						      </div>
						    </div>
							</div>
						</div>
		  </div>
		</div>
	  </div>
	</div>
</div>

	</body>
</html>
