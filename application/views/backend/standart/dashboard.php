<link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style type="text/css">
#container {
    height: 402px;
}

.highcharts-figure,
.highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}

.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}

.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
    padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}

.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
.dataTables_paginate.paging_simple_numbers, .dataTables_paginate .pagination {
    margin-top: 0px !important;
    float: right;
}
.table {
  border-spacing: 0 0.85rem !important;
}

.table .dropdown {
  display: inline-block;
}

.table td,
.table th {
  vertical-align: middle;
  margin-bottom: 10px;
  border: none;
}

.table thead tr,
.table thead th {
  border: none;
  /*font-size: 12px;*/
  letter-spacing: 1px;
  text-transform: uppercase;
  background: transparent;
}

.table td {
  vertical-align: middle;
  background: #fff;
}

/*.table td:first-child {
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
}

.table td:last-child {
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
}*/

.avatar {
  width: 2.75rem;
  height: 2.75rem;
  line-height: 3rem;
  border-radius: 50%;
  display: inline-block;
  background: transparent;
  position: relative;
  text-align: center;
  color: #868e96;
  font-weight: 700;
  vertical-align: bottom;
  /*font-size: 1rem;*/
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.avatar-sm {
  width: 2.5rem;
  height: 2.5rem;
  /*font-size: 0.83333rem;*/
  line-height: 1.5;
}

.avatar-img {
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
  object-fit: cover;
}

.avatar-blue {
  background-color: #c8d9f1;
  color: #467fcf;
}

table.dataTable.dtr-inline.collapsed
  > tbody
  > tr[role="row"]
  > td:first-child:before,
table.dataTable.dtr-inline.collapsed
  > tbody
  > tr[role="row"]
  > th:first-child:before {
  top: 28px;
  left: 14px;
  border: none;
  box-shadow: none;
}

table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child,
table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > th:first-child {
  padding-left: 48px;
}

table.dataTable > tbody > tr.child ul.dtr-details {
  width: 100%;
}

table.dataTable > tbody > tr.child span.dtr-title {
  min-width: 50%;
}

table.dataTable.dtr-inline.collapsed > tbody > tr > td.child,
table.dataTable.dtr-inline.collapsed > tbody > tr > th.child,
table.dataTable.dtr-inline.collapsed > tbody > tr > td.dataTables_empty {
  padding: 0.75rem 1rem 0.125rem;
}

div.dataTables_wrapper div.dataTables_length label,
div.dataTables_wrapper div.dataTables_filter label {
  margin-bottom: 0;
}

@media (max-width: 767px) {
  div.dataTables_wrapper div.dataTables_paginate ul.pagination {
    -ms-flex-pack: center !important;
    justify-content: center !important;
    margin-top: 1rem;
  }
}

.btn-icon {
  background: #fff;
}
.btn-icon .bx {
  /*font-size: 20px;*/
}

.btn .bx {
  vertical-align: middle;
  /*font-size: 24px;*/
}

.dropdown-menu {
  padding: 0.25rem 0;
}

.dropdown-item {
  padding: 0.5rem 1rem;
}

.badge {
  padding: 0.5em 0.75em;
}

.badge-success-alt {
  background-color: #607EAA;
  color: #F9F5EB;
}

.table a {
  color: white;
}

.table a:hover,
.table a:focus {
  text-decoration: none;
}

table.dataTable {
  margin-top: 12px !important;
}

.icon > .bx {
  display: block;
  min-width: 1.5em;
  min-height: 1.5em;
  text-align: center;
  /*font-size: 1.0625rem;*/
}

.btn {
  /*font-size: 0.9375rem;*/
  font-weight: 500;
  padding: 0.5rem 0.75rem;
}

.avatar-blue {
    background-color: white;
    color: #467fcf;
  }

  .avatar-pink {
    background-color: white;
    color: #f66d9b;
  }
</style>
<section class="content-header">
    <h1>
        <?= cclang('dashboard') ?>
        <small>
            
        <?= cclang('control_panel') ?>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard">
                </i>
                <?= cclang('home') ?>
            </a>
        </li>
        <li class="active">
            <?= cclang('dashboard') ?>
        </li>
    </ol>
</section>

<section class="content">
    <div class="row" >
     
      <div class="col-md-6">
         <div class="box">
            <div class="box-body">
                  <div class="box-warning">
                    <figure class="highcharts-figure">
                        <div id="container"></div>
                        <!-- <p class="highcharts-description">
                            Chart showing basic use of 3D cylindrical columns. A 3D cylinder chart
                            is similar to a 3D column chart, with a different shape. The chart
                            visualizes confirmed COVID-19 cases for women in Norway.
                        </p> -->
                    </figure>
                  </div>
            </div>
            <!--/box body -->
         </div>
       </div>
       <div class="col-md-6">
         <div class="box">
            <div class="box-body">
                  <div class="box-warning" style="height:430px;overflow-y: scroll;">
                    <table id="example" class="display nowrap" style="width:100%">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>OPD</th>
                              <th>Jumlah ASN &amp; sebaran kuadran</th>
                              <th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $i=0;foreach ($grup_opd as $key => $value) { $i++; ?>
                        <?php if($i%2 == 1) { ?>
                         <tr style="background-color: #f5e9f2;">
                            <td style="background-color:#ffd4f4;vertical-align: middle;">
                              <a href="#">
                                <div class="d-flex align-items-center">
                                  <div class="avatar avatar-blue mr-3" style="color:#ffd4f4"><?= $i ?></div>
                                </div>
                              </a>
                            </td>
                            <td style="background-color: transparent;vertical-align: middle;">
                             <div class="badge badge-success badge-success-alt"><?= $value->opd ?></div>
                            </td>
                            <td style="background-color: transparent;vertical-align: middle;">
                             <div class="badge badge-success badge-success-alt" style="background:#9FE7F5;color: #053F5C;"><?= $value->jml ?></div>
                              <?php foreach ($value->kuadran as $k => $val) { ?>
                                <div class="badge badge-success badge-success-alt"><?= $val->kuadran ?> : <?= $val->jml ?> ASN</div>
                              <?php } ?>
                            </td>
                        <?php } else { ?>
                         <tr style="background-color: #e0d2ff;">
                         <td style="background-color:#b99cf9;vertical-align: middle;">
                           <a href="#">
                             <div class="d-flex align-items-center">
                               <div class="avatar avatar-blue mr-3" style="color:#b99cf9"><?= $i ?></div>
                             </div>
                           </a>
                         </td>
                         <td style="background-color: transparent;vertical-align: middle;">
                           <div class="badge badge-success badge-success-alt"><?= $value->opd ?></div>
                         </td>
                         <td style="background-color: transparent;vertical-align: middle;">
                           <div class="badge badge-success badge-success-alt" style="background:#9FE7F5;color: #053F5C;"><?= $value->jml ?></div>
                            <?php foreach ($value->kuadran as $k => $val) { ?>
                              <div class="badge badge-success badge-success-alt"><?= $val->kuadran ?> : <?= $val->jml ?> ASN</div>
                            <?php } ?>
                          </td>
                        <?php } ?>
                         <td style="background-color:white;color: white; vertical-align: middle;">
                           <a href="<?= base_url('administrator/dashboard/opd/').str_replace(',', 'hesoyam', $value->opd) ?>" class="kualitatif btn" style="background-color: #1C3879;color: #F9F5EB;" data-toggle="tooltip" data-placement="top" title="List Pegawai" id=""><i class="fa fa-info"></i></a>
                         </td>
                           
                        </tr>
                      <?php } ?>
                      </tbody>
                  </table>
                  </div>
            </div>
            <!--/box body -->
         </div>
       </div>
     </div>
</section>
<!-- /.content -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/cylinder.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
Highcharts.chart('container2', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'Click points to go to URL'
    },

    xAxis: {
        type: 'category'
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        location.href = 'https://en.wikipedia.org/wiki/' +
                            this.options.key;
                    }
                }
            }
        }
    },

    series: [{
        data: [{
            y: 29.9,
            name: 'USA',
            key: 'United_States'
        }, {
            y: 71.5,
            name: 'Canada',
            key: 'Canada'
        }, {
            y: 106.4,
            name: 'Mexico',
            key: 'Mexico'
        }]
    }]
});
</script>
<script type="text/javascript">
  Highcharts.chart('container', {
    chart: {
        type: 'cylinder',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    title: {
        text: 'Sebaran Talenta Berdasarkan Eselon'
    },
    // subtitle: {
    //     text: 'Source: ' +
    //         '<a href="https://www.fhi.no/en/id/infectious-diseases/coronavirus/daily-reports/daily-reports-COVID19/"' +
    //         'target="_blank">FHI</a>'
    // },
    xAxis: {
        categories: <?= json_encode($cat_eselon) ?>,
        title: {
            text: 'Eselon'
        }
    },
    yAxis: {
        title: {
            // margin: 20,
            text: 'Jumlah ASN'
        }
    },
    tooltip: {
        headerFormat: '<b>Eselon: {point.x}</b><br>'
    },
    plotOptions: {
        series: {
            depth: 25,
            colorByPoint: true,
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        location.href = '<?= base_url('administrator/dashboard/eselon/') ?>' +
                            this.options.key;
                    }
                }
            }
        }
    },
    series: [{
        data: <?= json_encode($count_eselon) ?>,
        name: 'Pegawai',
        showInLegend: false
    }]
});
</script>