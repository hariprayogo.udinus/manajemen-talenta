<style type="text/css">
   .widget-user-header {
      padding-left: 20px !important;
   }

   
   .widget-user .widget-user-img {
  position: absolute;
  top: 10px;
  left: 50%;
  margin-left: -75px;
}

@media (min-width: 992px) {
.widget-user .widget-user-img > img {
  width: 155px;
  height: 160px;
  border: 2.5px solid #e9e9e9;
  }
}

@media (max-width: 992px) {
.widget-user .widget-user-img > img {
  position: absolute;
  top: 2px;
  left: 50%;
  margin-left: 170px;
  width: 100px;
  height: 100px;
  border: 1.5px solid #e9e9e9;
  }
}

@media (min-width: 992px) {
   .flex-container.right {
  display: flex;
  flex-wrap: wrap-reverse;
  position: absolute;
  right: 10px;
  top: 30px;
  width: 10%;
  height: 31%;
  }
}

@media (max-width: 992px) {
   .flex-container.right {
  display: nonw;
  flex-wrap: wrap-reverse;
  position: absolute;
  right: 10px;
  top: 30px;
  width: 0%;
  height: 0%;
  }
}

@media (min-width: 992px) {
.bg-aqua2 {
  background-color: #e5e9f4 !important;
  background-image: url("<?= BASE_ASSET;?>/img/banner-detail-peg.svg");
  background-repeat: no-repeat;
  background-size: 100% 100%;
  }
}

@media (max-width: 992px) {
.bg-aqua2 {
  background-color: #e5e9f4 !important;
  /* background-image: url("<?= BASE_ASSET;?>/img/banner-detail-peg.svg"); */
  background-repeat: no-repeat;
  background-size: 0% 0%;
  }
}

@media (min-width: 992px) {
.modal-ku {
  width: 1200px;
  hight: 1500px;
  margin: auto;
  }
}


</style>


<style>
    #img {
    position: absolute;
    z-index: -1/* Draw image behind the canvas */

  }

  @media (min-width: 800px) {
  #myChart {
    height: 800px;
    }
  }

  @media (max-width: 800px) {
  #myChart {
    height: 400px;
    }
  }

  .modal-body {
  height:90%;
  overflow:auto;
  }

  hr {
    border: none;
    height: 0.5px;
    /* Set the hr color */
    color: rgb(198, 198, 198); /* old IE */
    background-color: rgb(214, 214, 214); /* Modern Browsers */
  }
    

  </style>

<link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.css">

<section class="content-header">
    <h1>
        <?= cclang('dashboard') ?>
        <small>
            
        <?= cclang('control_panel') ?>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard">
                </i>
                <?= cclang('home') ?>
            </a>
        </li>
        <li class="active">
            <?= cclang('dashboard') ?>
        </li>
    </ol>
</section>

<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body">
                  <div class="col-md-12">
                    
                      <canvas id="myChart"></canvas>
                    
                  </div>
            </div>
            <!--/box body -->
         </div>
         <div class="modal fade" id="myModal" class="modalku" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog modal-lg" role="document" style="width: 90%;height: 85% !important;">
       <div class="modal-content" style="height:100% !important">
         <div class="modal-header" style="background-color: transparent;background-image: linear-gradient(to right, #f9c1eb 0, #a38ed2 75%);    background-repeat: repeat-x;">
           <button type="button" class="close btn btn-primary" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
           <h4 class="modal-title" id="myModalLabel">ASN Talenta</h4>
         </div>
         <!-- memulai untuk konten dinamis -->
         <!-- lihat id="data_siswa", ini yang di pangging pada ajax di bawah -->
         <div class="modal-body" id="data_siswa">
         </div>
         <!-- selesai konten dinamis -->
       </div>
     </div>
   </div>
         <div class="modal fade" id="modal-pegawai-detail" data-keyboard="false" data-backdrop="static">
    
   <div class="modal-dialog modal-ku">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"><b>Detail Data Pegawai |</b></h4>
      </div>
      <div class="modal-body bg-gray-light">
        <div class="box box-widget widget-user">
          <!-- Add the bg color to the header using any of the bg-* classes -->
          <div class="widget-user-header bg-aqua2 animate__animated animate__fadeIn">
          <img class="flex-container right" src="<?= BASE_ASSET; ?>img/penilaian-bg.png" >
            <h3 class="widget-user-username animate__animated animate__fadeInDown"><font color="#231955"> <b>Adnan Asnawi, S.Kom</b></font></h3>
            <h5 class="widget-user-desc animate__animated animate__fadeInUp"><font color="#231955"><b>Pranata Komputer Ahli Pertama</b></font></h5>
            <h4 class="widget-user-desc label label-warning animate__animated animate__fadeIn">III A/ Penata Muda</h4>
          </div>
          <div class="widget-user-img">
            <img class="img-circle" src="https://sisdm.semarangkota.go.id/foto/20216_foto_usulan_1652230669.jpg">
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-6">
                <div class="description-block animate__animated animate__fadeIn">
              
                  <h3 class="description-header"><h3><i class="fa fa-line-chart"></i> | <b style="color:crimson">KINERJA</b></h3></h3><hr>
                  <span  class="badge bg-green-gradient animate__animated animate__flipInX"><h1><b id="nilai-kinerja"></b></h1></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <!-- /.col -->
              <div class="col-sm-6">
                <div class="description-block animate__animated animate__fadeIn">
                  <h3 class="description-header"><h3><i class="fa fa-child"></i> | <b style="color:darkviolet">KOMPETENSI</b></h3></h3><hr>
                  <span  class="badge bg-green-gradient animate__animated animate__flipInX"><h1><b id="nilai-kompetensi"></b></h1></span>
                </div>
                <!-- /.description-block -->
              </div>

              <?php //echo $GetNip="<span id='id-nip'></span>"; ?>

              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <br><hr>
          <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom bg-gray">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_pengembangankompetensi" data-toggle="tab">Pengembangan Kompetensi</a></li>
                <li><a href="#tab_ujikompetensi" data-toggle="tab">Uji Kompetensi</a></li>
                <li><a href="#tab_catpotensi" data-toggle="tab">CAT Potensi</a></li>
                <li><a href="#tab_riwayatjabatan" data-toggle="tab">Riwayat Jabatan</a></li>
                <li><a href="#tab_pendidikan" data-toggle="tab">Pendidikan</a></li>
                
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_pengembangankompetensi">
                  <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body animate__animated animate__fadeInUp animate__faster">
                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Jenis</th>
                                                <th>Lembaga Penyelenggara</th>
                                                <th>Waktu Pelaksanaan</th>
                                                <th>Jumlah Menit</th>
                                                <th>File Sertifikat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Diklatsar CPNS</td>
                                                <td>BKPP Kota Semarang</td>
                                                <td>November 2019</td>
                                                <td>3000 menit</td>
                                                <td><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="File Sertifikat" style="max-height: 50px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_ujikompetensi">
                            <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Penilaian Uji Kompetensi ASN </b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart animate__animated animate__fadeInUp animate__faster">
                <canvas id="ujiKomChart" ></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      
                </div>
                <div class="tab-pane" id="tab_catpotensi">
                <div class="row">
        <div class="animate__animated animate__fadeInUp animate__faster col-lg-3 col-xs-6" id="kemampuanberpikir">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 style="font-size: 50px">85</h3>
              <p>Kemampuan Berpikir <i class="fa fa-arrow-circle-left"></i></p>
            </div>
            <div class="icon">
              <i class="fa fa-comments"></i>
            </div>
            <a class="small-box-footer">
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="animate__animated animate__fadeInUp animate__fast col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 style="font-size: 50px">91</h3>

              <p>Nilai Sikap Kerja <i class="fa fa-arrow-circle-left"></i></p>
            </div>
            <div class="icon">
              <i class="fa fa-search"></i>
            </div>
            <a class="small-box-footer">
          </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="animate__animated animate__fadeInUp animate__faster animate__delay-0.5s col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 style="font-size: 50px">92</h3>

              <p>Nilai Kepribadian <i class="fa fa-arrow-circle-left"></i></p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a class="small-box-footer">
              
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="animate__animated animate__fadeInUp animate__fast col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 style="font-size: 50px">89</h3>

              <p>Nilai Akhir Individu <i class="fa fa-arrow-circle-left"></i></p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a class="small-box-footer">
           
            </a>
          </div>
        </div>
        <!-- ./col -->
      </div>
                </div>
                
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_riwayatjabatan">
                <table class="table table-bordered table-striped animate__animated animate__fadeInUp animate__faster">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Jabatan</th>
                                                <th>OPD</th>
                                                <th>Unit Kerja</th>
                                                <th>Lokasi Kerja</th>
                                                <th>File SK</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Pranata Komputer Ahli Pertama</td>
                                                <td>Dinas Komunikasi, Informatika, Statistik, dan Persandian</td>
                                                <td>Dinas Komunikasi, Informatika, Statistik, dan Persandian</td>
                                                <td>Dinas Komunikasi, Informatika, Statistik, dan Persandian</td>
                                                <td><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="File SK" style="max-height: 50px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                </div>

                <div class="tab-pane" id="tab_pendidikan">
                <table class="table table-bordered table-striped animate__animated animate__fadeInUp animate__faster">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Jenis</th>
                                                <th>Lembaga Penyelenggara</th>
                                                <th>Waktu Pelaksanaan</th>
                                                <th>Jumlah Menit</th>
                                                <th>File Sertifikat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Diklatsar CPNS</td>
                                                <td>BKPP Kota Semarang</td>
                                                <td>November 2019</td>
                                                <td>3000 menit</td>
                                                <td><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="File Sertifikat" style="max-height: 50px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
      </div>
      </div>
      
      </div>
      
      </div>
      </div>
    
         <!--/box -->

      </div>
      <?php
        // $groups = $this->session->userdata('groups');

        // if (in_array("1", $groups) OR in_array("16", $groups)){

      ?>
      <div class="col-md-6">
      </div>
   </div>
</section>
<!-- /.content -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<script>

var xyValues = [
<?php foreach ($asn as $key => $value) {
  echo '{x:'.$value->kompetensi.', y:'.$value->kinerja.', opd:"'.$value->opd.'", id_asn_talenta:'.$value->id_asn_talenta.', nama: "'.$value->nama.'"},';
} ?>

];


    const myChart = new Chart('myChart', {
  type: 'scatter',
  plugins: [{
    beforeDraw: chart => {
      var ctx = chart.ctx;
      ctx.save();
      const image = new Image();
      image.src = '<?= BASE_ASSET; ?>img/NineBox.png';
      ctx.drawImage(image, chart.chartArea.left, chart.chartArea.top, chart.chartArea.width, chart.chartArea.height);
      ctx.restore();
    }
  }],
    data: {
    datasets: [{
      label: 'Data Pegawai / ASN',
      pointRadius: 4,
      pointBackgroundColor: "rgb(0, 62, 110)",
      data: xyValues
    }]
  },


  options: {
    responsive: true,
    maintainAspectRatio : false,
    onClick:detailpegawai,
    plugins: {
            title: {
                display: true,
                font: {
                        size: 17,
                        weight: 'normal',
                        family: 'Verdana, Arial, Helvetica, sans-serif'
                    },
                text: 'Sebaran Pegawai Masuk Kuadran 9 (Nine Boxs)'
            },
            tooltip: {
                callbacks: {
                    label: function(context) {
                        let label = context.raw.nama || '';

                        if (label) {
                            label += ' ';
                        }
                        if (context.parsed.y !== null) {
                            label += '('+context.raw.opd+') : '+context.raw.x+', '+context.raw.y;
                        }
                        return label;
                    },
                     labelColor: function(context) {
                        return {
                            borderColor: 'rgb(0, 0, 255)',
                            backgroundColor: '#A8D1E7',
                            borderWidth: 2,
                            borderDash: [2, 2],
                            borderRadius: 2,
                        };
                    },
                    labelTextColor: function(context) {
                        return '#FEE5E0';
                    }
                }
            }
        },
    
    scales: {
      x: {
        title: {
          display: true,
          font: {
                        size: 15,
                        weight: 'Bold',
                        family: 'Verdana, Arial, Helvetica, sans-serif'
                    },
          text: 'KOMPETENSI'
        },
        min: 40,
        max: 100,
        ticks: {
          display: true,
          // forces step size to be 50 units
          stepSize: 20
        }
      },
      y: {
        title: {
          display: true,
          font: {
                        size: 15,
                        weight: 'Bold',
                        family: 'Verdana, Arial, Helvetica, sans-serif'
                    },
          text: 'KINERJA'
        },
        min: 40,
        max: 100,
        ticks: {
          display: true,
          // forces step size to be 50 units
          stepSize: 20
        }
      }
    }
  }
});

var densityData = {
  label: 'Hasil Uji Kompetensi',
  backgroundColor: ["#4895ef", "#4361ee", "#3f37c9", "#3a0ca3", "#480ca9", "#560bad", "#7209b7", "#b5179e", "#f72585"],
  data: [8, 8.5, 9, 7.8, 8.9, 7.7, 9.5, 8.1, 8.3 ]
};



const ujiKomChart = new Chart('ujiKomChart', {
  plugins: [ChartDataLabels],
  options: {
    plugins: {
      // Change options for ALL labels of THIS CHART
      datalabels: {
        color: '#fafcfc'
      }
    }
  },
  type: 'bar',
  
  data: {
    labels: ["Integritas", "Kerjasama", "Komunikasi Lisan", "Orientasi pada Hasil", "Pelayanan Publik", "Pengembangan Orang Lain", "Mengelola Perubahan", "Pengambilan Keputusan","Perekat Bangsa"],
    datasets: [densityData]
  }

});




function detailpegawai(e) { console.log();

  const points = myChart.getElementsAtEventForMode(e, 'point', { intersect: true }, true);

  if (points.length) {
        const firstPoint = points[0];
        const value = myChart.data.datasets[firstPoint.datasetIndex].data[firstPoint.index];
        
        bukaModal(value.x,value.y,value.nip,value.id_asn_talenta);
        //console.log(value);  

    }
}

function bukaModal(x,y,nama,id_asn_talenta) {
  // $('#modal-pegawai-detail').modal("show");
  // $('#nilai-kinerja').text(x);
  // $('#nilai-kompetensi').text(y);
  // $('#id-nip').text(nip);

  // memulai ajax
    $.ajax({
      url: '<?= base_url('administrator/asn_talenta/mantap') ?>',  // set url -> ini file yang menyimpan query tampil detail data siswa
      method: 'get',   // method -> metodenya pakai post. Tahu kan post? gak tahu? browsing aja :)
      data: {nama:nama,id_asn_talenta:id_asn_talenta},    // nah ini datanya -> {id:id} = berarti menyimpan data post id yang nilainya dari = var id = $(this).attr("id");
      success:function(data){   // kode dibawah ini jalan kalau sukses
        $('#data_siswa').html(data);  // mengisi konten dari -> <div class="modal-body" id="data_siswa">
        $('#myModal').modal("show");  // menampilkan dialog modal nya
      }
    });

  //console.log(x,y,nip);
}

</script>
