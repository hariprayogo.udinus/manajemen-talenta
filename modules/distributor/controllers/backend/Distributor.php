<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Distributor Controller
*| --------------------------------------------------------------------------
*| Distributor site
*|
*/
class Distributor extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_distributor');
		$this->load->model('group/model_group');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Distributors
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('distributor_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['distributors'] = $this->model_distributor->get($filter, $field, $this->limit_page, $offset);
		$this->data['distributor_counts'] = $this->model_distributor->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/distributor/index/',
			'total_rows'   => $this->data['distributor_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Distributor List');
		$this->render('backend/standart/administrator/distributor/distributor_list', $this->data);
	}
	
	/**
	* Add new distributors
	*
	*/
	public function add()
	{
		$this->is_allowed('distributor_add');

		$this->template->title('Distributor New');
		$this->render('backend/standart/administrator/distributor/distributor_add', $this->data);
	}

	/**
	* Add New Distributors
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('distributor_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		

		$this->form_validation->set_rules('nama_distributor', 'Nama Distributor', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		

		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		

		$this->form_validation->set_rules('telp', 'Telp', 'trim|required|max_length[15]');
		

		$this->form_validation->set_rules('mulai_kontrak', 'Mulai Kontrak', 'trim|required');
		

		$this->form_validation->set_rules('selesai_kontrak', 'Selesai Kontrak', 'trim|required');
		

		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		

		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_distributor' => $this->input->post('nama_distributor'),
				'alamat' => $this->input->post('alamat'),
				'email' => $this->input->post('email'),
				'telp' => $this->input->post('telp'),
				'mulai_kontrak' => $this->input->post('mulai_kontrak'),
				'selesai_kontrak' => $this->input->post('selesai_kontrak'),
				'status' => $this->input->post('status'),
			];

			
			
//$save_data['_example'] = $this->input->post('_example');
			



			
			
			$save_distributor = $id = $this->model_distributor->store($save_data);
            

			if ($save_distributor) {
				
				$id = $save_distributor;
				
				
					
				
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_distributor;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/distributor/edit/' . $save_distributor, 'Edit Distributor'),
						anchor('administrator/distributor', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/distributor/edit/' . $save_distributor, 'Edit Distributor')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/distributor');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/distributor');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$this->response($this->data);
	}
	
		/**
	* Update view Distributors
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('distributor_update');

		$this->data['distributor'] = $this->model_distributor->find($id);

		$this->template->title('Distributor Update');
		$this->render('backend/standart/administrator/distributor/distributor_update', $this->data);
	}

	/**
	* Update Distributors
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('distributor_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
				$this->form_validation->set_rules('nama_distributor', 'Nama Distributor', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		

		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
		

		$this->form_validation->set_rules('telp', 'Telp', 'trim|required|max_length[15]');
		

		$this->form_validation->set_rules('mulai_kontrak', 'Mulai Kontrak', 'trim|required');
		

		$this->form_validation->set_rules('selesai_kontrak', 'Selesai Kontrak', 'trim|required');
		

		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		

		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_distributor' => $this->input->post('nama_distributor'),
				'alamat' => $this->input->post('alamat'),
				'email' => $this->input->post('email'),
				'telp' => $this->input->post('telp'),
				'mulai_kontrak' => $this->input->post('mulai_kontrak'),
				'selesai_kontrak' => $this->input->post('selesai_kontrak'),
				'status' => $this->input->post('status'),
			];

			

			
//$save_data['_example'] = $this->input->post('_example');
			


			
			
			$save_distributor = $this->model_distributor->change($id, $save_data);

			if ($save_distributor) {

				
				

				
				
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/distributor', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/distributor');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/distributor');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$this->response($this->data);
	}
	
	/**
	* delete Distributors
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('distributor_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'distributor'), 'success');
        } else {
            set_message(cclang('error_delete', 'distributor'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Distributors
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('distributor_view');

		$this->data['distributor'] = $this->model_distributor->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Distributor Detail');
		$this->render('backend/standart/administrator/distributor/distributor_view', $this->data);
	}
	
	/**
	* delete Distributors
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$distributor = $this->model_distributor->find($id);

		
		
		return $this->model_distributor->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('distributor_export');

		$this->model_distributor->export(
			'distributor', 
			'distributor',
			$this->model_distributor->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('distributor_export');

		$this->model_distributor->pdf('distributor', 'distributor');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('distributor_export');

		$table = $title = 'distributor';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_distributor->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file distributor.php */
/* Location: ./application/controllers/administrator/Distributor.php */