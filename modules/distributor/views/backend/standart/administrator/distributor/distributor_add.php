
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<style>
   /* .group-nama_distributor */
   .group-nama_distributor {

   }

   .group-nama_distributor .control-label {

   }

   .group-nama_distributor .col-sm-8 {

   }

   .group-nama_distributor .form-control {

   }

   .group-nama_distributor .help-block {

   }
   /* end .group-nama_distributor */



   /* .group-alamat */
   .group-alamat {

   }

   .group-alamat .control-label {

   }

   .group-alamat .col-sm-8 {

   }

   .group-alamat .form-control {

   }

   .group-alamat .help-block {

   }
   /* end .group-alamat */



   /* .group-email */
   .group-email {

   }

   .group-email .control-label {

   }

   .group-email .col-sm-8 {

   }

   .group-email .form-control {

   }

   .group-email .help-block {

   }
   /* end .group-email */



   /* .group-telp */
   .group-telp {

   }

   .group-telp .control-label {

   }

   .group-telp .col-sm-8 {

   }

   .group-telp .form-control {

   }

   .group-telp .help-block {

   }
   /* end .group-telp */



   /* .group-mulai_kontrak */
   .group-mulai_kontrak {

   }

   .group-mulai_kontrak .control-label {

   }

   .group-mulai_kontrak .col-sm-8 {

   }

   .group-mulai_kontrak .form-control {

   }

   .group-mulai_kontrak .help-block {

   }
   /* end .group-mulai_kontrak */



   /* .group-selesai_kontrak */
   .group-selesai_kontrak {

   }

   .group-selesai_kontrak .control-label {

   }

   .group-selesai_kontrak .col-sm-8 {

   }

   .group-selesai_kontrak .form-control {

   }

   .group-selesai_kontrak .help-block {

   }
   /* end .group-selesai_kontrak */



   /* .group-status */
   .group-status {

   }

   .group-status .control-label {

   }

   .group-status .col-sm-8 {

   }

   .group-status .form-control {

   }

   .group-status .help-block {

   }
   /* end .group-status */




</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Distributor        <small><?= cclang('new', ['Distributor']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/distributor'); ?>">Distributor</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Distributor</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Distributor']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name' => 'form_distributor',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_distributor',
                            'enctype' => 'multipart/form-data',
                            'method' => 'POST'
                        ]); ?>
                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>
                                                                                                    <div class="form-group group-nama_distributor ">
                                <label for="nama_distributor" class="col-sm-2 control-label">Nama Distributor                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama_distributor" id="nama_distributor" placeholder="Nama Distributor" value="<?= set_value('nama_distributor'); ?>">
                                    <small class="info help-block">
                                        <b>Input Nama Distributor</b> Max Length : 100.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-alamat ">
                                <label for="alamat" class="col-sm-2 control-label">Alamat                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <textarea id="alamat" name="alamat" rows="5" class="textarea form-control" placeholder="Alamat"><?= set_value('alamat'); ?></textarea>
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-email ">
                                <label for="email" class="col-sm-2 control-label">Email                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email'); ?>">
                                    <small class="info help-block">
                                        <b>Input Email</b> Max Length : 50.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-telp ">
                                <label for="telp" class="col-sm-2 control-label">Telp                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="telp" id="telp" placeholder="Telp" value="<?= set_value('telp'); ?>">
                                    <small class="info help-block">
                                        <b>Input Telp</b> Max Length : 15.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-mulai_kontrak ">
                                <label for="mulai_kontrak" class="col-sm-2 control-label">Mulai Kontrak                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-6">
                                    <div class="input-group date col-sm-8">
                                        <input type="text" class="form-control pull-right datetimepicker" name="mulai_kontrak" id="mulai_kontrak">
                                    </div>
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-selesai_kontrak ">
                                <label for="selesai_kontrak" class="col-sm-2 control-label">Selesai Kontrak                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-6">
                                    <div class="input-group date col-sm-8">
                                        <input type="text" class="form-control pull-right datetimepicker" name="selesai_kontrak" id="selesai_kontrak">
                                    </div>
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-status ">
                                <label for="status" class="col-sm-2 control-label">Status                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen chosen-select" name="status" id="status" data-placeholder="Select Status">
                                        <option value=""></option>
                                        <option value="1">Aktif</option>
                                        <option value="'0'">Non Aktif</option>
                                                                            </select>
                                    <small class="info help-block">
                                        
                                        </small>
                                </div>
                            </div>
                        

                                                

                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>

                            <div class="custom-button-wrapper">

                                                        </div>


                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>

                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->

<script>
    $(document).ready(function() {
        
    window.event_submit_and_action = '';
        
    (function(){
    var nama_distributor = $('#nama_distributor');
   /* 
    nama_distributor.on('change', function() {});
    */
    var alamat = $('#alamat');
   var email = $('#email');
   var telp = $('#telp');
   var mulai_kontrak = $('#mulai_kontrak');
   var selesai_kontrak = $('#selesai_kontrak');
   var status = $('#status');
   
})()
      

      
      

                    
    $('#btn_cancel').click(function() {
        swal({
                title: "<?= cclang('are_you_sure'); ?>",
                text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = BASE_URL + 'administrator/distributor';
                }
            });

        return false;
    }); /*end btn cancel*/

    $('.btn_save').click(function() {
        $('.message').fadeOut();
        
    var form_distributor = $('#form_distributor');
    var data_post = form_distributor.serializeArray();
    var save_type = $(this).attr('data-stype');

    data_post.push({
        name: 'save_type',
        value: save_type
    });

    data_post.push({
        name: 'event_submit_and_action',
        value: window.event_submit_and_action
    });

    (function(){
    data_post.push({
        name : '_example',
        value : 'value_of_example',
    })
})()
      

    $('.loading').show();

    $.ajax({
            url: BASE_URL + '/administrator/distributor/add_save',
            type: 'POST',
            dataType: 'json',
            data: data_post,
        })
        .done(function(res) {
            $('form').find('.form-group').removeClass('has-error');
            $('.steps li').removeClass('error');
            $('form').find('.error-input').remove();
            if (res.success) {
                
            if (save_type == 'back') {
                window.location.href = res.redirect;
                return;
            }

            $('.message').printMessage({
                message: res.message
            });
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
            
            } else {
                if (res.errors) {

                    $.each(res.errors, function(index, val) {
                        $('form #' + index).parents('.form-group').addClass('has-error');
                        $('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
                    });
                    $('.steps li').removeClass('error');
                    $('.content section').each(function(index, el) {
                        if ($(this).find('.has-error').length) {
                            $('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
                        }
                    });
                }
                $('.message').printMessage({
                    message: res.message,
                    type: 'warning'
                });
            }

        })
        .fail(function() {
            $('.message').printMessage({
                message: 'Error save data',
                type: 'warning'
            });
        })
        .always(function() {
            $('.loading').hide();
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 2000);
        });

    return false;
    }); /*end btn save*/

    

    

    


    }); /*end doc ready*/
</script>