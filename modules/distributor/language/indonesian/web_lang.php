<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['distributor'] = 'Distributor';
$lang['id_distributor'] = 'Id Distributor';
$lang['nama_distributor'] = 'Nama Distributor';
$lang['alamat'] = 'Alamat';
$lang['email'] = 'Email';
$lang['telp'] = 'Telp';
$lang['mulai_kontrak'] = 'Mulai Kontrak';
$lang['selesai_kontrak'] = 'Selesai Kontrak';
$lang['status'] = 'Status';
