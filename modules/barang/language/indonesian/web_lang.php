<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['barang'] = 'Barang';
$lang['id_barang'] = 'Id Barang';
$lang['nama_barang'] = 'Nama Barang';
$lang['kategori_barang'] = 'Kategori Barang';
$lang['distributor'] = 'Distributor';
$lang['foto'] = 'Foto';
