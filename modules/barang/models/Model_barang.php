<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_barang extends MY_Model {

    private $primary_key    = 'id_barang';
    private $table_name     = 'barang';
    public $field_search   = ['nama_barang', 'kategori_barang', 'distributor', 'foto', 'kategori.nama_kategori', 'distributor.nama_distributor'];
    public $sort_option = ['nama_barang', 'asc'];

    public $distributor     = 'distributor';
    
    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
            'sort_option'   => $this->sort_option,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                $f_search = "barang.".$field;

                if (strpos($field, '.')) {
                    $f_search = $field;
                }
                if ($iterasi == 1) {
                    $where .=  $f_search . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " .  $f_search . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "barang.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                $f_search = "barang.".$field;
                if (strpos($field, '.')) {
                    $f_search = $field;
                }

                if ($iterasi == 1) {
                    $where .= $f_search . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " .$f_search . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "barang.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('kategori', 'kategori.id_kategori = barang.kategori_barang', 'LEFT');
        $this->db->join('distributor', 'distributor.id_distributor = barang.distributor', 'LEFT');
        
        $this->db->select('kategori.nama_kategori,distributor.nama_distributor,barang.*,kategori.nama_kategori as kategori_nama_kategori,kategori.nama_kategori as nama_kategori,distributor.nama_distributor as distributor_nama_distributor,distributor.nama_distributor as nama_distributor');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

    public function get_distributor_status()
    
    {
        $query = $this->db->get_where('distributor',['status' => '1']);
        return $query->result();  
    }
}

/* End of file Model_barang.php */
/* Location: ./application/models/Model_barang.php */