<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Barang Controller
*| --------------------------------------------------------------------------
*| Barang site
*|
*/
class Barang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_barang');
		$this->load->model('group/model_group');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Barangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('barang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['barangs'] = $this->model_barang->get($filter, $field, $this->limit_page, $offset);
		$this->data['barang_counts'] = $this->model_barang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/barang/index/',
			'total_rows'   => $this->data['barang_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Barang List');
		$this->render('backend/standart/administrator/barang/barang_list', $this->data);
	}
	
	/**
	* Add new barangs
	*
	*/
	public function add()
	{
		$this->is_allowed('barang_add');

		//$this->load->model('model_barang');
		$this->data['distributor'] = $this->model_barang->get_distributor_status();

		$this->template->title('Barang New');
		$this->render('backend/standart/administrator/barang/barang_add', $this->data);
	}

	/**
	* Add New Barangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('barang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		

		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('kategori_barang', 'Kategori Barang', 'trim|required');
		

		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');
		

		

		if ($this->form_validation->run()) {
			$barang_foto_uuid = $this->input->post('barang_foto_uuid');
			$barang_foto_name = $this->input->post('barang_foto_name');
		
			$save_data = [
				'nama_barang' => $this->input->post('nama_barang'),
				'kategori_barang' => $this->input->post('kategori_barang'),
				'distributor' => $this->input->post('distributor'),
			];

			
			
//$save_data['_example'] = $this->input->post('_example');
			



			
			if (!is_dir(FCPATH . '/uploads/barang/')) {
				mkdir(FCPATH . '/uploads/barang/');
			}

			if (!empty($barang_foto_name)) {
				$barang_foto_name_copy = date('YmdHis') . '-' . $barang_foto_name;

				rename(FCPATH . 'uploads/tmp/' . $barang_foto_uuid . '/' . $barang_foto_name, 
						FCPATH . 'uploads/barang/' . $barang_foto_name_copy);

				if (!is_file(FCPATH . '/uploads/barang/' . $barang_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['foto'] = $barang_foto_name_copy;
			}
		
			
			$save_barang = $id = $this->model_barang->store($save_data);
            

			if ($save_barang) {
				
				$id = $save_barang;
				
				
					
				
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_barang;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/barang/edit/' . $save_barang, 'Edit Barang'),
						anchor('administrator/barang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/barang/edit/' . $save_barang, 'Edit Barang')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/barang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/barang');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$this->response($this->data);
	}
	
		/**
	* Update view Barangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('barang_update');

		$this->data['barang'] = $this->model_barang->find($id);

		$this->template->title('Barang Update');
		$this->render('backend/standart/administrator/barang/barang_update', $this->data);
	}

	/**
	* Update Barangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('barang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
				$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('kategori_barang', 'Kategori Barang', 'trim|required');
		

		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');
		

		
		if ($this->form_validation->run()) {
			$barang_foto_uuid = $this->input->post('barang_foto_uuid');
			$barang_foto_name = $this->input->post('barang_foto_name');
		
			$save_data = [
				'nama_barang' => $this->input->post('nama_barang'),
				'kategori_barang' => $this->input->post('kategori_barang'),
				'distributor' => $this->input->post('distributor'),
			];

			

			
//$save_data['_example'] = $this->input->post('_example');
			


			
			if (!is_dir(FCPATH . '/uploads/barang/')) {
				mkdir(FCPATH . '/uploads/barang/');
			}

			if (!empty($barang_foto_uuid)) {
				$barang_foto_name_copy = date('YmdHis') . '-' . $barang_foto_name;

				rename(FCPATH . 'uploads/tmp/' . $barang_foto_uuid . '/' . $barang_foto_name, 
						FCPATH . 'uploads/barang/' . $barang_foto_name_copy);

				if (!is_file(FCPATH . '/uploads/barang/' . $barang_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['foto'] = $barang_foto_name_copy;
			}
		
			
			$save_barang = $this->model_barang->change($id, $save_data);

			if ($save_barang) {

				
				

				
				
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/barang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/barang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/barang');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$this->response($this->data);
	}
	
	/**
	* delete Barangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('barang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'barang'), 'success');
        } else {
            set_message(cclang('error_delete', 'barang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Barangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('barang_view');

		$this->data['barang'] = $this->model_barang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Barang Detail');
		$this->render('backend/standart/administrator/barang/barang_view', $this->data);
	}
	
	/**
	* delete Barangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$barang = $this->model_barang->find($id);

		if (!empty($barang->foto)) {
			$path = FCPATH . '/uploads/barang/' . $barang->foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_barang->remove($id);
	}
	
	/**
	* Upload Image Barang	* 
	* @return JSON
	*/
	public function upload_foto_file()
	{
		if (!$this->is_allowed('barang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'barang',
		]);
	}

	/**
	* Delete Image Barang	* 
	* @return JSON
	*/
	public function delete_foto_file($uuid)
	{
		if (!$this->is_allowed('barang_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'foto', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'barang',
            'primary_key'       => 'id_barang',
            'upload_path'       => 'uploads/barang/'
        ]);
	}

	/**
	* Get Image Barang	* 
	* @return JSON
	*/
	public function get_foto_file($id)
	{
		if (!$this->is_allowed('barang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$barang = $this->model_barang->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'foto', 
            'table_name'        => 'barang',
            'primary_key'       => 'id_barang',
            'upload_path'       => 'uploads/barang/',
            'delete_endpoint'   => 'administrator/barang/delete_foto_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('barang_export');

		$this->model_barang->export(
			'barang', 
			'barang',
			$this->model_barang->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('barang_export');

		$this->model_barang->pdf('barang', 'barang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('barang_export');

		$table = $title = 'barang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_barang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file barang.php */
/* Location: ./application/controllers/administrator/Barang.php */