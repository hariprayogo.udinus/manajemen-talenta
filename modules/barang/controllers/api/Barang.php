<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Barang extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_barang');
	}

	/**
	 * @api {get} /barang/all Get all barangs.
	 * @apiVersion 0.1.0
	 * @apiName AllBarang 
	 * @apiGroup barang
	 * @apiHeader {String} X-Api-Key Barangs unique access-key.
	 * @apiHeader {String} X-Token Barangs unique token.
	 * @apiPermission Barang Cant be Accessed permission name : api_barang_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Barangs.
	 * @apiParam {String} [Field="All Field"] Optional field of Barangs : id_barang, kode_barang, nama_barang, foto, kategori_barang, distributor.
	 * @apiParam {String} [Start=0] Optional start index of Barangs.
	 * @apiParam {String} [Limit=10] Optional limit data of Barangs.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of barang.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataBarang Barang data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_barang_all');

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id_barang', 'kode_barang', 'nama_barang', 'foto', 'kategori_barang', 'distributor'];
		$barangs = $this->model_api_barang->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_barang->count_all($filter, $field);
		$barangs = array_map(function($row){
						
			return $row;
		}, $barangs);

		$barang_arr = [];

		foreach ($barangs as $barang) {
			$barang->foto  = BASE_URL.'uploads/barang/'.$barang->foto;
			$barang_arr[] = $barang;
		}

		$data['barang'] = $barang_arr;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Barang',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /barang/detail Detail Barang.
	 * @apiVersion 0.1.0
	 * @apiName DetailBarang
	 * @apiGroup barang
	 * @apiHeader {String} X-Api-Key Barangs unique access-key.
	 * @apiHeader {String} X-Token Barangs unique token.
	 * @apiPermission Barang Cant be Accessed permission name : api_barang_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Barangs.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of barang.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError BarangNotFound Barang data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_barang_detail');

		$this->requiredInput(['id_barang']);

		$id = $this->get('id_barang');

		$select_field = ['id_barang', 'kode_barang', 'nama_barang', 'foto', 'kategori_barang', 'distributor'];
		$barang = $this->model_api_barang->find($id, $select_field);

		if (!$barang) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['barang'] = $barang;
		if ($data['barang']) {
			$data['barang']->foto = BASE_URL.'uploads/barang/'.$data['barang']->foto;
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Barang',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Barang not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /barang/add Add Barang.
	 * @apiVersion 0.1.0
	 * @apiName AddBarang
	 * @apiGroup barang
	 * @apiHeader {String} X-Api-Key Barangs unique access-key.
	 * @apiHeader {String} X-Token Barangs unique token.
	 * @apiPermission Barang Cant be Accessed permission name : api_barang_add
	 *
 	 * @apiParam {String} Kode_barang Mandatory kode_barang of Barangs.  
	 * @apiParam {String} Nama_barang Mandatory nama_barang of Barangs. Input Nama Barang Max Length : 100. 
	 * @apiParam {String} Kategori_barang Mandatory kategori_barang of Barangs. Input Kategori Barang Max Length : 100. 
	 * @apiParam {String} Distributor Mandatory distributor of Barangs. Input Distributor Max Length : 100. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_barang_add');

		$this->form_validation->set_rules('kode_barang', 'Kode Barang', 'trim|required');
		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('kategori_barang', 'Kategori Barang', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required|max_length[100]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'kode_barang' => $this->input->post('kode_barang'),
				'nama_barang' => $this->input->post('nama_barang'),
				'kategori_barang' => $this->input->post('kategori_barang'),
				'distributor' => $this->input->post('distributor'),
			];
			
			$save_barang = $this->model_api_barang->store($save_data);

			if ($save_barang) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Validation Errors.',
				'errors' 	=> $this->form_validation->error_array()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /barang/update Update Barang.
	 * @apiVersion 0.1.0
	 * @apiName UpdateBarang
	 * @apiGroup barang
	 * @apiHeader {String} X-Api-Key Barangs unique access-key.
	 * @apiHeader {String} X-Token Barangs unique token.
	 * @apiPermission Barang Cant be Accessed permission name : api_barang_update
	 *
	 * @apiParam {String} Kode_barang Mandatory kode_barang of Barangs.  
	 * @apiParam {String} Nama_barang Mandatory nama_barang of Barangs. Input Nama Barang Max Length : 100. 
	 * @apiParam {String} Kategori_barang Mandatory kategori_barang of Barangs. Input Kategori Barang Max Length : 100. 
	 * @apiParam {String} Distributor Mandatory distributor of Barangs. Input Distributor Max Length : 100. 
	 * @apiParam {Integer} id_barang Mandatory id_barang of Barang.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_barang_update');

		
		$this->form_validation->set_rules('kode_barang', 'Kode Barang', 'trim|required');
		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('kategori_barang', 'Kategori Barang', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required|max_length[100]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'kode_barang' => $this->input->post('kode_barang'),
				'nama_barang' => $this->input->post('nama_barang'),
				'kategori_barang' => $this->input->post('kategori_barang'),
				'distributor' => $this->input->post('distributor'),
			];
			
			$save_barang = $this->model_api_barang->change($this->post('id_barang'), $save_data);

			if ($save_barang) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Validation Errors.',
				'errors' 	=> $this->form_validation->error_array()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /barang/delete Delete Barang. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteBarang
	 * @apiGroup barang
	 * @apiHeader {String} X-Api-Key Barangs unique access-key.
	 * @apiHeader {String} X-Token Barangs unique token.
	 	 * @apiPermission Barang Cant be Accessed permission name : api_barang_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Barangs .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_barang_delete');

		$barang = $this->model_api_barang->find($this->post('id_barang'));

		if (!$barang) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Barang not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_barang->remove($this->post('id_barang'));

			if (!empty($barang->foto)) {
				$path = FCPATH . '/uploads/barang/' . $barang->foto;

				if (is_file($path)) {
					$delete_file = unlink($path);
				}
			}

		}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Barang deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Barang not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Barang.php */
/* Location: ./application/controllers/api/Barang.php */