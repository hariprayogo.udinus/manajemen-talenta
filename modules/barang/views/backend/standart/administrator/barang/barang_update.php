

    <!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
    <link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
    <!-- Fine Uploader jQuery JS file
    ====================================================================== -->
    <script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
    <?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Barang        <small>Edit Barang</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/barang'); ?>">Barang</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<style>
   /* .group-kode_barang */
   .group-kode_barang {

   }

   .group-kode_barang .control-label {

   }

   .group-kode_barang .col-sm-8 {

   }

   .group-kode_barang .form-control {

   }

   .group-kode_barang .help-block {

   }
   /* end .group-kode_barang */



   /* .group-nama_barang */
   .group-nama_barang {

   }

   .group-nama_barang .control-label {

   }

   .group-nama_barang .col-sm-8 {

   }

   .group-nama_barang .form-control {

   }

   .group-nama_barang .help-block {

   }
   /* end .group-nama_barang */



   /* .group-kategori_barang */
   .group-kategori_barang {

   }

   .group-kategori_barang .control-label {

   }

   .group-kategori_barang .col-sm-8 {

   }

   .group-kategori_barang .form-control {

   }

   .group-kategori_barang .help-block {

   }
   /* end .group-kategori_barang */



   /* .group-distributor */
   .group-distributor {

   }

   .group-distributor .control-label {

   }

   .group-distributor .col-sm-8 {

   }

   .group-distributor .form-control {

   }

   .group-distributor .help-block {

   }
   /* end .group-distributor */




</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Barang</h3>
                            <h5 class="widget-user-desc">Edit Barang</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/barang/edit_save/'.$this->uri->segment(4)), [
                            'name' => 'form_barang',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_barang',
                            'method' => 'POST'
                        ]); ?>

                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>

                                                    
                        
                        <div class="form-group group-nama_barang  ">
                                <label for="nama_barang" class="col-sm-2 control-label">Nama Barang                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="" value="<?= set_value('nama_barang', $barang->nama_barang); ?>">
                                    <small class="info help-block">
                                        <b>Input Nama Barang</b> Max Length : 100.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-kategori_barang">
                                <label for="kategori_barang" class="col-sm-2 control-label">Kategori Barang                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen chosen-select-deselect" name="kategori_barang" id="kategori_barang" data-placeholder="Select Kategori Barang">
                                        <option value=""></option>
                                                                                    <?php foreach (db_get_all_data('kategori') as $row): ?>
                                            <option <?= $row->id_kategori == $barang->kategori_barang ? 'selected' : ''; ?> value="<?= $row->id_kategori ?>"><?= $row->nama_kategori; ?></option>
                                            <?php endforeach; ?>
                                                                            </select>
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>

                        
                        
                                                    
                        
                        <div class="form-group group-distributor">
                                <label for="distributor" class="col-sm-2 control-label">Distributor                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen chosen-select-deselect" name="distributor" id="distributor" data-placeholder="Select Distributor">
                                        <option value=""></option>
                                                                                    <?php foreach (db_get_all_data('distributor') as $row): ?>
                                            <option <?= $row->id_distributor == $barang->distributor ? 'selected' : ''; ?> value="<?= $row->id_distributor ?>"><?= $row->nama_distributor; ?></option>
                                            <?php endforeach; ?>
                                                                            </select>
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>

                        
                        
                                                    
                        
                        <div class="form-group group-foto  ">
                                <label for="foto" class="col-sm-2 control-label">Foto                                    </label>
                                <div class="col-sm-8">
                                    <div id="barang_foto_galery"></div>
                                    <input class="data_file data_file_uuid" name="barang_foto_uuid" id="barang_foto_uuid" type="hidden" value="<?= set_value('barang_foto_uuid'); ?>">
                                    <input class="data_file" name="barang_foto_name" id="barang_foto_name" type="hidden" value="<?= set_value('barang_foto_name', $barang->foto); ?>">
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>
                        
                        
                        
                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>

                            <div class="custom-button-wrapper">

                                                        </div>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function() {
    window.event_submit_and_action = '';
            
    (function(){
    var kode_barang = $('#kode_barang');
   /* 
    kode_barang.on('change', function() {});
    */
    var nama_barang = $('#nama_barang');
   var kategori_barang = $('#kategori_barang');
   var distributor = $('#distributor');
   
})()
      
      
      
      
        
        
    
    $('#btn_cancel').click(function() {
        swal({
                title: "Are you sure?",
                text: "the data that you have created will be in the exhaust!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = BASE_URL + 'administrator/barang';
                }
            });

        return false;
    }); /*end btn cancel*/

    $('.btn_save').click(function() {
        $('.message').fadeOut();
        
    var form_barang = $('#form_barang');
    var data_post = form_barang.serializeArray();
    var save_type = $(this).attr('data-stype');
    data_post.push({
        name: 'save_type',
        value: save_type
    });

    (function(){
    data_post.push({
        name : '_example',
        value : 'value_of_example',
    })
})()
      
      
    data_post.push({
        name: 'event_submit_and_action',
        value: window.event_submit_and_action
    });

    $('.loading').show();

    $.ajax({
            url: form_barang.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: data_post,
        })
        .done(function(res) {
            $('form').find('.form-group').removeClass('has-error');
            $('form').find('.error-input').remove();
            $('.steps li').removeClass('error');
            if (res.success) {
                var id = $('#barang_image_galery').find('li').attr('qq-file-id');
                if (save_type == 'back') {
                    window.location.href = res.redirect;
                    return;
                }

                $('.message').printMessage({
                    message: res.message
                });
                $('.message').fadeIn();
                $('.data_file_uuid').val('');

            } else {
                if (res.errors) {
                    parseErrorField(res.errors);
                }
                $('.message').printMessage({
                    message: res.message,
                    type: 'warning'
                });
            }

        })
        .fail(function() {
            $('.message').printMessage({
                message: 'Error save data',
                type: 'warning'
            });
        })
        .always(function() {
            $('.loading').hide();
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 2000);
        });

    return false;
    }); /*end btn save*/

                        var params = {};
            params[csrf] = token;

            $('#barang_foto_galery').fineUploader({
                template: 'qq-template-gallery',
                request: {
                    endpoint: BASE_URL + '/administrator/barang/upload_foto_file',
                    params: params
                },
                deleteFile: {
                    enabled: true, // defaults to false
                    endpoint: BASE_URL + '/administrator/barang/delete_foto_file'
                },
                thumbnails: {
                    placeholders: {
                        waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                        notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                    }
                },
                session: {
                    endpoint: BASE_URL + 'administrator/barang/get_foto_file/<?= $barang->id_barang; ?>',
                    refreshOnRequest: true
                },
                multiple: false,
                validation: {
                    allowedExtensions: ["*"],
                    sizeLimit: 0,
                                    },
                showMessage: function(msg) {
                    toastr['error'](msg);
                },
                callbacks: {
                    onComplete: function(id, name, xhr) {
                        if (xhr.success) {
                            var uuid = $('#barang_foto_galery').fineUploader('getUuid', id);
                            $('#barang_foto_uuid').val(uuid);
                            $('#barang_foto_name').val(xhr.uploadName);
                        } else {
                            toastr['error'](xhr.error);
                        }
                    },
                    onSubmit: function(id, name) {
                        var uuid = $('#barang_foto_uuid').val();
                        $.get(BASE_URL + '/administrator/barang/delete_foto_file/' + uuid);
                    },
                    onDeleteComplete: function(id, xhr, isError) {
                        if (isError == false) {
                            $('#barang_foto_uuid').val('');
                            $('#barang_foto_name').val('');
                        }
                    }
                }
            }); /*end foto galey*/
            

    

    async function chain() {
            }

    chain();




    }); /*end doc ready*/
</script>