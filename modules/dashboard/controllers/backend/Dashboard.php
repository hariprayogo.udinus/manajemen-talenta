<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Dashboard Controller
*| --------------------------------------------------------------------------
*| For see your board
*|
*/
class Dashboard extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if (!$this->aauth->is_allowed('dashboard')) {
			redirect('/', 'refresh');
		}
		$data = [];
		$grup_eselon 	= $this->db->select('tipe_eselon, COUNT(*) jml')
								   ->group_by('tipe_eselon')
								   ->get('asn_talenta')
								   ->result();

		$grup_opd 		= $this->db->select('opd, COUNT(*) jml')
								   ->group_by('opd')
								   ->get('asn_talenta')
								   ->result();

		$cat_eselon 	= [];
		$count_eselon 	= [];
		foreach ($grup_eselon as $key => $value) {
			$cat_eselon[] 	= $value->tipe_eselon;
			$count_eselon[]	= array('key' => $value->tipe_eselon, 'y' => (int)$value->jml);
		}

		foreach ($grup_opd as $key => $value) {
			$kuadran 	= $this->db->select('kuadran, COUNT(*) jml')
								   ->where('opd', $value->opd)
								   ->order_by('kuadran', 'DESC')
								   ->group_by('kuadran')
								   ->get('asn_talenta')
								   ->result();

			$value->kuadran = $kuadran;
		}

		$data['cat_eselon'] 	= $cat_eselon;
		$data['count_eselon'] 	= $count_eselon;
		$data['grup_opd'] 		= $grup_opd;

		$this->render('backend/standart/dashboard', $data);
	}

	public function eselon($eselon='III')
	{
		if (!$this->aauth->is_allowed('dashboard')) {
			redirect('/', 'refresh');
		}
		$data = [];
		$data['asn'] 	= $this->db->where('tipe_eselon', $eselon)
								   ->get('asn_talenta')
								   ->result();

		$this->render('backend/standart/eselon', $data);
	}

	public function opd($opd='III')
	{
		if (!$this->aauth->is_allowed('dashboard')) {
			redirect('/', 'refresh');
		}
		$data = [];
		$opd  = str_replace('%20', ' ', $opd);
		$opd  = str_replace('hesoyam', ',', $opd);
		$data['asn'] 	= $this->db->where('opd', $opd)
								   ->get('asn_talenta')
								   ->result();

		$this->render('backend/standart/eselon', $data);
	}

	public function chart()
	{
		if (!$this->aauth->is_allowed('dashboard')) {
			redirect('/','refresh');
		}

		$data = [];
		$this->render('backend/standart/chart', $data);
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/administrator/Dashboard.php */