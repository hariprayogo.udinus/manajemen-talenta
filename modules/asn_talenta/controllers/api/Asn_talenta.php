<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Asn_talenta extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_asn_talenta');
	}

	/**
	 * @api {get} /asn_talenta/all Get all asn_talentas.
	 * @apiVersion 0.1.0
	 * @apiName AllAsntalenta 
	 * @apiGroup asn_talenta
	 * @apiHeader {String} X-Api-Key Asn talentas unique access-key.
	 * @apiHeader {String} X-Token Asn talentas unique token.
	 * @apiPermission Asn talenta Cant be Accessed permission name : api_asn_talenta_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Asn talentas.
	 * @apiParam {String} [Field="All Field"] Optional field of Asn talentas : id_asn_talenta, nama, nip, golongan, pangkat, jabatan, lokasi_kerja, unit_kerja, opd, avatar, status, kompetensi, kinerja, kelas_jabatan, nilai_jabatan, kuadran, eselon, tipe_eselon, created_by, created_at.
	 * @apiParam {String} [Start=0] Optional start index of Asn talentas.
	 * @apiParam {String} [Limit=10] Optional limit data of Asn talentas.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of asn_talenta.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataAsn talenta Asn talenta data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_asn_talenta_all');

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id_asn_talenta', 'nama', 'nip', 'golongan', 'pangkat', 'jabatan', 'lokasi_kerja', 'unit_kerja', 'opd', 'avatar', 'status', 'kompetensi', 'kinerja', 'kelas_jabatan', 'nilai_jabatan', 'kuadran', 'eselon', 'tipe_eselon', 'created_by', 'created_at'];
		$asn_talentas = $this->model_api_asn_talenta->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_asn_talenta->count_all($filter, $field);
		$asn_talentas = array_map(function($row){
						
			return $row;
		}, $asn_talentas);

		$data['asn_talenta'] = $asn_talentas;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Asn talenta',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /asn_talenta/detail Detail Asn talenta.
	 * @apiVersion 0.1.0
	 * @apiName DetailAsn talenta
	 * @apiGroup asn_talenta
	 * @apiHeader {String} X-Api-Key Asn talentas unique access-key.
	 * @apiHeader {String} X-Token Asn talentas unique token.
	 * @apiPermission Asn talenta Cant be Accessed permission name : api_asn_talenta_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Asn talentas.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of asn_talenta.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Asn talentaNotFound Asn talenta data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_asn_talenta_detail');

		$this->requiredInput(['nip']);

		$id = $this->get('nip');

		$select_field = ['id_asn_talenta', 'nama', 'nip', 'golongan', 'pangkat', 'jabatan', 'lokasi_kerja', 'unit_kerja', 'opd', 'avatar', 'status', 'kompetensi', 'kinerja', 'kelas_jabatan', 'nilai_jabatan', 'kuadran', 'eselon', 'tipe_eselon', 'created_by', 'created_at'];
		$asn_talenta = $this->model_api_asn_talenta->find($id, $select_field);

		if (!$asn_talenta) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['asn_talenta'] = $asn_talenta;
		if ($data['asn_talenta']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Asn talenta',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Asn talenta not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function kuadran_get()
	{
		// $this->is_allowed('api_asn_talenta_detail');

		$this->requiredInput(['nip']);

		$id = $this->get('nip');

		// $select_field = ['id_asn_talenta', 'nama', 'nip', 'golongan', 'pangkat', 'jabatan', 'lokasi_kerja', 'unit_kerja', 'opd', 'avatar', 'status', 'kompetensi', 'kinerja', 'kelas_jabatan', 'nilai_jabatan', 'kuadran', 'eselon', 'tipe_eselon', 'created_by', 'created_at'];
		// $asn_talenta = $this->model_api_asn_talenta->find($id, $select_field);

		$data_kuadran 	= $this->db->select('nama, nip, kuadran, kompetensi nilai_kompetensi, kinerja nilai_kinerja')
								   ->where('nip', $id)
								   ->get('asn_talenta')
								   ->row();

		if ($data_kuadran) {
			$detail 	= $this->db->select('bobot_skp, bobot_absen, bobot_hukdis, bobot_uji_kompetensi, bobot_cat, bobot_pendidikan, bobot_jabatan, bobot_diklat, bobot_seminar, nilai_seleksi_kompetensi, uji_gagasan_tertulis, uji_gagasan_lisan, nilai_evaluasi_kinerja, nilai_uji_gagasan_tertulis, nilai_paparan, nilai_wawancara, nilai_rekam_jejak, klasifikasi_seleksi_kompetensi, saran_diklat_pengembangan, kompetensi_yang_perlu_dikembangkan')
								   ->where('nip', $id)
								   ->get('asn_talenta_penilaian')
								   ->row();

			$data_kuadran->detail 	= $detail;
		}

		if (!$data_kuadran) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Data Tidak Ditemukan'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['data_kuadran'] = $data_kuadran;
		if ($data['data_kuadran']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Asn talenta',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Data Tidak Ditemukan'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /asn_talenta/add Add Asn talenta.
	 * @apiVersion 0.1.0
	 * @apiName AddAsn talenta
	 * @apiGroup asn_talenta
	 * @apiHeader {String} X-Api-Key Asn talentas unique access-key.
	 * @apiHeader {String} X-Token Asn talentas unique token.
	 * @apiPermission Asn talenta Cant be Accessed permission name : api_asn_talenta_add
	 *
 	 * @apiParam {String} Id_asn_talenta Mandatory id_asn_talenta of Asn talentas. Input Id Asn Talenta Max Length : 11. 
	 * @apiParam {String} Nama Mandatory nama of Asn talentas. Input Nama Max Length : 50. 
	 * @apiParam {String} Golongan Mandatory golongan of Asn talentas. Input Golongan Max Length : 20. 
	 * @apiParam {String} Pangkat Mandatory pangkat of Asn talentas. Input Pangkat Max Length : 20. 
	 * @apiParam {String} Jabatan Mandatory jabatan of Asn talentas. Input Jabatan Max Length : 100. 
	 * @apiParam {String} Lokasi_kerja Mandatory lokasi_kerja of Asn talentas. Input Lokasi Kerja Max Length : 100. 
	 * @apiParam {String} Unit_kerja Mandatory unit_kerja of Asn talentas. Input Unit Kerja Max Length : 100. 
	 * @apiParam {String} Opd Mandatory opd of Asn talentas. Input Opd Max Length : 100. 
	 * @apiParam {String} Avatar Mandatory avatar of Asn talentas.  
	 * @apiParam {String} Status Mandatory status of Asn talentas.  
	 * @apiParam {String} Kompetensi Mandatory kompetensi of Asn talentas. Input Kompetensi Max Length : 5. 
	 * @apiParam {String} Kinerja Mandatory kinerja of Asn talentas. Input Kinerja Max Length : 5. 
	 * @apiParam {String} Kelas_jabatan Mandatory kelas_jabatan of Asn talentas. Input Kelas Jabatan Max Length : 11. 
	 * @apiParam {String} Nilai_jabatan Mandatory nilai_jabatan of Asn talentas. Input Nilai Jabatan Max Length : 11. 
	 * @apiParam {String} Kuadran Mandatory kuadran of Asn talentas. Input Kuadran Max Length : 11. 
	 * @apiParam {String} Eselon Mandatory eselon of Asn talentas. Input Eselon Max Length : 10. 
	 * @apiParam {String} Tipe_eselon Mandatory tipe_eselon of Asn talentas. Input Tipe Eselon Max Length : 10. 
	 * @apiParam {String} Created_by Mandatory created_by of Asn talentas. Input Created By Max Length : 11. 
	 * @apiParam {String} Created_at Mandatory created_at of Asn talentas.  
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_asn_talenta_add');

		$this->form_validation->set_rules('id_asn_talenta', 'Id Asn Talenta', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('golongan', 'Golongan', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('pangkat', 'Pangkat', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('lokasi_kerja', 'Lokasi Kerja', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('opd', 'Opd', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('avatar', 'Avatar', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('kompetensi', 'Kompetensi', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('kinerja', 'Kinerja', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('kelas_jabatan', 'Kelas Jabatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('nilai_jabatan', 'Nilai Jabatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kuadran', 'Kuadran', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('eselon', 'Eselon', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('tipe_eselon', 'Tipe Eselon', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'id_asn_talenta' => $this->input->post('id_asn_talenta'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'status' => $this->input->post('status'),
				'kompetensi' => $this->input->post('kompetensi'),
				'kinerja' => $this->input->post('kinerja'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'kuadran' => $this->input->post('kuadran'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $this->input->post('tipe_eselon'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
			];
			
			$save_asn_talenta = $this->model_api_asn_talenta->store($save_data);

			if ($save_asn_talenta) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Validation Errors.',
				'errors' 	=> $this->form_validation->error_array()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /asn_talenta/update Update Asn talenta.
	 * @apiVersion 0.1.0
	 * @apiName UpdateAsn talenta
	 * @apiGroup asn_talenta
	 * @apiHeader {String} X-Api-Key Asn talentas unique access-key.
	 * @apiHeader {String} X-Token Asn talentas unique token.
	 * @apiPermission Asn talenta Cant be Accessed permission name : api_asn_talenta_update
	 *
	 * @apiParam {String} Id_asn_talenta Mandatory id_asn_talenta of Asn talentas. Input Id Asn Talenta Max Length : 11. 
	 * @apiParam {String} Nama Mandatory nama of Asn talentas. Input Nama Max Length : 50. 
	 * @apiParam {String} Golongan Mandatory golongan of Asn talentas. Input Golongan Max Length : 20. 
	 * @apiParam {String} Pangkat Mandatory pangkat of Asn talentas. Input Pangkat Max Length : 20. 
	 * @apiParam {String} Jabatan Mandatory jabatan of Asn talentas. Input Jabatan Max Length : 100. 
	 * @apiParam {String} Lokasi_kerja Mandatory lokasi_kerja of Asn talentas. Input Lokasi Kerja Max Length : 100. 
	 * @apiParam {String} Unit_kerja Mandatory unit_kerja of Asn talentas. Input Unit Kerja Max Length : 100. 
	 * @apiParam {String} Opd Mandatory opd of Asn talentas. Input Opd Max Length : 100. 
	 * @apiParam {String} Avatar Mandatory avatar of Asn talentas.  
	 * @apiParam {String} Status Mandatory status of Asn talentas.  
	 * @apiParam {String} Kompetensi Mandatory kompetensi of Asn talentas. Input Kompetensi Max Length : 5. 
	 * @apiParam {String} Kinerja Mandatory kinerja of Asn talentas. Input Kinerja Max Length : 5. 
	 * @apiParam {String} Kelas_jabatan Mandatory kelas_jabatan of Asn talentas. Input Kelas Jabatan Max Length : 11. 
	 * @apiParam {String} Nilai_jabatan Mandatory nilai_jabatan of Asn talentas. Input Nilai Jabatan Max Length : 11. 
	 * @apiParam {String} Kuadran Mandatory kuadran of Asn talentas. Input Kuadran Max Length : 11. 
	 * @apiParam {String} Eselon Mandatory eselon of Asn talentas. Input Eselon Max Length : 10. 
	 * @apiParam {String} Tipe_eselon Mandatory tipe_eselon of Asn talentas. Input Tipe Eselon Max Length : 10. 
	 * @apiParam {String} Created_by Mandatory created_by of Asn talentas. Input Created By Max Length : 11. 
	 * @apiParam {String} Created_at Mandatory created_at of Asn talentas.  
	 * @apiParam {Integer} nip Mandatory nip of Asn Talenta.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_asn_talenta_update');

		
		$this->form_validation->set_rules('id_asn_talenta', 'Id Asn Talenta', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('golongan', 'Golongan', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('pangkat', 'Pangkat', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('lokasi_kerja', 'Lokasi Kerja', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('opd', 'Opd', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('avatar', 'Avatar', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('kompetensi', 'Kompetensi', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('kinerja', 'Kinerja', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('kelas_jabatan', 'Kelas Jabatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('nilai_jabatan', 'Nilai Jabatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kuadran', 'Kuadran', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('eselon', 'Eselon', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('tipe_eselon', 'Tipe Eselon', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'id_asn_talenta' => $this->input->post('id_asn_talenta'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'status' => $this->input->post('status'),
				'kompetensi' => $this->input->post('kompetensi'),
				'kinerja' => $this->input->post('kinerja'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'kuadran' => $this->input->post('kuadran'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $this->input->post('tipe_eselon'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
			];
			
			$save_asn_talenta = $this->model_api_asn_talenta->change($this->post('nip'), $save_data);

			if ($save_asn_talenta) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Validation Errors.',
				'errors' 	=> $this->form_validation->error_array()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /asn_talenta/delete Delete Asn talenta. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteAsn talenta
	 * @apiGroup asn_talenta
	 * @apiHeader {String} X-Api-Key Asn talentas unique access-key.
	 * @apiHeader {String} X-Token Asn talentas unique token.
	 	 * @apiPermission Asn talenta Cant be Accessed permission name : api_asn_talenta_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Asn talentas .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_asn_talenta_delete');

		$asn_talenta = $this->model_api_asn_talenta->find($this->post('nip'));

		if (!$asn_talenta) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Asn talenta not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_asn_talenta->remove($this->post('nip'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Asn talenta deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Asn talenta not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Asn talenta.php */
/* Location: ./application/controllers/api/Asn talenta.php */