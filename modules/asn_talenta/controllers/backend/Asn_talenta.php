<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Asn Talenta Controller
*| --------------------------------------------------------------------------
*| Asn Talenta site
*|
*/
class Asn_talenta extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_asn_talenta');
		$this->load->model('group/model_group');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Asn Talentas
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('asn_talenta_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['asn_talentas'] = $this->model_asn_talenta->get($filter, $field, $this->limit_page, $offset);
		$this->data['asn_talenta_counts'] = $this->model_asn_talenta->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/asn_talenta/index/',
			'total_rows'   => $this->data['asn_talenta_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Asn Talenta List');
		$this->render('backend/standart/administrator/asn_talenta/asn_talenta_list', $this->data);
	}

	public function by_opd()
	{
		$this->is_allowed('asn_talenta_list');

		$keyword = $this->input->get('keyword');
		$page 	 = $this->input->get('page');

		$groups         = $this->session->userdata('groups');

        $list_opd 	= $this->model_asn_talenta->get_list_opd();

		$this->data['list_opd'] = $list_opd;//echo json_encode($list_opd);die;

		$this->template->title('Pengajuan List');
		$this->render('backend/standart/administrator/asn_talenta/list_opd', $this->data);
	}

	public function list_pegawai($kode)
	{
		$this->is_allowed('asn_talenta_list');

		$list_pegawai 	= $this->model_asn_talenta->get_list_pegawai($kode);

		$this->data['list_pegawai'] = $list_pegawai;//echo json_encode($list_pegawai);die;

		$this->template->title('Pengajuan List');
		$this->render('backend/standart/administrator/asn_talenta/list_pegawai', $this->data);
	}


	public function riwayat_jabatan($nip)
	{
		$this->is_allowed('asn_talenta_list');

		$riwayat_jabatan = $this->model_asn_talenta->get_riwayat_jabatan($nip);

		return $riwayat_jabatan;
	}

	public function riwayat_pendidikan($nip)
	{
		$this->is_allowed('asn_talenta_list');

		$riwayat_pendidikan = $this->model_asn_talenta->get_riwayat_pendidikan($nip);

		return $riwayat_pendidikan;
	}

	public function riwayat_kompetensi($nip)
	{
		$this->is_allowed('asn_talenta_list');

		$riwayat_kompetensi = $this->model_asn_talenta->get_riwayat_kompetensi($nip);

		return $riwayat_kompetensi;
	}

	public function riwayat_skp($nip)
	{
		$this->is_allowed('asn_talenta_list');

		$riwayat_skp = $this->model_asn_talenta->get_riwayat_skp($nip);

		return $riwayat_skp;
	}

	public function riwayat_hukdis($nip)
	{
		$this->is_allowed('asn_talenta_list');

		$riwayat_hukdis = $this->model_asn_talenta->get_riwayat_hukdis($nip);

		return $riwayat_hukdis;
	}

	
	/**
	* Add new asn_talentas
	*
	*/
	public function add()
	{
		$this->is_allowed('asn_talenta_add');

		$this->template->title('Asn Talenta New');
		$this->render('backend/standart/administrator/asn_talenta/asn_talenta_add', $this->data);
	}

	public function get_detail_by_nip(){
		$nip_data 	= $this->input->get('nip');
		// $is_atasan_plt 	= $this->input->get('is_atasan_plt');
	
		$post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
	
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/oauth/token'); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
		curl_setopt($ch, CURLOPT_POST,           1 ); 
		curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
		$result=curl_exec ($ch); 
		 
		curl_close($ch); 
	
		$obj = json_decode($result);
		// $token_type=$obj->token_type;
		$access_token= $obj->access_token;
		// $tokennya = $obj->token_type.' '.$obj->access_token;
		$headers2 = array();
		$headers2[] = 'Authorization: Bearer '.$access_token;
		//$headers2[] = 'Authorization: Bearer '.$obj->access_token;
		#echo $access_token;
	
	
		$curl2 = curl_init();
	
		curl_setopt($curl2, CURLOPT_URL, "https://sisdm.semarangkota.go.id/api/mantel/pegawai/api/pegawai?keyword=$nip_data");
	
	
		curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers2);
			
		$result22 = curl_exec($curl2);
		curl_close($curl2);
	
		$profile2 = json_decode($result22, TRUE);
	
		$login_id        		= $this->session->userdata('id');
	
		$arr 	= array(
							'golongan' 		=> NULL,
							'jabatan' 		=> NULL,
							'lokasi_kerja'	=> NULL,
							'nama' 			=> NULL,
							'nip' 			=> NULL,
							'no_hp' 		=> NULL,
							'opd' 			=> NULL,
							'pangkat' 		=> NULL,
							'unit_kerja' 	=> NULL,
							'avatar' 		=> NULL,
							'status' 		=> NULL,
							'login_id'		=> $login_id
						  );
	
			if ($profile2['data'] != []) {
				$data 	= $profile2['data'][0];
				$arr_id	= array('login_id' => $login_id);
				$array	= array_merge($data,$arr_id);
				
			} else {
				$array 	= $arr;
			
			}

			echo json_encode($array);
	}

	public function get_asn_by_nip($nip_data){
		// $is_atasan_plt 	= $this->input->get('is_atasan_plt');
	
		$post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
	
		$ch = curl_init(); 
		 curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token;
		// $tokennya = $obj->token_type.' '.$obj->access_token;
		$headers2 = array();
		$headers2[] = 'Authorization: Bearer '.$access_token;
		//$headers2[] = 'Authorization: Bearer '.$obj->access_token;
		#echo $access_token;
	
	
		$curl2 = curl_init();
	
		curl_setopt_array($curl2, array(
		  CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/pegawai?keyword='.$nip_data,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_SSL_VERIFYHOST => 0, 
          CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => $headers2,
		));

		$result22 = curl_exec($curl2);
		curl_close($curl2);
	
		$profile2 = json_decode($result22, TRUE);
	
		return $profile2['data'][0];
	}

	/**
	* Add New Asn Talentas
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('asn_talenta_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		

		$this->form_validation->set_rules('nip', 'Nip', 'trim|required|max_length[20]');
		

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[50]');
		

		$this->form_validation->set_rules('golongan', 'Golongan', 'trim|required|max_length[20]');
		

		$this->form_validation->set_rules('pangkat', 'Pangkat', 'trim|required|max_length[20]');
		

		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('lokasi_kerja', 'Lokasi Kerja', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('opd', 'Opd', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('avatar', 'Avatar', 'trim|required');
		

		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'created_by' => $this->session->userdata('id'),
			];

			
			
//$save_data['_example'] = $this->input->post('_example');
			



			
			
			$save_asn_talenta = $id = $this->model_asn_talenta->store($save_data);
            

			if ($save_asn_talenta) {
				
				$id = $save_asn_talenta;
				
				
					
				
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_asn_talenta;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/asn_talenta/edit/' . $save_asn_talenta, 'Edit Asn Talenta'),
						anchor('administrator/asn_talenta', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/asn_talenta/edit/' . $save_asn_talenta, 'Edit Asn Talenta')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/asn_talenta');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/asn_talenta');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$this->response($this->data);
	}

	public function get_kuadran($kompetensi=0, $kinerja=0){
		if ($kompetensi >= 80) {
			if ($kinerja >= 80) {
				$kuadran = 9;
			} elseif ($kinerja >= 60) {
				$kuadran = 8;
			} else {
				$kuadran = 6;
			}
		} elseif ($kompetensi >= 60) {
			if ($kinerja >= 80) {
				$kuadran = 7;
			} elseif ($kinerja >= 60) {
				$kuadran = 5;
			} else {
				$kuadran = 3;
			}
		} else {
			if ($kinerja >= 80) {
				$kuadran = 4;
			} elseif ($kinerja >= 60) {
				$kuadran = 2;
			} else {
				$kuadran = 1;
			}
		}

		return $kuadran;
	}

	public function penilaian_save()
	{
		$nip_asn 	= $this->input->post('nip');
		$asn_talenta = $this->db->select('id_asn_talenta')
								->where('nip', $nip_asn)
								->get('asn_talenta')
								->row();

		if (strpos($this->input->post('eselon'), 'III.') !== FALSE) {
			$tipe_eselon = 'III';
		} else if (strpos($this->input->post('eselon'), 'II.') !== FALSE) {
			$tipe_eselon = 'II';
		} else if (strpos($this->input->post('eselon'), 'IV.') !== FALSE) {
			$tipe_eselon = 'IV';
		} else {
			if ($this->input->post('eselon') == 'JFT') {
				$tipe_eselon = 'JFT';
			} else if ($this->input->post('eselon') == 'JFU') {
				$tipe_eselon = 'JFU';
			}
		}

		if (!$asn_talenta) {
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $tipe_eselon,
				'created_by' => $this->session->userdata('id'),
			]; 

			$save_asn_talenta = $this->model_asn_talenta->store($save_data);

			if ($save_asn_talenta) {
				if ($this->input->post('jumlah_jam') == 0 AND $this->input->post('jumlah_menit') == 0) {
					$bobot_absen 	= 40;
				} elseif ($this->input->post('jumlah_jam') < 1) {
					if ($this->input->post('jumlah_menit') < 31) {
						$bobot_absen 	= 0.8*40;
					} else {
						$bobot_absen 	= 0.7*40;
					}
				} elseif ($this->input->post('jumlah_jam') < 2) {
					$bobot_absen 	= 0.6*40;
				} else {
					$bobot_absen 	= 0.5*40;
				}

				$bobot_uji_kompetensi = $this->input->post('nilai_akhir_uji_kompetensi')*0.4;
				$bobot_cat = $this->input->post('nilai_akhir_individu')*0.2;

				$arr = array(
						'nip' => $this->input->post('nip'),
						'nilai_kemampuan_berpikir' => $this->input->post('nilai_kemampuan_berpikir'),
						'nilai_kepribadian' => $this->input->post('nilai_kepribadian'),
						'nilai_sikap_kerja' => $this->input->post('nilai_sikap_kerja'),
						'nilai_akhir_individu' => $this->input->post('nilai_akhir_individu'),
						'nilai_integritas' => $this->input->post('nilai_integritas'),
						'nilai_kerjasama' => $this->input->post('nilai_kerjasama'),
						'nilai_komunikasi_lisan' => $this->input->post('nilai_komunikasi_lisan'),
						'nilai_mengelola_perubahan' => $this->input->post('nilai_mengelola_perubahan'),
						'nilai_orientasi_pada_hasil' => $this->input->post('nilai_orientasi_pada_hasil'),
						'nilai_pelayanan_publik' => $this->input->post('nilai_pelayanan_publik'),
						'nilai_pengembangan_orang_lain' => $this->input->post('nilai_pengembangan_orang_lain'),
						'nilai_perekat_bangsa' => $this->input->post('nilai_perekat_bangsa'),
						'nilai_pengambilan_keputusan' => $this->input->post('nilai_pengambilan_keputusan'),
						'nilai_akhir_uji_kompetensi' => $this->input->post('nilai_akhir_uji_kompetensi'),
						'saran_diklat_pengembangan' => $this->input->post('saran_diklat_pengembangan'),
						'kompetensi_yang_perlu_dikembangkan' => $this->input->post('kompetensi_yang_perlu_dikembangkan'),
						'klasifikasi_uji_potensi' => $this->input->post('klasifikasi_uji_potensi'),
						'klasifikasi_cat_potensi' => $this->input->post('klasifikasi_cat_potensi'),
						'jumlah_jam' => $this->input->post('jumlah_jam'),
						'jumlah_menit' => $this->input->post('jumlah_menit'),
						'bobot_pendidikan' => $this->input->post('bobot_pendidikan'),
						'bobot_jabatan' => $this->input->post('bobot_jabatan'),
						'bobot_diklat' => $this->input->post('bobot_diklat'),
						'bobot_seminar' => $this->input->post('bobot_seminar'),
						'nilai_skp' => $this->input->post('nilai_skp'),
						'bobot_skp' => $this->input->post('bobot_skp'),
						'bobot_absen' => $bobot_absen,
						'bobot_uji_kompetensi' => $bobot_uji_kompetensi,
						'bobot_cat' => $bobot_cat,
						'bobot_hukdis' => $this->input->post('bobot_hukdis'));

				$this->db->insert('asn_talenta_penilaian', $arr);

				$kompetensi 	= $this->input->post('bobot_pendidikan') + $this->input->post('bobot_jabatan') + $bobot_uji_kompetensi + $bobot_cat + $this->input->post('bobot_diklat') + $this->input->post('bobot_seminar');
				$kinerja 		= $bobot_absen + $this->input->post('bobot_skp') + $this->input->post('bobot_hukdis');

				$kuadran 		= $this->get_kuadran($kompetensi, $kinerja);

				$arr_talent 	= ['kompetensi' => $kompetensi, 'kinerja' => $kinerja, 'kuadran' => $kuadran];
				$this->db->where('nip', $this->input->post('nip'));
				$this->db->update('asn_talenta', $arr_talent);

				set_message('Penilaian Berhasil', 'success');
			} else {
	            set_message('Penilaian Gagal', 'warning');
	        }

			redirect_back();
		} else {
			if (strpos($this->input->post('eselon'), 'III.') !== FALSE) {
				$tipe_eselon = 'III';
			} else if (strpos($this->input->post('eselon'), 'II.') !== FALSE) {
				$tipe_eselon = 'II';
			} else if (strpos($this->input->post('eselon'), 'IV.') !== FALSE) {
				$tipe_eselon = 'IV';
			} else {
				if ($this->input->post('eselon') == 'JFT') {
					$tipe_eselon = 'JFT';
				} else if ($this->input->post('eselon') == 'JFU') {
					$tipe_eselon = 'JFU';
				}
			}
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $tipe_eselon,
			];

			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta', $save_data);

			if ($this->input->post('jumlah_jam') == 0 AND $this->input->post('jumlah_menit') == 0) {
					$bobot_absen 	= 40;
				} elseif ($this->input->post('jumlah_jam') < 1) {
					if ($this->input->post('jumlah_menit') < 31) {
						$bobot_absen 	= 0.8*40;
					} else {
						$bobot_absen 	= 0.7*40;
					}
				} elseif ($this->input->post('jumlah_jam') < 2) {
					$bobot_absen 	= 0.6*40;
				} else {
					$bobot_absen 	= 0.5*40;
				}

				$bobot_uji_kompetensi = $this->input->post('nilai_akhir_uji_kompetensi')*0.4;
				$bobot_cat = $this->input->post('nilai_akhir_individu')*0.2;

				$arr = array(
						'nilai_kemampuan_berpikir' => $this->input->post('nilai_kemampuan_berpikir'),
						'nilai_kepribadian' => $this->input->post('nilai_kepribadian'),
						'nilai_sikap_kerja' => $this->input->post('nilai_sikap_kerja'),
						'nilai_akhir_individu' => $this->input->post('nilai_akhir_individu'),
						'nilai_integritas' => $this->input->post('nilai_integritas'),
						'nilai_kerjasama' => $this->input->post('nilai_kerjasama'),
						'nilai_komunikasi_lisan' => $this->input->post('nilai_komunikasi_lisan'),
						'nilai_mengelola_perubahan' => $this->input->post('nilai_mengelola_perubahan'),
						'nilai_orientasi_pada_hasil' => $this->input->post('nilai_orientasi_pada_hasil'),
						'nilai_pelayanan_publik' => $this->input->post('nilai_pelayanan_publik'),
						'nilai_pengembangan_orang_lain' => $this->input->post('nilai_pengembangan_orang_lain'),
						'nilai_perekat_bangsa' => $this->input->post('nilai_perekat_bangsa'),
						'nilai_pengambilan_keputusan' => $this->input->post('nilai_pengambilan_keputusan'),
						'nilai_akhir_uji_kompetensi' => $this->input->post('nilai_akhir_uji_kompetensi'),
						'saran_diklat_pengembangan' => $this->input->post('saran_diklat_pengembangan'),
						'kompetensi_yang_perlu_dikembangkan' => $this->input->post('kompetensi_yang_perlu_dikembangkan'),
						'klasifikasi_uji_potensi' => $this->input->post('klasifikasi_uji_potensi'),
						'klasifikasi_cat_potensi' => $this->input->post('klasifikasi_cat_potensi'),
						'jumlah_jam' => $this->input->post('jumlah_jam'),
						'jumlah_menit' => $this->input->post('jumlah_menit'),
						'bobot_pendidikan' => $this->input->post('bobot_pendidikan'),
						'bobot_jabatan' => $this->input->post('bobot_jabatan'),
						'bobot_diklat' => $this->input->post('bobot_diklat'),
						'bobot_seminar' => $this->input->post('bobot_seminar'),
						'nilai_skp' => $this->input->post('nilai_skp'),
						'bobot_skp' => $this->input->post('bobot_skp'),
						'bobot_absen' => $bobot_absen,
						'bobot_uji_kompetensi' => $bobot_uji_kompetensi,
						'bobot_cat' => $bobot_cat,
						'bobot_hukdis' => $this->input->post('bobot_hukdis'));

				$this->db->where('nip', $this->input->post('nip'));
				$this->db->update('asn_talenta_penilaian', $arr);

				$kompetensi 	= $this->input->post('bobot_pendidikan') + $this->input->post('bobot_jabatan') + $bobot_uji_kompetensi + $bobot_cat + $this->input->post('bobot_diklat') + $this->input->post('bobot_seminar');
				$kinerja 		= $bobot_absen + $this->input->post('bobot_skp') + $this->input->post('bobot_hukdis');

				// echo 'pend ='.$this->input->post('bobot_pendidikan').' jabatan ='.$this->input->post('bobot_jabatan').' uji kompetensi ='.$bobot_uji_kompetensi.' cat = '.$bobot_cat. ' diklat = '.$this->input->post('bobot_diklat').' seminar ='.$this->input->post('bobot_seminar'). '  kom = '.$kompetensi;die();

				$kuadran 		= $this->get_kuadran($kompetensi, $kinerja);

				$arr_talent 	= ['kompetensi' => $kompetensi, 'kinerja' => $kinerja, 'kuadran' => $kuadran];
				$this->db->where('nip', $this->input->post('nip'));
				$this->db->update('asn_talenta', $arr_talent);

				set_message('Penilaian Berhasil', 'success');
				redirect_back();
			}
	}

	public function penilaian_save_eselon()
	{
		$nip_asn 	= $this->input->post('nip');
		$asn_talenta = $this->db->select('id_asn_talenta')
								->where('nip', $nip_asn)
								->get('asn_talenta')
								->row();

		if (strpos($this->input->post('eselon'), 'II.') !== FALSE) {
			$tipe_eselon = 'II';
		}

		if (!$asn_talenta) {
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $tipe_eselon,
				'created_by' => $this->session->userdata('id'),
			]; 

			$save_asn_talenta = $this->model_asn_talenta->store($save_data);

			if ($save_asn_talenta) {
				if ($this->input->post('jumlah_jam') == 0 AND $this->input->post('jumlah_menit') == 0) {
					$bobot_absen 	= 40;
				} elseif ($this->input->post('jumlah_jam') < 1) {
					if ($this->input->post('jumlah_menit') < 31) {
						$bobot_absen 	= 0.8*40;
					} else {
						$bobot_absen 	= 0.7*40;
					}
				} elseif ($this->input->post('jumlah_jam') < 2) {
					$bobot_absen 	= 0.6*40;
				} else {
					$bobot_absen 	= 0.5*40;
				}

				$arr = array(
						'nip' => $this->input->post('nip'),
						'jumlah_jam' => $this->input->post('jumlah_jam'),
						'jumlah_menit' => $this->input->post('jumlah_menit'),
						'nilai_skp' => $this->input->post('nilai_skp'),
						'bobot_skp' => $this->input->post('bobot_skp'),
						'bobot_absen' => $bobot_absen,
						'bobot_hukdis' => $this->input->post('bobot_hukdis'),
						'nilai_seleksi_kompetensi' => $this->input->post('nilai_seleksi_kompetensi'),
						'uji_gagasan_tertulis' => $this->input->post('uji_gagasan_tertulis'),
						'uji_gagasan_lisan' => $this->input->post('uji_gagasan_lisan'),
						'nilai_rekam_jejak' => $this->input->post('nilai_rekam_jejak'),
						'klasifikasi_seleksi_kompetensi' => $this->input->post('klasifikasi_seleksi_kompetensi'),
					);

				$this->db->insert('asn_talenta_penilaian', $arr);

				$kompetensi 	= ($this->input->post('nilai_seleksi_kompetensi')*0.25) + ($this->input->post('uji_gagasan_tertulis')*0.2) + ($this->input->post('uji_gagasan_lisan')*0.35) + (($this->input->post('nilai_rekam_jejak')/45)*20);
				$kinerja 		= $bobot_absen + $this->input->post('bobot_skp') + $this->input->post('bobot_hukdis');

				$kuadran 		= $this->get_kuadran($kompetensi, $kinerja);

				$arr_talent 	= ['kompetensi' => $kompetensi, 'kinerja' => $kinerja, 'kuadran' => $kuadran];
				$this->db->where('nip', $this->input->post('nip'));
				$this->db->update('asn_talenta', $arr_talent);

				set_message('Penilaian Berhasil', 'success');
			} else {
	            set_message('Penilaian Gagal', 'warning');
	        }

			redirect_back();
		} else {
			if (strpos($this->input->post('eselon'), 'II.') !== FALSE) {
				$tipe_eselon = 'II';
			}
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $tipe_eselon,
				'created_by' => $this->session->userdata('id'),
			]; 

			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta', $save_data);

			if ($this->input->post('jumlah_jam') == 0 AND $this->input->post('jumlah_menit') == 0) {
				$bobot_absen 	= 40;
			} elseif ($this->input->post('jumlah_jam') < 1) {
				if ($this->input->post('jumlah_menit') < 31) {
					$bobot_absen 	= 0.8*40;
				} else {
					$bobot_absen 	= 0.7*40;
				}
			} elseif ($this->input->post('jumlah_jam') < 2) {
				$bobot_absen 	= 0.6*40;
			} else {
				$bobot_absen 	= 0.5*40;
			}

			$bobot_uji_kompetensi = $this->input->post('nilai_akhir_uji_kompetensi')*0.4;
			$bobot_cat = $this->input->post('nilai_akhir_individu')*0.2;

			$arr = array(
					'nip' => $this->input->post('nip'),
					'jumlah_jam' => $this->input->post('jumlah_jam'),
					'jumlah_menit' => $this->input->post('jumlah_menit'),
					'nilai_skp' => $this->input->post('nilai_skp'),
					'bobot_skp' => $this->input->post('bobot_skp'),
					'bobot_absen' => $bobot_absen,
					'bobot_hukdis' => $this->input->post('bobot_hukdis'),
					'nilai_seleksi_kompetensi' => $this->input->post('nilai_seleksi_kompetensi'),
					'uji_gagasan_tertulis' => $this->input->post('uji_gagasan_tertulis'),
					'uji_gagasan_lisan' => $this->input->post('uji_gagasan_lisan'),
					'nilai_rekam_jejak' => $this->input->post('nilai_rekam_jejak'),
					'klasifikasi_seleksi_kompetensi' => $this->input->post('klasifikasi_seleksi_kompetensi'),
				);

			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta_penilaian', $arr);

			$kompetensi 	= ($this->input->post('nilai_seleksi_kompetensi')*0.25) + ($this->input->post('uji_gagasan_tertulis')*0.2) + ($this->input->post('uji_gagasan_lisan')*0.35) + (($this->input->post('nilai_rekam_jejak')/45)*20);
			$kinerja 		= $bobot_absen + $this->input->post('bobot_skp') + $this->input->post('bobot_hukdis');

			// echo 'nilai_seleksi_kom ='.$this->input->post('nilai_seleksi_kompetensi')*0.25.' makalah ='.$this->input->post('nilai_penulisan_makalah')*0.2.' wawancara ='.$nilai_presentasi_wawancara*0.35.' rekam ='.($this->input->post('nilai_rekam_jejak')*20/45). '  kom = '.$kompetensi;die();

			$kuadran 		= $this->get_kuadran($kompetensi, $kinerja);

			$arr_talent 	= ['kompetensi' => $kompetensi, 'kinerja' => $kinerja, 'kuadran' => $kuadran];
			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta', $arr_talent);
		}
		set_message('Penilaian Berhasil', 'success');
		redirect_back();
	}

	public function penilaian_save_eselon_tanpa_rekam()
	{
		$nip_asn 	= $this->input->post('nip');
		$asn_talenta = $this->db->select('id_asn_talenta')
								->where('nip', $nip_asn)
								->get('asn_talenta')
								->row();

		if (strpos($this->input->post('eselon'), 'II.') !== FALSE) {
			$tipe_eselon = 'II';
		}

		if (!$asn_talenta) {
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $tipe_eselon,
				'created_by' => $this->session->userdata('id'),
			]; 

			$save_asn_talenta = $this->model_asn_talenta->store($save_data);

			if ($save_asn_talenta) {
				if ($this->input->post('jumlah_jam') == 0 AND $this->input->post('jumlah_menit') == 0) {
					$bobot_absen 	= 40;
				} elseif ($this->input->post('jumlah_jam') < 1) {
					if ($this->input->post('jumlah_menit') < 31) {
						$bobot_absen 	= 0.8*40;
					} else {
						$bobot_absen 	= 0.7*40;
					}
				} elseif ($this->input->post('jumlah_jam') < 2) {
					$bobot_absen 	= 0.6*40;
				} else {
					$bobot_absen 	= 0.5*40;
				}

				$arr = array(
						'nip' => $this->input->post('nip'),
						'jumlah_jam' => $this->input->post('jumlah_jam'),
						'jumlah_menit' => $this->input->post('jumlah_menit'),
						'nilai_skp' => $this->input->post('nilai_skp'),
						'bobot_skp' => $this->input->post('bobot_skp'),
						'bobot_absen' => $bobot_absen,
						'bobot_hukdis' => $this->input->post('bobot_hukdis'),
						'nilai_evaluasi_kinerja' => $this->input->post('nilai_evaluasi_kinerja'),
						'nilai_uji_gagasan_tertulis' => $this->input->post('nilai_uji_gagasan_tertulis'),
						'nilai_paparan' => $this->input->post('nilai_paparan'),
						'nilai_wawancara' => $this->input->post('nilai_wawancara'),
						'klasifikasi_seleksi_kompetensi' => $this->input->post('klasifikasi_seleksi_kompetensi'),
					);

				$this->db->insert('asn_talenta_penilaian', $arr);

				$kompetensi 	= ($this->input->post('nilai_evaluasi_kinerja')*0.3) + ($this->input->post('nilai_uji_gagasan_tertulis')*0.25) + ($this->input->post('nilai_paparan')*0.2) + ($this->input->post('nilai_wawancara')*0.25);
				$kinerja 		= $bobot_absen + $this->input->post('bobot_skp') + $this->input->post('bobot_hukdis');

				$kuadran 		= $this->get_kuadran($kompetensi, $kinerja);

				$arr_talent 	= ['kompetensi' => $kompetensi, 'kinerja' => $kinerja, 'kuadran' => $kuadran];
				$this->db->where('nip', $this->input->post('nip'));
				$this->db->update('asn_talenta', $arr_talent);

				set_message('Penilaian Berhasil', 'success');
			} else {
	            set_message('Penilaian Gagal', 'warning');
	        }

			redirect_back();
		} else {
			if (strpos($this->input->post('eselon'), 'II.') !== FALSE) {
				$tipe_eselon = 'II';
			}
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'kelas_jabatan' => $this->input->post('kelas_jabatan'),
				'nilai_jabatan' => $this->input->post('nilai_jabatan'),
				'eselon' => $this->input->post('eselon'),
				'tipe_eselon' => $tipe_eselon,
				'created_by' => $this->session->userdata('id'),
			]; 

			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta', $save_data);

			if ($this->input->post('jumlah_jam') == 0 AND $this->input->post('jumlah_menit') == 0) {
				$bobot_absen 	= 40;
			} elseif ($this->input->post('jumlah_jam') < 1) {
				if ($this->input->post('jumlah_menit') < 31) {
					$bobot_absen 	= 0.8*40;
				} else {
					$bobot_absen 	= 0.7*40;
				}
			} elseif ($this->input->post('jumlah_jam') < 2) {
				$bobot_absen 	= 0.6*40;
			} else {
				$bobot_absen 	= 0.5*40;
			}

			$bobot_uji_kompetensi = $this->input->post('nilai_akhir_uji_kompetensi')*0.4;
			$bobot_cat = $this->input->post('nilai_akhir_individu')*0.2;

			$arr = array(
					'nip' => $this->input->post('nip'),
					'jumlah_jam' => $this->input->post('jumlah_jam'),
					'jumlah_menit' => $this->input->post('jumlah_menit'),
					'nilai_skp' => $this->input->post('nilai_skp'),
					'bobot_skp' => $this->input->post('bobot_skp'),
					'bobot_absen' => $bobot_absen,
					'bobot_hukdis' => $this->input->post('bobot_hukdis'),
					'nilai_evaluasi_kinerja' => $this->input->post('nilai_evaluasi_kinerja'),
					'nilai_uji_gagasan_tertulis' => $this->input->post('nilai_uji_gagasan_tertulis'),
					'nilai_paparan' => $this->input->post('nilai_paparan'),
					'nilai_wawancara' => $this->input->post('nilai_wawancara'),
					'klasifikasi_seleksi_kompetensi' => $this->input->post('klasifikasi_seleksi_kompetensi'),
				);

			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta_penilaian', $arr);

			$kompetensi 	= ($this->input->post('nilai_evaluasi_kinerja')*0.3) + ($this->input->post('nilai_uji_gagasan_tertulis')*0.25) + ($this->input->post('nilai_paparan')*0.2) + ($this->input->post('nilai_wawancara')*0.25);
			$kinerja 		= $bobot_absen + $this->input->post('bobot_skp') + $this->input->post('bobot_hukdis');

			// echo 'nilai_seleksi_kom ='.$this->input->post('nilai_seleksi_kompetensi')*0.25.' makalah ='.$this->input->post('nilai_penulisan_makalah')*0.2.' wawancara ='.$nilai_presentasi_wawancara*0.35.' rekam ='.($this->input->post('nilai_rekam_jejak')*20/45). '  kom = '.$kompetensi;die();

			$kuadran 		= $this->get_kuadran($kompetensi, $kinerja);

			$arr_talent 	= ['kompetensi' => $kompetensi, 'kinerja' => $kinerja, 'kuadran' => $kuadran];
			$this->db->where('nip', $this->input->post('nip'));
			$this->db->update('asn_talenta', $arr_talent);
		}
		set_message('Penilaian Berhasil', 'success');
		redirect_back();
	}
	
		/**
	* Update view Asn Talentas
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('asn_talenta_update');

		$this->data['asn_talenta'] = $this->model_asn_talenta->find($id);

		$this->template->title('Asn Talenta Update');
		$this->render('backend/standart/administrator/asn_talenta/asn_talenta_update', $this->data);
	}

	public function penilaian($id)
	{
		$this->is_allowed('asn_talenta_penilaian');

		$asn 							  = $this->model_asn_talenta->find($id);
		$this->data['asn_talenta'] 		  = $asn;
		$this->data['riwayat_pendidikan'] = $this->riwayat_pendidikan($asn->nip);
		$this->data['riwayat_jabatan'] 	  = $this->riwayat_jabatan($asn->nip);
		$this->data['riwayat_kompetensi'] = $this->riwayat_kompetensi($asn->nip);
		$this->data['riwayat_skp'] 		  = $this->riwayat_skp($asn->nip);
		$this->data['riwayat_hukdis'] 	  = $this->riwayat_hukdis($asn->nip);

		$this->template->title('Asn Talenta Update');
		$this->render('backend/standart/administrator/asn_talenta/asn_talenta_penilaian', $this->data);
	}

	public function penilaian_by_nip()
	{
		$this->is_allowed('asn_talenta_penilaian');

		$nip 	   	   = $this->input->get('nip');
  		$kelas_jabatan = $this->input->get('kelas');
  		$nilai_jabatan = $this->input->get('nilai');
  		$asn_talenta_penilaian 	= $this->db->where('nip', $nip)
  										   ->get('asn_talenta_penilaian')
  										   ->row();

  		$detail_asn   = $this->get_asn_by_nip($nip);

		$this->data['detail_asn'] 		  = $detail_asn;
		$eselon 						  = $detail_asn['eselon'];
		$this->data['riwayat_pendidikan'] = $this->riwayat_pendidikan($nip);
		$this->data['riwayat_jabatan'] 	  = $this->riwayat_jabatan($nip);
		$this->data['riwayat_kompetensi'] = $this->riwayat_kompetensi($nip);
		$this->data['riwayat_skp'] 		  = $this->riwayat_skp($nip);
		$this->data['riwayat_hukdis'] 	  = $this->riwayat_hukdis($nip);
		$this->data['kelas_jabatan'] 	  = $kelas_jabatan;
		$this->data['nilai_jabatan'] 	  = $nilai_jabatan;
		$this->data['asn_talenta_penilaian'] 	  = $asn_talenta_penilaian;

		if (strpos($eselon, 'III.') !== FALSE) {
			$tipe_eselon = 'III';
		} else if (strpos($eselon, 'II.') !== FALSE) {
			$tipe_eselon = 'II';
		} else if (strpos($eselon, 'IV.') !== FALSE) {
			$tipe_eselon = 'IV';
		} else {
			if ($eselon == 'JFT') {
				$tipe_eselon = 'JFT';
			} else if ($eselon == 'JFU') {
				$tipe_eselon = 'JFU';
			}
		}

		if ($tipe_eselon == 'II') {
			$this->template->title('Asn Talenta Update');
			$this->render('backend/standart/administrator/asn_talenta/asn_talenta_penilaian_by_nip_eselon', $this->data);
		} else {
			$this->template->title('Asn Talenta Update');
			$this->render('backend/standart/administrator/asn_talenta/asn_talenta_penilaian_by_nip', $this->data);
		}		
	}

	public function penilaian_by_nip_tanpa_rekam_jejak()
	{
		$this->is_allowed('asn_talenta_penilaian');

		$nip 	   	   = $this->input->get('nip');
  		$kelas_jabatan = $this->input->get('kelas');
  		$nilai_jabatan = $this->input->get('nilai');
  		$asn_talenta_penilaian 	= $this->db->where('nip', $nip)
  										   ->get('asn_talenta_penilaian')
  										   ->row();

  		$detail_asn   = $this->get_asn_by_nip($nip);

		$this->data['detail_asn'] 		  = $detail_asn;
		$eselon 						  = $detail_asn['eselon'];
		$this->data['riwayat_pendidikan'] = $this->riwayat_pendidikan($nip);
		$this->data['riwayat_jabatan'] 	  = $this->riwayat_jabatan($nip);
		$this->data['riwayat_kompetensi'] = $this->riwayat_kompetensi($nip);
		$this->data['riwayat_skp'] 		  = $this->riwayat_skp($nip);
		$this->data['riwayat_hukdis'] 	  = $this->riwayat_hukdis($nip);
		$this->data['kelas_jabatan'] 	  = $kelas_jabatan;
		$this->data['nilai_jabatan'] 	  = $nilai_jabatan;
		$this->data['asn_talenta_penilaian'] 	  = $asn_talenta_penilaian;

		$this->template->title('Asn Talenta Update');
		$this->render('backend/standart/administrator/asn_talenta/penilaian_by_nip_tanpa_rekam_jejak', $this->data);		
	}

	/**
	* Update Asn Talentas
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('asn_talenta_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
				$this->form_validation->set_rules('nip', 'Nip', 'trim|required|max_length[20]');
		

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[50]');
		

		$this->form_validation->set_rules('golongan', 'Golongan', 'trim|required|max_length[20]');
		

		$this->form_validation->set_rules('pangkat', 'Pangkat', 'trim|required|max_length[20]');
		

		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('lokasi_kerja', 'Lokasi Kerja', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('opd', 'Opd', 'trim|required|max_length[100]');
		

		$this->form_validation->set_rules('avatar', 'Avatar', 'trim|required');
		

		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		

		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[11]');
		

		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'golongan' => $this->input->post('golongan'),
				'pangkat' => $this->input->post('pangkat'),
				'jabatan' => $this->input->post('jabatan'),
				'lokasi_kerja' => $this->input->post('lokasi_kerja'),
				'unit_kerja' => $this->input->post('unit_kerja'),
				'opd' => $this->input->post('opd'),
				'avatar' => $this->input->post('avatar'),
				'status' => $this->input->post('status'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => date('Y-m-d H:i:s'),
			];

			

			
//$save_data['_example'] = $this->input->post('_example');
			


			
			
			$save_asn_talenta = $this->model_asn_talenta->change($id, $save_data);

			if ($save_asn_talenta) {

				
				

				
				
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/asn_talenta', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/asn_talenta');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/asn_talenta');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		$this->response($this->data);
	}
	
	/**
	* delete Asn Talentas
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('asn_talenta_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'asn_talenta'), 'success');
        } else {
            set_message(cclang('error_delete', 'asn_talenta'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Asn Talentas
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('asn_talenta_view');

		$this->data['asn_talenta'] = $this->model_asn_talenta->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Asn Talenta Detail');
		$this->render('backend/standart/administrator/asn_talenta/asn_talenta_view', $this->data);
	}

	public function mantap()
    {
    	$data['id_asn_talenta'] 	= $this->input->get('id_asn_talenta');
    	$asn_talenta 	= $this->db->select('nama, nip, golongan, jabatan, pangkat, lokasi_kerja, avatar, kompetensi, kinerja, kelas_jabatan, nilai_jabatan, kuadran')
    							   ->where('id_asn_talenta', $this->input->get('id_asn_talenta'))
    							   ->get('asn_talenta')
    							   ->row();

    	$penilaian 		= $this->db->select('nilai_akhir_individu, klasifikasi_cat_potensi, nilai_akhir_uji_kompetensi, klasifikasi_uji_potensi')
    							   ->where('nip', $asn_talenta->nip)
    							   ->get('asn_talenta_penilaian')
    							   ->row();

    	$ws_asn 		= $this->ws_talenta($asn_talenta->nip);

    	$data['asn_talenta'] 	= $asn_talenta;
    	$data['ws_asn'] 	= $ws_asn;
    	$data['penilaian'] 	= $penilaian;

    	// $this->model_perjanjian_kinerja->expor_pk($id);
        $this->load->view('mantap', $data);
    }

    public function ws_talenta($nip){
    	$bearer 	= $this->get_bearer();
    	$ret 		= array();

    	$riwayat_pendidikan = $this->pendidikan($bearer, $nip);
    	$riwayat_jabatan 	= $this->jabatan($bearer, $nip);
    	$riwayat_kompetensi = $this->kompetensi($bearer, $nip);

    	$ret['pendidikan'] 	= $riwayat_pendidikan;
    	$ret['jabatan'] 	= $riwayat_jabatan;
    	$ret['kompetensi'] 	= $riwayat_kompetensi;

    	return $ret;
    }

    public function pendidikan($bearer=null, $nip=null){
    	$curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_pendidikan',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0, 
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $bearer
          ),
        ));

        $response = curl_exec($curl2);//echo json_encode($response);die;

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function jabatan($bearer=null, $nip=null){
    	$curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_jabatan',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0, 
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $bearer
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function kompetensi($bearer=null, $nip=null){
    	$curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_kompetensi',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0, 
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $bearer
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function get_bearer(){
    	$post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        return $headers2;
    }
	
	/**
	* delete Asn Talentas
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$asn_talenta = $this->model_asn_talenta->find($id);

		
		
		return $this->model_asn_talenta->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('asn_talenta_export');

		$this->model_asn_talenta->export(
			'asn_talenta', 
			'asn_talenta',
			$this->model_asn_talenta->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('asn_talenta_export');

		$this->model_asn_talenta->pdf('asn_talenta', 'asn_talenta');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('asn_talenta_export');

		$table = $title = 'asn_talenta';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_asn_talenta->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file asn_talenta.php */
/* Location: ./application/controllers/administrator/Asn Talenta.php */