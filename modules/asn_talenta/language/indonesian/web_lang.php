<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['asn_talenta'] = 'Asn Talenta';
$lang['id_asn_talenta'] = 'Id Asn Talenta';
$lang['nip'] = 'Nip';
$lang['nama'] = 'Nama';
$lang['golongan'] = 'Golongan';
$lang['pangkat'] = 'Pangkat';
$lang['jabatan'] = 'Jabatan';
$lang['lokasi_kerja'] = 'Lokasi Kerja';
$lang['unit_kerja'] = 'Unit Kerja';
$lang['opd'] = 'Opd';
$lang['avatar'] = 'Avatar';
$lang['status'] = 'Status';
$lang['created_by'] = 'Created By';
$lang['created_at'] = 'Created At';
