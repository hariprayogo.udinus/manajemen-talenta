<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_asn_talenta extends MY_Model {

    private $primary_key    = 'id_asn_talenta';
    private $table_name     = 'asn_talenta';
    public $field_search   = ['nip', 'nama', 'golongan', 'pangkat', 'jabatan', 'lokasi_kerja', 'unit_kerja', 'opd', 'avatar', 'status', 'created_by', 'created_at', 'aauth_users.full_name'];
    public $sort_option = ['nip', 'asc'];
    
    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
            'sort_option'   => $this->sort_option,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                $f_search = "asn_talenta.".$field;

                if (strpos($field, '.')) {
                    $f_search = $field;
                }
                if ($iterasi == 1) {
                    $where .=  $f_search . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " .  $f_search . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "asn_talenta.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                $f_search = "asn_talenta.".$field;
                if (strpos($field, '.')) {
                    $f_search = $field;
                }

                if ($iterasi == 1) {
                    $where .= $f_search . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " .$f_search . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "asn_talenta.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('aauth_users', 'aauth_users.id = asn_talenta.created_by', 'LEFT');
        
        $this->db->select('asn_talenta.*,aauth_users.username as aauth_users_name,aauth_users.full_name as name');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

    public function get_list_opd(){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/list_opd',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function get_list_pegawai($kode){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai_by_opd/'.$kode,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }


    public function get_riwayat_jabatan($nip){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_jabatan',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function get_riwayat_pendidikan($nip){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_pendidikan',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function get_riwayat_kompetensi($nip){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_kompetensi',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function get_riwayat_skp($nip){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_skp',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    public function get_riwayat_hukdis($nip){
        $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://sisdm.semarangkota.go.id/oauth/token'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_POST,           1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
        $result=curl_exec($ch); 
        curl_close($ch); 
         
        $obj = json_decode($result); 
        $token_type=$obj->token_type; 
        $access_token= $obj->access_token; 
        $tokennya = $obj->token_type.' '.$obj->access_token; 
        $headers2 = array(); 
        $headers2 = 'Authorization: Bearer '.$access_token; 

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
          CURLOPT_URL => 'https://sisdm.semarangkota.go.id/api/mantel/pegawai/'.$nip.'/riwayat_hukdis',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array( $headers2
          ),
        ));

        $response = curl_exec($curl2);

        curl_close($curl2);
        $response = json_decode($response);
        return $response;
    }

    

    
}

/* End of file Model_asn_talenta.php */
/* Location: ./application/models/Model_asn_talenta.php */