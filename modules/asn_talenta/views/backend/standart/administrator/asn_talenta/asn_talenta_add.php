
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<style>
   /* .group-nip */
   .group-nip {

   }

   .group-nip .control-label {

   }

   .group-nip .col-sm-8 {

   }

   .group-nip .form-control {

   }

   .group-nip .help-block {

   }
   /* end .group-nip */




</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asn Talenta        <small><?= cclang('new', ['Asn Talenta']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/asn_talenta'); ?>">Asn Talenta</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Asn Talenta</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Asn Talenta']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name' => 'form_asn_talenta',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_asn_talenta',
                            'enctype' => 'multipart/form-data',
                            'method' => 'POST'
                        ]); ?>
                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>
                                                                                                    <div class="form-group group-nip ">
                                <label for="nip" class="col-sm-2 control-label">Nip                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?= set_value('nip'); ?>" onkeyup="isi_otomatis_asn()">
                                    <small class="info help-block">
                                        <b>Input Nip</b> Max Length : 20.</small>
                                </div>
                            </div>
                        
                            <div id="response" name="response">
                                                                                                                            <div class="form-group group-nama ">
                                <label for="nama" class="col-sm-2 control-label">Nama                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Nama</b> Max Length : 50.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-golongan ">
                                <label for="golongan" class="col-sm-2 control-label">Golongan                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="golongan" id="golongan" placeholder="Golongan" value="<?= set_value('golongan'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Golongan</b> Max Length : 20.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-pangkat ">
                                <label for="pangkat" class="col-sm-2 control-label">Pangkat                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="pangkat" id="pangkat" placeholder="Pangkat" value="<?= set_value('pangkat'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Pangkat</b> Max Length : 20.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-jabatan ">
                                <label for="jabatan" class="col-sm-2 control-label">Jabatan                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?= set_value('jabatan'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Jabatan</b> Max Length : 100.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-lokasi_kerja ">
                                <label for="lokasi_kerja" class="col-sm-2 control-label">Lokasi Kerja                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="lokasi_kerja" id="lokasi_kerja" placeholder="Lokasi Kerja" value="<?= set_value('lokasi_kerja'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Lokasi Kerja</b> Max Length : 100.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-unit_kerja ">
                                <label for="unit_kerja" class="col-sm-2 control-label">Unit Kerja                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="unit_kerja" id="unit_kerja" placeholder="Unit Kerja" value="<?= set_value('unit_kerja'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Unit Kerja</b> Max Length : 100.</small>
                                </div>
                            </div>
                        

                                                                                                                            <div class="form-group group-opd ">
                                <label for="opd" class="col-sm-2 control-label">Opd                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="opd" id="opd" placeholder="Opd" value="<?= set_value('opd'); ?>" readonly>
                                    <input type="hidden" class="form-control" name="avatar" id="avatar" placeholder="Foto" value="<?= set_value('avatar'); ?>" readonly>
                                    <small class="info help-block">
                                        <b>Input Opd</b> Max Length : 100.</small>
                                </div>
                            </div>

                        
                            </div>                                 
                                                

                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>

                            <div class="custom-button-wrapper">

                                                        </div>


                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>

                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>

                            <span class="loading2 loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i>Harap tunggu sedang memuat data...</i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->

<script>
    $(document).ready(function() {

        $('#response').hide();
        
    window.event_submit_and_action = '';
        
    (function(){
    var nip = $('#nip');
   /* 
    nip.on('change', function() {});
    */
    
})()
      

      
      

                    
    $('#btn_cancel').click(function() {
        swal({
                title: "<?= cclang('are_you_sure'); ?>",
                text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = BASE_URL + 'administrator/asn_talenta';
                }
            });

        return false;
    }); /*end btn cancel*/

    $('.btn_save').click(function() {
        $('.message').fadeOut();
        
    var form_asn_talenta = $('#form_asn_talenta');
    var data_post = form_asn_talenta.serializeArray();
    var save_type = $(this).attr('data-stype');

    data_post.push({
        name: 'save_type',
        value: save_type
    });

    data_post.push({
        name: 'event_submit_and_action',
        value: window.event_submit_and_action
    });

    (function(){
    data_post.push({
        name : '_example',
        value : 'value_of_example',
    })
})()
      

    $('.loading').show();

    $.ajax({
            url: BASE_URL + '/administrator/asn_talenta/add_save',
            type: 'POST',
            dataType: 'json',
            data: data_post,
        })
        .done(function(res) {
            $('form').find('.form-group').removeClass('has-error');
            $('.steps li').removeClass('error');
            $('form').find('.error-input').remove();
            if (res.success) {
                
            if (save_type == 'back') {
                window.location.href = res.redirect;
                return;
            }

            $('.message').printMessage({
                message: res.message
            });
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
            
            } else {
                if (res.errors) {

                    $.each(res.errors, function(index, val) {
                        $('form #' + index).parents('.form-group').addClass('has-error');
                        $('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
                    });
                    $('.steps li').removeClass('error');
                    $('.content section').each(function(index, el) {
                        if ($(this).find('.has-error').length) {
                            $('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
                        }
                    });
                }
                $('.message').printMessage({
                    message: res.message,
                    type: 'warning'
                });
            }

        })
        .fail(function() {
            $('.message').printMessage({
                message: 'Error save data',
                type: 'warning'
            });
        })
        .always(function() {
            $('.loading').hide();
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 2000);
        });

    return false;
    }); /*end btn save*/


    }); /*end doc ready*/
</script>

<script>

function isi_otomatis_asn(){
        var nip = $("#nip").val();
       
        if (nip.length > 17) {

            //swal("Maaf !", "Data tidak ditemukan, pastikan NIP sudah benar", "error");
           //alert('test');
           $('.loading2').show();
          $.ajax({
              url: '<?= base_url('administrator/asn_talenta/get_detail_by_nip') ?>',
              data:"nip="+nip ,
          }).done(function(data) {
           
              var json = data,
              obj = JSON.parse(json);
              var nama_asn = obj.nama;

            //   console.log(nama_asn);
            //   return;
              
              if(nama_asn == null){
                // swal("Maaf !", "Data tidak ditemukan, pastikan NIP sudah benar", "error");
                // location.reload();

                swal({
                    title: "Oops, Gagal Mengambil Data !",
                    text: "NIP yang anda masukkan tidak sesuai, silakan ulangi lagi setelah 3 detik. ",
                    type: "error",
                    timer: 3000,
                    showConfirmButton: false
                    }, function(){
                        // console.log(obj);
                        // return;
                        location.reload();
                    });

              } else {

                $('.loading2').hide();
                $('#response').show();

              $('#nama').val(obj.nama);
              $('#golongan').val(obj.golongan);
              $('#pangkat').val(obj.pangkat);
              $('#jabatan').val(obj.jabatan);
              $('#lokasi_kerja').val(obj.lokasi_kerja);
              $('#unit_kerja').val(obj.unit_kerja);
              $('#opd').val(obj.opd);
              $('#avatar').val(obj.foto);
            //console.log(obj);


              }

              
            
          })
        } else {
           // $('.loading2').show();
                $('#response').hide();
        }
    }
</script>