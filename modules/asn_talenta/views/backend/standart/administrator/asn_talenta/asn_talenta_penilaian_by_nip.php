

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asn Talenta        <small>Edit Asn Talenta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/asn_talenta'); ?>">Asn Talenta</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<style>
   /* .group-nip */
   .group-nip {

   }

   .group-nip .control-label {

   }

   .group-nip .col-sm-8 {

   }

   .group-nip .form-control {

   }

   .group-nip .help-block {

   }
   /* end .group-nip */


   #catkompetensi {
     background-color: #bbd0ff;
    }

    #ujikompetensi {
     background-color: #ffd6ff;
    }

    .nav-tabs-custom li.active {
        border-top-color: #a38ed2 !important;
        border-bottom: none !important;
    }

  #justify {
  width: 100%;
  margin-top: 5px;
  margin-left: 5px;
}

</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Asn Talenta</h3>
                            <h5 class="widget-user-desc">Edit Asn Talenta</h5>
                        </div>
                        <?= form_open(base_url('administrator/asn_talenta/penilaian_save/'.$this->uri->segment(4)), [
                            'name' => 'form_asn_talenta',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_asn_talenta',
                            'method' => 'POST'
                        ]); ?>

                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-widget widget-user-2">
                <div class="widget-user-header" style="background-color: transparent;background-image: linear-gradient(to right, #a38ed2 0, #f9c1eb 75%);    background-repeat: repeat-x;">
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?= $detail_asn['foto'] ?>" alt="User Avatar">
                    </div>

                    <h3 class="widget-user-username"><b><?= ucwords(strtolower($detail_asn['nama'])) ?></b></h3>
                    <h5 class="widget-user-desc"><?= ucwords(strtolower($detail_asn['jabatan'])) ?></h5>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-justified" style = "margin:10px">
                         <li><b><?= $detail_asn['lokasi_kerja'] ?></b></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="fa fa-check-circle"></i> <b>Penilaian Talenta ASN</b></li>
                                <li class="active"><a href="#tab_1" data-toggle="tab">Sumbu Kompetensi</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Sumbu Kinerja</a></li>
                               
                            <br>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1-1" data-toggle="tab">Pendidikan</a></li>
                                            <li><a href="#tab_2-2" data-toggle="tab">Riwayat Jabatan</a></li>
                                            <li><a href="#tab_3-2" data-toggle="tab">CAT Potensi</a></li>
                                            <li><a href="#tab_4-2" data-toggle="tab">Uji Kompetensi</a></li>
                                            <li><a href="#tab_5-2" data-toggle="tab">Pengembangan Kompetensi</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1-1">
                                            <div style="overflow-y: scroll;height: 420px;">
                                            <div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="pendidikan">
                                                <table class="table table-bordered animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Jenjang Pendidikan</th>
                                                                <th>Almamater</th>
                                                                <th>Ijazah</th>
                                                                <th>Transkrip Nilai</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i=0; foreach ($riwayat_pendidikan as $key => $value) { $i++;?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><span class="label label-info"><?= $value->tingkat_pendidikan ?></span>&nbsp;<?= $value->pendidikan ?></td>
                                                                <td><?= $value->nama_sekolah ?></td>
                                                                <td>
                                                                    <a href="<?= $value->file_ijazah ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                                </td>
                                                                <td>
                                                                    <a href="<?= $value->file_transkrip ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div>
                                                
                                            </div><br>
                                            <div class="col-sm-4">
                                                    <div class="small-box" style="background-color: #cfbaff;">
                                                        <div class="inner">
                                                            <?php  
                                                           
                                                                $jenjang_terakhir   = $riwayat_pendidikan[array_key_last($riwayat_pendidikan)]->tingkat_pendidikan;
                                                               
                                                                if ($jenjang_terakhir == 'S-3') {
                                                                    $nilai_pendidikan  = 100;
                                                                } elseif ($jenjang_terakhir == 'S-2') {
                                                                    $nilai_pendidikan  = 90;
                                                                } elseif ($jenjang_terakhir == 'S-1') {
                                                                    $nilai_pendidikan  = 80;
                                                                } elseif ($jenjang_terakhir == 'Diploma IV') {
                                                                    $nilai_pendidikan  = 80;
                                                                } elseif ($jenjang_terakhir == 'Diploma III') {
                                                                    $nilai_pendidikan  = 70;
                                                                } else {
                                                                    $nilai_pendidikan  = 50;
                                                                }
                                                            
                                                                $bobot_pendidikan    = (15/100)*$nilai_pendidikan;
                                                            ?>
                                                            <h3><?= $nilai_pendidikan ?></h3>
                                                            <p>Jenjang Pendidikan</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-graduation-cap"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Nilai Jenjang Pendidikan ASN <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                    <div class="small-box" style="background-color: #f7d0ed;">
                                                        <div class="inner">
                                                            <h3><?= $bobot_pendidikan ?> %</h3>
                                                            <p>Bobot</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Hasil Bobot Nilai Pendidikan <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                            
                                                
                                            </div>

                                            <div class="tab-pane" id="tab_2-2">
                                            <div style="overflow-y: scroll;height: 420px;">
                                            <div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="riwayatpendidikan">
                                                <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Jabatan</th>
                                                                <th>TMT SK</th>
                                                                <th>Lokasi Kerja</th>
                                                                <th>File SK</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i=0; foreach ($riwayat_jabatan as $key => $value) { $i++;?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->jabatan_baru ?></td>
                                                                <td><?= date('d M Y', strtotime($value->tmt_sk)) ?></td>
                                                                <td><?= $value->unit_kerja_baru ?></td>
                                                                <td>
                                                                    <a href="<?= $value->file_jabatan ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div>
                                                
                                            </div><br>
                                            <div class="col-sm-4">
                                            <div class="small-box" style="background-color: #f7d0ed;">
                                                        <div class="inner">
                                                            <h3><?= $kelas_jabatan ?></h3>
                                                            <p>Kelas Jabatan</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-graduation-cap"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Nilai Kelas Jabatan <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                    <div class="small-box" style="background-color: #cfbaff;">
                                                        <div class="inner">
                                                            <h3><?= $nilai_jabatan ?></h3>
                                                            <p>Nilai Jabatan</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Nilai Jabatan <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                    <div class="small-box" style="background-color: #cfbaff;">
                                                        <div class="inner">
                                                            <?php $bobot_jabatan    = round(($nilai_jabatan/3555)*15, 2); ?>
                                                            <h3><?= $bobot_jabatan.' %'; ?></h3>
                                                            <p>Bobot</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Bobot Nilai Kelas Jabatan <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                            
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_3-2">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <table class="table">
                                                        <div class="col-sm-6 animate__animated animate__fadeIn animate__fast">
                                                            <div class="box-body" id="catkompetensi">
                                                                <div class="form-group">
                                                                    
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_kemampuan_berpikir" class="control-label">Nilai Kemampuan Berpikir <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_kemampuan_berpikir" id="nilai_kemampuan_berpikir" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_kemampuan_berpikir : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Kemampuan Berpikir</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                    <label for="nilai_kepribadian" class="control-label">Nilai Kepribadian <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_kepribadian" id="nilai_kepribadian" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_kepribadian : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Kepribadian</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                    <label for="nilai_sikap_kerja" class="control-label">Nilai Sikap Kerja <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_sikap_kerja" id="nilai_sikap_kerja" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_sikap_kerja : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Sikap Kerja</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                               


                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 animate__animated animate__fadeIn animate__slow">
                                                            <div class="box-body" id="catkompetensi">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_akhir_individu" class="control-label">Nilai Akhir Individu <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_akhir_individu" id="nilai_akhir_individu" placeholder="" onkeyup="calcVal()" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_akhir_individu : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Akhir Individu</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_akhir_individu" class="ontrol-label">Klasifikasi <i class="required">*</i>
                                                                        </label>
                                                                        <select class="form-control" aria-label="Klasifikasi Potensi" name="klasifikasi_cat_potensi">
                                                                        <option value="">- Klasifikasi CAT Potensi -</option>
                                                                        <option value="Action Oriented" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_cat_potensi == 'Action Oriented' ? 'selected' : '') : '' ?>>Action Oriented</option>
                                                                        <option value="Thought Oriented" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_cat_potensi == 'Thought Oriented' ? 'selected' : '') : '' ?>>Thought Oriented</option>
                                                                        <option value="People Oriented" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_cat_potensi == 'People Oriented' ? 'selected' : '') : '' ?>>People Oriented</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </table>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_4-2">  
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <table class="table">
                                                        <div class="col-sm-4 animate__animated animate__fadeIn animate__slow">
                                                            <div class="box-body" id="ujikompetensi">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_integritas" class="control-label">Nilai Integritas <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_integritas" id="nilai_integritas" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_integritas : null ?>">
                                                                        <small class="info help-block">
                                                                        <b>Input Nilai Integritas</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_kerjasama" class="control-label">Nilai Kerjasama <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_kerjasama" id="nilai_kerjasama" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_kerjasama : null ?>">
                                                                        <small class="info help-block">
                                                                        <b>Input Nilai Kerjasama</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">                                   
                                                                    <div class="col-sm-12">
                                                                    <label for="nilai_komunikasi_lisan" class="control-label">Nilai Komunikasi Lisan <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_komunikasi_lisan" id="nilai_komunikasi_lisan" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_komunikasi_lisan : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Komunikasi Lisan</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_mengelola_perubahan" class="control-label">  Nilai Mengelola Perubahan <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_mengelola_perubahan" id="nilai_mengelola_perubahan" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_mengelola_perubahan : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Mengelola Perubahan</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 animate__animated animate__fadeIn animate__slow">
                                                            <div class="box-body" id="ujikompetensi">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_orientasi_pada_hasil" class="control-label">Nilai Orientasi pada Hasil <i class="required">*</i>
                                                                            </label>
                                                                        <input type="text" class="form-control" name="nilai_orientasi_pada_hasil" id="nilai_orientasi_pada_hasil" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_orientasi_pada_hasil : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Orientasi pada Hasil</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_pelayanan_publik" class="control-label">Nilai Pelayanan Publik <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_pelayanan_publik" id="nilai_pelayanan_publik" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_pelayanan_publik : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Pelayanan Publik</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_pengembangan_orang_lain" class="control-label">Nilai Pengembangan Orang Lain <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_pengembangan_orang_lain" id="nilai_pengembangan_orang_lain" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_pengembangan_orang_lain : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Pengembangan Orang Lain</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_perekat_bangsa" class="control-label">Nilai Perekat Bangsa <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_perekat_bangsa" id="nilai_perekat_bangsa" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_perekat_bangsa : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Perekat Bangsa</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4 animate__animated animate__fadeIn animate__fast">
                                                            <div class="box-body" id="ujikompetensi">

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_pengambilan_keputusan" class="control-label">Nilai Pengambilan Keputusan <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_pengambilan_keputusan" id="nilai_pengambilan_keputusan" onkeyup="calcVal()" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_pengambilan_keputusan : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Pengambilan Keputusan</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_akhir_uji_kompetensi" class="control-label">Nilai Akhir Uji Kompetensi <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_akhir_uji_kompetensi" id="nilai_akhir_uji_kompetensi" onkeyup="calcVal()" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_akhir_uji_kompetensi : null ?>">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Akhir Uji Kompetensi</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                    <label for="saran_diklat_pengembangan" class="control-label">Saran Diklat Pengembangan <i class="required">*</i>
                                                                        </label>
                                                                        <textarea rows="5" class="form-control" name="saran_diklat_pengembangan"><?= $asn_talenta_penilaian ? $asn_talenta_penilaian->saran_diklat_pengembangan : null ?></textarea>
                                                                        <br>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                    <label for="kompetensi_yang_perlu_dikembangkan" class="control-label">Kompetensi Yang Perlu Dikembangkan <i class="required">*</i>
                                                                        </label>
                                                                        <textarea rows="5" class="form-control" name="kompetensi_yang_perlu_dikembangkan"><?= $asn_talenta_penilaian ? $asn_talenta_penilaian->kompetensi_yang_perlu_dikembangkan : null ?></textarea>
                                                                        <br>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                        <label for="klasifikasi_uji_potensi" class="control-label">Kategori Uji Kompetensi <i class="required">*</i>
                                                                        </label>
                                                                        <select class="form-control" aria-label="Klasifikasi Potensi" name="klasifikasi_uji_potensi">
                                                                        <option value="">-Kategori Uji Kompetensi-</option>
                                                                        <option value="Optimal" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_uji_potensi == 'Optimal' ? 'selected' : '') : '' ?>>Optimal</option>
                                                                        <option value="Cukup Optimal" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_uji_potensi == 'Cukup Optimal' ? 'selected' : '') : '' ?>>Cukup Optimal</option>
                                                                        <option value="Kurang Optimal" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_uji_potensi == 'Kurang Optimal' ? 'selected' : '') : '' ?>>Kurang Optimal</option>
                                                                        
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    
                                                    </table>
                                                </div>
                                                 

                                            </div>

                                            <div class="tab-pane" id="tab_5-2">
                                            <div style="overflow-y: scroll;height: 420px;">
                                            <div class="col-sm-12 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="kompetensi">
                                                <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                <?php 
                                                $jml_seminar    = 0;
                                                $jml_diklat     = 0;
                                                
                                                $i=0; foreach ($riwayat_kompetensi as $key => $value) {
                                                     $arr_seminar    = array('Pra Jabatan/Latsar', 'Diklat Kepemimpinan Tk.I/SEPATI', 'Diklat Kepemimpinan Tk.II/SPAMEN/SESPA/SESPANAS', 'Diklat Kepemimpinan Tk.III/SPAMA/SEPADYA', 'Diklat Kepemimpinan Tk.IV/ADUM/SEPALA', 'Diklat Penjenjangan', 'Fungsional', 'Teknis');
                                                     $arr_diklat     = array('Workshop/Lokakarya', 'Seminar', 'Magang', 'Kompetensi', 'Kursus');
                                         
                                                     if (in_array($value->jenis_pengembangan_kompetensi, $arr_diklat)) {
                                                         $jml_seminar    = $jml_seminar+1;
                                                     } else if (in_array($value->jenis_pengembangan_kompetensi, $arr_seminar)) {
                                                         $jml_diklat     = $jml_diklat+1;
                                                     }
                                         
                                                     if ($jml_diklat < 3) {
                                                         $bobot_diklat = round((0.6*10), 2);
                                                     } else if ($jml_diklat > 5) {
                                                         $bobot_diklat = 10;
                                                     } else {
                                                         $bobot_diklat = round((0.8*10), 2);
                                                     }
                                         
                                                     if ($jml_seminar < 5) {
                                                         $bobot_seminar = round((0.6*5), 2);
                                                     } else if ($jml_seminar > 10) {
                                                         $bobot_seminar = 5;
                                                     } else {
                                                         $bobot_seminar = round((0.8*5), 2);
                                                     }

                                                } ?>



                                                    <div class="small-box" style="background-color: #f7d0ed;">
        <div class="inner">
            <h3><?= $jml_diklat ?></h3>
            <p>Diklat</p>
        </div>
        <div class="icon">
            <i class="fa fa-graduation-cap"></i>
        </div>
        <a href="#" class="small-box-footer">
        Jumlah Diklat Diikuti <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div>    
                                                <div class="col-sm-3">
                                                <div class="small-box" style="background-color: #cfbaff;">
        <div class="inner">
            <h3><?= $jml_seminar ?></h3>
            <p>Seminar</p>
        </div>
        <div class="icon">
            <i class="fa fa-star"></i>
        </div>
        <a href="#" class="small-box-footer">
        Jumlah Seminar Diikuti <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div>    
                                                <div class="col-sm-3">
                                                <div class="small-box" style="background-color: #f7d0ed;">
        <div class="inner">
            <h3><?= isset($bobot_diklat) ? $bobot_diklat : 0 ?></h3>
            <p>Bobot Diklat</p>
        </div>
        <div class="icon">
            <i class="fa fa-graduation-cap"></i>
        </div>
        <a href="#" class="small-box-footer">
        Bobot Diklat <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div> 
                                                <div class="col-sm-3">   
                                                <div class="small-box" style="background-color: #cfbaff;">
        <div class="inner">
            <h3><?= isset($bobot_seminar) ? $bobot_seminar : 0 ?></h3>
            <p>Bobot Seminar</p>
        </div>
        <div class="icon">
            <i class="fa fa-star"></i>
        </div>
        <a href="#" class="small-box-footer">
        Bobot Seminar <i class="fa fa-arrow-circle-left"></i>
        </a>
    </div>
                                                </div>                    
                                                </div>
                                                <table class="table animate__animated animate__fadeIn table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Jenis</th>
            <th>Nama</th>
            <th>Lembaga Penyelenggara</th>
            <th>Waktu Pelaksanaan</th>
            <th>Jumlah Jam</th>
            <th>File Sertifikat</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $jml_seminar    = 0;
            $jml_diklat     = 0;
        ?>
        <?php $i=0; foreach ($riwayat_kompetensi as $key => $value) { $i++;?>
        <?php
            $arr_seminar    = array('Pra Jabatan/Latsar', 'Diklat Kepemimpinan Tk.I/SEPATI', 'Diklat Kepemimpinan Tk.II/SPAMEN/SESPA/SESPANAS', 'Diklat Kepemimpinan Tk.III/SPAMA/SEPADYA', 'Diklat Kepemimpinan Tk.IV/ADUM/SEPALA', 'Diklat Penjenjangan', 'Fungsional', 'Teknis');
            $arr_diklat     = array('Workshop/Lokakarya', 'Seminar', 'Magang', 'Kompetensi', 'Kursus');

            if (in_array($value->jenis_pengembangan_kompetensi, $arr_diklat)) {
                $jml_seminar    = $jml_seminar+1;
            } else if (in_array($value->jenis_pengembangan_kompetensi, $arr_seminar)) {
                $jml_diklat     = $jml_diklat+1;
            }

            if ($jml_diklat < 3) {
                $bobot_diklat = round((0.6*10), 2);
            } else if ($jml_diklat > 5) {
                $bobot_diklat = 10;
            } else {
                $bobot_diklat = round((0.8*10), 2);
            }

            if ($jml_seminar < 5) {
                $bobot_seminar = round((0.6*5), 2);
            } else if ($jml_seminar > 10) {
                $bobot_seminar = 5;
            } else {
                $bobot_seminar = round((0.8*5), 2);
            }
        ?>
        <tr>
            <td><?= $i ?></td>
            <td><?= $value->jenis_pengembangan_kompetensi ?></td>
            <td><?= $value->nama_pengembangan_kompetensi ?></td>
            <td><?= $value->penyelenggara ?></td>
            <td><?= date('d M Y', strtotime($value->tanggal_mulai)).'-'.date('d M Y', strtotime($value->tanggal_mulai)) ?></td>
            <td><?= $value->jumlah_jam ?></td>
            <td>
                <a href="<?= $value->kompetensi_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>


                                                </div>
                                                
                                            </div><br>
                                            
                                            </div>



                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_9-1" data-toggle="tab">SKP</a></li>
                                            <li><a href="#tab_9-2" data-toggle="tab">Kekurangan Jam Kerja</a></li>
                                            <li><a href="#tab_9-3" data-toggle="tab">Riwayat Hukdis</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_9-1">
                                            <div style="overflow-y: scroll;height: 420px;">
                                            <div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="skp">
                                                <table class="table animate__animated animate__fadeIn table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Jabatan</th>
            <th>Lokasi Kerja</th>
            <th>Tahun</th>
            <th>Nilai</th>
            <th>File Target</th>
            <th>File Realisasi</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=0; foreach ($riwayat_skp as $key => $value) { $i++;?>
        <tr>
            <td><?= $i ?></td>
            <td><?= $value->jabatan ?></td>
            <td><?= $value->unit_kerja ?></td>
            <td><span class="label label-info"><?= $value->tahun ?></td>
            <td>
                <span class="label label-primary"><?= $value->skp ?></span>
            </td>
            <td>
                <a href="<?= $value->target_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 30px;"></a>
            </td>
            <td>
                <a href="<?= $value->realisasi_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 30px;"></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
<div class="small-box" style="background-color: #cfbaff;">
    <div class="inner">
        <?php  
            $skp_terakhir   = $riwayat_skp[array_key_last($riwayat_skp)-1]->skp;

            if ($skp_terakhir < 60) {
                $predikat  = 'Dibawah ekspektasi';
            } elseif ($skp_terakhir < 80) {
                $predikat  = 'Sesuai ekspektasi';
            } else {
                $predikat  = 'Diatas Ekspektasi';
            }

            $bobot_skp    = (30/100)*($skp_terakhir*100/120);
        ?>
        <h3><?= $skp_terakhir ?></h3>
        <p><?= $predikat ?></p>
    </div>
    <div class="icon">
        <i class="fa fa-graduation-cap"></i>
    </div>
    <a href="#" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>
<div class="small-box" style="background-color: #f7d0ed;">
    <div class="inner">
        <h3><?= $bobot_skp ?> %</h3>
        <p>Bobot</p>
    </div>
    <div class="icon">
        <i class="fa fa-star"></i>
    </div>
    <a href="#" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>
</div>
                                            </div>
                                            </div>
                                            <div class="tab-pane" id="tab_9-2">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                                                        <div class="box-body" id="ujikompetensi">
                                                            <div class="form-group">
                                                                    <label for="jml_absen_setahun" class="control-label">Jumlah Kekurangan Jam Kerja Setahun <i class="required">*</i>
                                                                    </label>
                                                                    <div class="row">
                                                                        <div class="col-sm-6"><input class="form-control" type="number" name="jumlah_jam" placeholder="Jumlah Jam" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->jumlah_jam : null ?>"></div>
                                                                        <div class="col-sm-6"><input class="form-control" type="number" name="jumlah_menit" placeholder="Jumlah Menit" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->jumlah_menit : null ?>"></div>
                                                                    </div>
                                                                    <!-- <input type="time" class="form-control" name="jml_absen_setahun" id="jml_absen_setahun" placeholder=""> -->
                                                                    <small class="info help-block">
                                                                    <b>Input Jumlah Kekurangan Jam Kerja Setahun</b> Max Length : 20.</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_9-3">
                                            <div style="overflow-y: scroll;height: 420px;">
<div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
    <div class="box-body" id="hukumandisiplin">
    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Tingkat Hukuman Disiplin</th>
                                                                <th>Nomor SK</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $berat = 0;$sedang = 0; $ringan = 0;
                                                            ?>
                                                            <?php $i=0; foreach ($riwayat_hukdis as $key => $value) { $i++;?>
                                                            <?php 
                                                                if ($value->tingkat_hukuman_disiplin == 'Hukuman Disiplin Berat') {
                                                                    $berat++;
                                                                } elseif ($value->tingkat_hukuman_disiplin == 'Hukuman Disiplin Ringan') {
                                                                    $ringan++;
                                                                } elseif ($value->tingkat_hukuman_disiplin == 'Hukuman Disiplin Sedang') {
                                                                    $sedang++;
                                                                }
                                                            ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->tingkat_hukuman_disiplin ?></td>
                                                                <td><?= $value->nomor_sk ?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
    </div>
</div>
<div class="col-sm-4">
                                                    <div class="small-box" style="background-color: #cfbaff;">
                                                        <div class="inner">
                                                            <?php  
                                                                if (count($riwayat_hukdis) == 0) {
                                                                    $predikat  = 'Nihil';
                                                                    $nilai     = 100;
                                                                    $bobot_hukdis    = (30);
                                                                } else {
                                                                    if ($ringan > 0) {
                                                                        $nilai     = 80;
                                                                        $bobot_hukdis    = (30*0.8);
                                                                    }
                                                                    if ($sedang > 0) {
                                                                        $nilai     = 60;
                                                                        $bobot_hukdis    = (30*0.6);
                                                                    }
                                                                    if ($berat > 0) {
                                                                        $nilai     = 40;
                                                                        $bobot_hukdis    = (30*0.4);
                                                                    }
                                                                }

                                                                
                                                            ?>
                                                            <h3><?= $nilai ?></h3>
                                                            <p><?= $predikat ?></p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-graduation-cap"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Nilai Humanan Disiplin <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                    <div class="small-box" style="background-color: #f7d0ed;">
                                                        <div class="inner">
                                                            <h3><?= $bobot_hukdis ?> %</h3>
                                                            <p>Bobot</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Bobot Hukuman Disiplin <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                </div>
</div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            
                            </div>    

                        </div>

                    </div>
                    <input type="hidden" id="bobot_pendidikan" name="bobot_pendidikan" value="<?= $bobot_pendidikan ?>">
                    <input type="hidden" id="bobot_jabatan" name="bobot_jabatan" value="<?= $bobot_jabatan ?>">
                    <input type="hidden" id="bobot_diklat" name="bobot_diklat" value="<?= isset($bobot_diklat) ? $bobot_diklat : 0 ?>">
                    <input type="hidden" id="bobot_seminar" name="bobot_seminar" value="<?= isset($bobot_seminar) ? $bobot_seminar : 0 ?>">
                    <input type="hidden" id="bobot_skp" name="bobot_skp" value="<?= $bobot_skp ?>">
                    <input type="hidden" id="bobot_hukdis" name="bobot_hukdis" value="<?= $bobot_hukdis ?>">
                    <input type="hidden" id="kompetensi"name="kompetensi" value="">

                    <input type="hidden" id="nama" name="nama" value="<?= $detail_asn['nama'] ?>">
                    <input type="hidden" id="nip" name="nip" value="<?= $detail_asn['nip'] ?>">
                    <input type="hidden" id="golongan" name="golongan" value="<?= $detail_asn['golongan'] ?>">
                    <input type="hidden" id="pangkat" name="pangkat" value="<?= $detail_asn['pangkat'] ?>">
                    <input type="hidden" id="jabatan" name="jabatan" value="<?= $detail_asn['jabatan'] ?>">
                    <input type="hidden" id="eselon" name="eselon" value="<?= $detail_asn['eselon'] ?>">
                    <input type="hidden" id="lokasi_kerja" name="lokasi_kerja" value="<?= $detail_asn['lokasi_kerja'] ?>">
                    <input type="hidden" id="unit_kerja" name="unit_kerja" value="<?= $detail_asn['unit_kerja'] ?>">
                    <input type="hidden" id="opd" name="opd" value="<?= $detail_asn['opd'] ?>">
                    <input type="hidden" id="avatar" name="avatar" value="<?= $detail_asn['foto'] ?>">
                    <input type="hidden" name="nilai_skp" value="<?= $skp_terakhir ?>">
                    <input type="hidden" name="kelas_jabatan" value="<?= $kelas_jabatan ?>">
                    <input type="hidden" name="nilai_jabatan" value="<?= $nilai_jabatan ?>">


                        
                        
                                                    
                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>

                            <div class="custom-button-wrapper">

                                                        </div>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>