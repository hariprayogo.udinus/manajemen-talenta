

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asn Talenta        <small>Edit Asn Talenta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/asn_talenta'); ?>">Asn Talenta</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<style>
   /* .group-nip */
   .group-nip {

   }

   .group-nip .control-label {

   }

   .group-nip .col-sm-8 {

   }

   .group-nip .form-control {

   }

   .group-nip .help-block {

   }
   /* end .group-nip */




</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Asn Talenta</h3>
                            <h5 class="widget-user-desc">Edit Asn Talenta</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/asn_talenta/edit_save/'.$this->uri->segment(4)), [
                            'name' => 'form_asn_talenta',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_asn_talenta',
                            'method' => 'POST'
                        ]); ?>

                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>

                                                    
                        
                        <div class="form-group group-nip  ">
                                <label for="nip" class="col-sm-2 control-label">Nip                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nip" id="nip" placeholder="" value="<?= set_value('nip', $asn_talenta->nip); ?>">
                                    <small class="info help-block">
                                        <b>Input Nip</b> Max Length : 20.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-nama  ">
                                <label for="nama" class="col-sm-2 control-label">Nama                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="" value="<?= set_value('nama', $asn_talenta->nama); ?>">
                                    <small class="info help-block">
                                        <b>Input Nama</b> Max Length : 50.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-golongan  ">
                                <label for="golongan" class="col-sm-2 control-label">Golongan                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="golongan" id="golongan" placeholder="" value="<?= set_value('golongan', $asn_talenta->golongan); ?>">
                                    <small class="info help-block">
                                        <b>Input Golongan</b> Max Length : 20.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-pangkat  ">
                                <label for="pangkat" class="col-sm-2 control-label">Pangkat                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="pangkat" id="pangkat" placeholder="" value="<?= set_value('pangkat', $asn_talenta->pangkat); ?>">
                                    <small class="info help-block">
                                        <b>Input Pangkat</b> Max Length : 20.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-jabatan  ">
                                <label for="jabatan" class="col-sm-2 control-label">Jabatan                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="" value="<?= set_value('jabatan', $asn_talenta->jabatan); ?>">
                                    <small class="info help-block">
                                        <b>Input Jabatan</b> Max Length : 100.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-lokasi_kerja  ">
                                <label for="lokasi_kerja" class="col-sm-2 control-label">Lokasi Kerja                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="lokasi_kerja" id="lokasi_kerja" placeholder="" value="<?= set_value('lokasi_kerja', $asn_talenta->lokasi_kerja); ?>">
                                    <small class="info help-block">
                                        <b>Input Lokasi Kerja</b> Max Length : 100.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-unit_kerja  ">
                                <label for="unit_kerja" class="col-sm-2 control-label">Unit Kerja                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="unit_kerja" id="unit_kerja" placeholder="" value="<?= set_value('unit_kerja', $asn_talenta->unit_kerja); ?>">
                                    <small class="info help-block">
                                        <b>Input Unit Kerja</b> Max Length : 100.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-opd  ">
                                <label for="opd" class="col-sm-2 control-label">Opd                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="opd" id="opd" placeholder="" value="<?= set_value('opd', $asn_talenta->opd); ?>">
                                    <small class="info help-block">
                                        <b>Input Opd</b> Max Length : 100.</small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-avatar  ">
                                <label for="avatar" class="col-sm-2 control-label">Avatar                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="avatar" id="avatar" placeholder="" value="<?= set_value('avatar', $asn_talenta->avatar); ?>">
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group ">
                                <label for="status" class="col-sm-2 control-label">Status                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen chosen-select" name="status" id="status" data-placeholder="Select Status">
                                        <option value=""></option>
                                        <option <?= $asn_talenta->status == "aktif" ? 'selected' :''; ?> value="aktif">aktif</option>
                                        <option <?= $asn_talenta->status == "nonaktif" ? 'selected' :''; ?> value="nonaktif">nonaktif</option>
                                                                            </select>
                                    <small class="info help-block">
                                        </small>
                                </div>
                            </div>
                        
                        
                                                    
                        
                        <div class="form-group group-created_by">
                                <label for="created_by" class="col-sm-2 control-label">Created By                                    <i class="required">*</i>
                                    </label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen chosen-select-deselect" name="created_by" id="created_by" data-placeholder="Select Created By">
                                        <option value=""></option>
                                        <?php
                                        $conditions = [
                                            ];
                                        ?>
                                        <?php foreach (db_get_all_data('aauth_groups', $conditions) as $row): ?>
                                        <option <?= $row->id == $asn_talenta->created_by ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <small class="info help-block">
                                        <b>Input Created By</b> Max Length : 11.</small>
                                </div>
                            </div>


                        
                        
                                                    
                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>

                            <div class="custom-button-wrapper">

                                                        </div>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function() {
    window.event_submit_and_action = '';
            
    (function(){
    var nip = $('#nip');
   /* 
    nip.on('change', function() {});
    */
    
})()
      
      
      
      
        
        
    
    $('#btn_cancel').click(function() {
        swal({
                title: "Are you sure?",
                text: "the data that you have created will be in the exhaust!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = BASE_URL + 'administrator/asn_talenta';
                }
            });

        return false;
    }); /*end btn cancel*/

    $('.btn_save').click(function() {
        $('.message').fadeOut();
        
    var form_asn_talenta = $('#form_asn_talenta');
    var data_post = form_asn_talenta.serializeArray();
    var save_type = $(this).attr('data-stype');
    data_post.push({
        name: 'save_type',
        value: save_type
    });

    (function(){
    data_post.push({
        name : '_example',
        value : 'value_of_example',
    })
})()
      
      
    data_post.push({
        name: 'event_submit_and_action',
        value: window.event_submit_and_action
    });

    $('.loading').show();

    $.ajax({
            url: form_asn_talenta.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: data_post,
        })
        .done(function(res) {
            $('form').find('.form-group').removeClass('has-error');
            $('form').find('.error-input').remove();
            $('.steps li').removeClass('error');
            if (res.success) {
                var id = $('#asn_talenta_image_galery').find('li').attr('qq-file-id');
                if (save_type == 'back') {
                    window.location.href = res.redirect;
                    return;
                }

                $('.message').printMessage({
                    message: res.message
                });
                $('.message').fadeIn();
                $('.data_file_uuid').val('');

            } else {
                if (res.errors) {
                    parseErrorField(res.errors);
                }
                $('.message').printMessage({
                    message: res.message,
                    type: 'warning'
                });
            }

        })
        .fail(function() {
            $('.message').printMessage({
                message: 'Error save data',
                type: 'warning'
            });
        })
        .always(function() {
            $('.loading').hide();
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 2000);
        });

    return false;
    }); /*end btn save*/

    

    

    async function chain() {
            }

    chain();




    }); /*end doc ready*/
</script>