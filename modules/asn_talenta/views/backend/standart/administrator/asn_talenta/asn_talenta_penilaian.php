

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asn Talenta        <small>Edit Asn Talenta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/asn_talenta'); ?>">Asn Talenta</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<style>
   /* .group-nip */
   .group-nip {

   }

   .group-nip .control-label {

   }

   .group-nip .col-sm-8 {

   }

   .group-nip .form-control {

   }

   .group-nip .help-block {

   }
   /* end .group-nip */


   #catkompetensi {
     background-color: #bbd0ff;
    }

    #ujikompetensi {
     background-color: #ffd6ff;
    }

    .nav-tabs-custom li.active {
        border-top-color: #a38ed2 !important;
        border-bottom: none !important;
    }

</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Asn Talenta</h3>
                            <h5 class="widget-user-desc">Edit Asn Talenta</h5>
                        </div>
                        <?= form_open(base_url('administrator/asn_talenta/edit_savee/'.$this->uri->segment(4)), [
                            'name' => 'form_asn_talenta',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_asn_talenta',
                            'method' => 'POST'
                        ]); ?>

                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="fa fa-check-circle"></i> <b>Penilaian Talenta ASN</b></li>
                                <li class="active"><a href="#tab_1" data-toggle="tab">Sumbu Kompetensi</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Sumbu Kinerja</a></li>
                               
                            <br>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1-1" data-toggle="tab">Pendidikan</a></li>
                                            <li><a href="#tab_2-2" data-toggle="tab">Riwayat Jabatan</a></li>
                                            <li><a href="#tab_3-2" data-toggle="tab">CAT Potensi</a></li>
                                            <li><a href="#tab_4-2" data-toggle="tab">Uji Kompetensi</a></li>
                                            <li><a href="#tab_5-2" data-toggle="tab">Pengembangan Kompetensi</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1-1">
                                                <div class="col-sm-8" style="overflow-y: scroll;height: 420px;">
                                                    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Jenjang Pendidikan</th>
                                                                <th>Almamater</th>
                                                                <th>Ijazah</th>
                                                                <th>Transkrip Nilai</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i=0; foreach ($riwayat_pendidikan as $key => $value) { $i++;?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><span class="label label-info"><?= $value->tingkat_pendidikan ?></span><?= $value->pendidikan ?></td>
                                                                <td><?= $value->nama_sekolah ?></td>
                                                                <td>
                                                                    <a href="<?= $value->file_ijazah ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                                </td>
                                                                <td>
                                                                    <a href="<?= $value->file_transkrip ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="small-box" style="background-color: #cfbaff;">
                                                        <div class="inner">
                                                            <?php  
                                                                $jenjang_terakhir   = $riwayat_pendidikan[array_key_last($riwayat_pendidikan)]->tingkat_pendidikan;

                                                                if ($jenjang_terakhir == 'S-3') {
                                                                    $nilai_pendidikan  = 100;
                                                                } elseif ($jenjang_terakhir == 'S-2') {
                                                                    $nilai_pendidikan  = 90;
                                                                } elseif ($jenjang_terakhir == 'S-1') {
                                                                    $nilai_pendidikan  = 80;
                                                                } elseif ($jenjang_terakhir == 'Diploma IV') {
                                                                    $nilai_pendidikan  = 80;
                                                                } elseif ($jenjang_terakhir == 'Diploma III') {
                                                                    $nilai_pendidikan  = 70;
                                                                } else {
                                                                    $nilai_pendidikan  = 50;
                                                                }

                                                                $bobot_pendidikan    = (15/100)*$nilai_pendidikan;
                                                            ?>
                                                            <h3><?= $nilai_pendidikan ?></h3>
                                                            <p>Jenjang Pendidikan</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-graduation-cap"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        More info <i class="fa fa-arrow-circle-right"></i>
                                                        </a>
                                                    </div>
                                                    <div class="small-box" style="background-color: #f7d0ed;">
                                                        <div class="inner">
                                                            <h3><?= $bobot_pendidikan ?> %</h3>
                                                            <p>Bobot</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        More info <i class="fa fa-arrow-circle-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            <div class="tab-pane" id="tab_2-2">
                                                <div style="overflow-y: scroll;height: 420px;"><?= json_encode($riwayat_jabatan) ?>
                                                    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Jabatan</th>
                                                                <th>TMT SK</th>
                                                                <th>Lokasi Kerja</th>
                                                                <th>File SK</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i=0; foreach ($riwayat_jabatan as $key => $value) { $i++;?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->jabatan_baru ?></td>
                                                                <td><?= date('d M Y', strtotime($value->tmt_sk)) ?></td>
                                                                <td><?= $value->unit_kerja_baru ?></td>
                                                                <td>
                                                                    <a href="<?= $value->file_jabatan ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_3-2">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <table class="table">
                                                        <div class="col-sm-6 animate__animated animate__fadeIn animate__fast">
                                                            <div class="box-body" id="catkompetensi">
                                                                <div class="form-group">
                                                                    
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_kemampuan_berpikir" class="control-label">Nilai Kemampuan Berpikir <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_kemampuan_berpikir" id="nilai_kemampuan_berpikir" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Kemampuan Berpikir</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                    <label for="nilai_kepribadian" class="control-label">Nilai Kepribadian <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_kepribadian" id="nilai_kepribadian" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Kepribadian</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                    <label for="nilai_sikap_kerja" class="control-label">Nilai Sikap Kerja <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_sikap_kerja" id="nilai_sikap_kerja" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Sikap Kerja</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                               


                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 animate__animated animate__fadeIn animate__slow">
                                                            <div class="box-body" id="catkompetensi">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_akhir_individu" class="control-label">Nilai Akhir Individu <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_akhir_individu" id="nilai_akhir_individu" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Akhir Individu</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_akhir_individu" class="ontrol-label">Klasifikasi <i class="required">*</i>
                                                                        </label>
                                                                        <select class="form-control" aria-label="Klasifikasi Potensi" name="klasifikasi_potensi">
                                                                        <option>- Klasifikasi CAT Potensi -</option>
                                                                        <option value="Action">Action</option>
                                                                        <option value="Though">Though</option>
                                                                        <option value="People Oriented">People Oriented</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </table>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_4-2">  
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <table class="table">
                                                        <div class="col-sm-4 animate__animated animate__fadeIn animate__slow">
                                                            <div class="box-body" id="ujikompetensi">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_integritas" class="control-label">Nilai Integritas <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_integritas" id="nilai_integritas" placeholder="">
                                                                        <small class="info help-block">
                                                                        <b>Input Nilai Integritas</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_kerjasama" class="control-label">Nilai Kerjasama <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_kerjasama" id="nilai_kerjasama" placeholder="">
                                                                        <small class="info help-block">
                                                                        <b>Input Nilai Kerjasama</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">                                   
                                                                    <div class="col-sm-12">
                                                                    <label for="nilai_komunikasi_lisan" class="control-label">Nilai Komunikasi Lisan <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_komunikasi_lisan" id="nilai_komunikasi_lisan" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Komunikasi Lisan</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_mengelola_perubahan" class="control-label">  Nilai Mengelola Perubahan <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_mengelola_perubahan" id="nilai_mengelola_perubahan" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Mengelola Perubahan</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 animate__animated animate__fadeIn animate__slow">
                                                            <div class="box-body" id="ujikompetensi">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_orientasi_pada_hasil" class="control-label">Nilai Orientasi pada Hasil <i class="required">*</i>
                                                                            </label>
                                                                        <input type="text" class="form-control" name="nilai_orientasi_pada_hasil" id="nilai_orientasi_pada_hasil" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Orientasi pada Hasil</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_pelayanan_publik" class="control-label">Nilai Pelayanan Publik <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_pelayanan_publik" id="nilai_pelayanan_publik" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Pelayanan Publik</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_pengembangan_orang_lain" class="control-label">Nilai Pengembangan Orang Lain <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_pengembangan_orang_lain" id="nilai_pengembangan_orang_lain" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Pengembangan Orang Lain</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_perekat_bangsa" class="control-label">Nilai Perekat Bangsa <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_perekat_bangsa" id="nilai_perekat_bangsa" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Perekat Bangsa</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4 animate__animated animate__fadeIn animate__fast">
                                                            <div class="box-body" id="ujikompetensi">

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label for="nilai_akhir_uji_kompetensi" class="control-label">Nilai Akhir Uji Kompetensi <i class="required">*</i>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nilai_akhir_uji_kompetensi" id="nilai_akhir_uji_kompetensi" placeholder="">
                                                                        <small class="info help-block">
                                                                            <b>Input Nilai Akhir Uji Kompetensi</b> Max Length : 20.</small>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                    <label for="saran_pengembangan" class="control-label">Saran Pengembangan <i class="required">*</i>
                                                                        </label>
                                                                        <textarea rows="5" class="form-control" name="saran_pengembangan"></textarea>
                                                                        <br>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                        <label for="kategori_uji_kompetensi" class="control-label">Kategori Uji Kompetensi <i class="required">*</i>
                                                                        </label>
                                                                        <select class="form-control" aria-label="Klasifikasi Potensi" name="klasifikasi_potensi">
                                                                        <option>-Kategori Uji Kompetensi-</option>
                                                                        <option value="Optimal">Optimal</option>
                                                                        <option value="Kurang">Kurang</option>
                                                                        <option value="Cukup">Cukup</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    
                                                    </table>
                                                </div>
                                                 

                                            </div>

                                            <div class="tab-pane" id="tab_5-2">
                                                <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nama</th>
                                                            <th>Lembaga Penyelenggara</th>
                                                            <th>Waktu Pelaksanaan</th>
                                                            <th>Jumlah Jam</th>
                                                            <th>File Sertifikat</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i=0; foreach ($riwayat_kompetensi as $key => $value) { $i++;?>
                                                        <tr>
                                                            <td><?= $i ?></td>
                                                            <td><?= $value->nama_pengembangan_kompetensi ?></td>
                                                            <td><?= $value->penyelenggara ?></td>
                                                            <td><?= date('d M Y', strtotime($value->tanggal_mulai)).'-'.date('d M Y', strtotime($value->tanggal_mulai)) ?></td>
                                                            <td><?= $value->jumlah_jam ?></td>
                                                            <td>
                                                                <a href="<?= $value->kompetensi_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 50px;"></a>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_9-1" data-toggle="tab">SKP</a></li>
                                            <li><a href="#tab_9-2" data-toggle="tab">Absen</a></li>
                                            <li><a href="#tab_9-3" data-toggle="tab">Riwayat Hukdis</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_9-1">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Jabatan</th>
                                                                <th>Lokasi Kerja</th>
                                                                <th>Tahun</th>
                                                                <th>Nilai</th>
                                                                <th>File Target</th>
                                                                <th>File Realisasi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i=0; foreach ($riwayat_skp as $key => $value) { $i++;?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->jabatan ?></td>
                                                                <td><?= $value->unit_kerja ?></td>
                                                                <td><span class="label label-info"><?= $value->tahun ?></td>
                                                                <td>
                                                                    <span class="label label-primary"><?= $value->skp ?></span>
                                                                </td>
                                                                <td>
                                                                    <a href="<?= $value->target_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 30px;"></a>
                                                                </td>
                                                                <td>
                                                                    <a href="<?= $value->realisasi_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 30px;"></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_9-2">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <div class="col-sm-3 animate__animated animate__fadeIn animate__slow">
                                                        <div class="box-body" id="ujikompetensi">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <label for="jml_absen_setahun" class="control-label">Jumlah Absen Setahun <i class="required">*</i>
                                                                    </label>
                                                                    <input type="time" class="form-control" name="jml_absen_setahun" id="jml_absen_setahun" placeholder="">
                                                                    <small class="info help-block">
                                                                    <b>Input Jumlah Absen Setahun</b> Max Length : 20.</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_9-3">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Tingkat Hukuman Disiplin</th>
                                                                <th>Nomor SK</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i=0; foreach ($riwayat_hukdis as $key => $value) { $i++;?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->tingkat_hukuman_disiplin ?></td>
                                                                <td><?= $value->nomor_sk ?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            
                            </div>    

                        </div>

                    </div>


                        
                        
                                                    
                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>

                            <div class="custom-button-wrapper">

                                                        </div>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function() {
    window.event_submit_and_action = '';
            
    (function(){
    var nip = $('#nip');
   /* 
    nip.on('change', function() {});
    */
    
})()
      
      
      
      
        
        
    
    $('#btn_cancel').click(function() {
        swal({
                title: "Are you sure?",
                text: "the data that you have created will be in the exhaust!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = BASE_URL + 'administrator/asn_talenta';
                }
            });

        return false;
    }); /*end btn cancel*/

    $('.btn_save').click(function() {
        $('.message').fadeOut();
        
    var form_asn_talenta = $('#form_asn_talenta');
    var data_post = form_asn_talenta.serializeArray();
    var save_type = $(this).attr('data-stype');
    data_post.push({
        name: 'save_type',
        value: save_type
    });

    (function(){
    data_post.push({
        name : '_example',
        value : 'value_of_example',
    })
})()
      
      
    data_post.push({
        name: 'event_submit_and_action',
        value: window.event_submit_and_action
    });

    $('.loading').show();

    $.ajax({
            url: form_asn_talenta.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: data_post,
        })
        .done(function(res) {
            $('form').find('.form-group').removeClass('has-error');
            $('form').find('.error-input').remove();
            $('.steps li').removeClass('error');
            if (res.success) {
                var id = $('#asn_talenta_image_galery').find('li').attr('qq-file-id');
                if (save_type == 'back') {
                    window.location.href = res.redirect;
                    return;
                }

                $('.message').printMessage({
                    message: res.message
                });
                $('.message').fadeIn();
                $('.data_file_uuid').val('');

            } else {
                if (res.errors) {
                    parseErrorField(res.errors);
                }
                $('.message').printMessage({
                    message: res.message,
                    type: 'warning'
                });
            }

        })
        .fail(function() {
            $('.message').printMessage({
                message: 'Error save data',
                type: 'warning'
            });
        })
        .always(function() {
            $('.loading').hide();
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 2000);
        });

    return false;
    }); /*end btn save*/

    

    

    async function chain() {
            }

    chain();




    }); /*end doc ready*/
</script>