

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asn Talenta        <small>Edit Asn Talenta</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/asn_talenta'); ?>">Asn Talenta</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<style>
   /* .group-nip */
   .group-nip {

   }

   .group-nip .control-label {

   }

   .group-nip .col-sm-8 {

   }

   .group-nip .form-control {

   }

   .group-nip .help-block {

   }
   /* end .group-nip */


   #catkompetensi {
     background-color: #bbd0ff;
    }

    #ujikompetensi {
     background-color: #ffd6ff;
    }

    .nav-tabs-custom li.active {
        border-top-color: #a38ed2 !important;
        border-bottom: none !important;
    }

  #justify {
  width: 100%;
  margin-top: 5px;
  margin-left: 5px;
}

</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Asn Talenta</h3>
                            <h5 class="widget-user-desc">Edit Asn Talenta</h5>
                        </div>
                        <?= form_open(base_url('administrator/asn_talenta/penilaian_save_eselon/'.$this->uri->segment(4)), [
                            'name' => 'form_asn_talenta',
                            'class' => 'form-horizontal form-step',
                            'id' => 'form_asn_talenta',
                            'method' => 'POST'
                        ]); ?>

                        <?php
                        $user_groups = $this->model_group->get_user_group_ids();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-widget widget-user-2">
                <div class="widget-user-header" style="background-color: transparent;background-image: linear-gradient(to right, #a38ed2 0, #f9c1eb 75%);    background-repeat: repeat-x;">
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?= $detail_asn['foto'] ?>" alt="User Avatar">
                    </div>

                    <h3 class="widget-user-username"><b><?= ucwords(strtolower($detail_asn['nama'])) ?></b></h3>
                    <h5 class="widget-user-desc"><?= ucwords(strtolower($detail_asn['jabatan'])) ?></h5>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-justified" style = "margin:10px">
                         <li><b><?= $detail_asn['lokasi_kerja'] ?></b></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="fa fa-check-circle"></i> <b>Penilaian Talenta ASN</b></li>
                                <li class="active"><a href="#tab_1" data-toggle="tab">Sumbu Kompetensi</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Sumbu Kinerja</a></li>
                               
                            <br>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div style="overflow-y: scroll;height: 420px;">
                                        <table class="table">
                                            <div class="col-sm-6 animate__animated animate__fadeIn animate__fast">
                                                <div class="box-body" id="catkompetensi">
                                                    <div class="form-group">
                                                        
                                                        <div class="col-sm-12">
                                                            <label for="nilai_seleksi_kompetensi" class="control-label">Nilai Tes Kompetensi <i class="required">*</i>
                                                            </label>
                                                            <input type="number" class="form-control" name="nilai_seleksi_kompetensi" id="nilai_seleksi_kompetensi" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_seleksi_kompetensi : null ?>" min="0" max="999.999" step=".001">
                                                            <small class="info help-block">
                                                                <b>Input Nilai Tes Kompetensi</b> </small>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                        <label for="uji_gagasan_tertulis" class="control-label">Uji Gagasan Tertulis <i class="required">*</i>
                                                            </label>
                                                            <input type="number" class="form-control" name="uji_gagasan_tertulis" id="uji_gagasan_tertulis" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->uji_gagasan_tertulis : null ?>"  min="0" max="999.999" step=".001">
                                                            <small class="info help-block">
                                                                <b>Input Nilai Uji Gagasan Tertulis</b> </small>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <div class="col-sm-12">
                                                        <label for="uji_gagasan_lisan" class="control-label">Nilai Uji Gagasan Lisan <i class="required">*</i>
                                                            </label>
                                                            <input type="number" class="form-control" name="uji_gagasan_lisan" id="uji_gagasan_lisan" placeholder="" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->uji_gagasan_lisan : null ?>" min="0" max="999.999" step=".001">
                                                            <small class="info help-block">
                                                                <b>Input Nilai Uji Gagasan Lisan</b></small>
                                                        </div>
                                                    </div>
                                                   


                                                </div>
                                            </div>
                                            <div class="col-sm-6 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="catkompetensi">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="nilai_rekam_jejak" class="control-label">Nilai Rekam Jejak <i class="required">*</i>
                                                            </label>
                                                            <input type="number" class="form-control" name="nilai_rekam_jejak" id="nilai_rekam_jejak" placeholder="" onkeyup="calcVal()" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->nilai_rekam_jejak : null ?>"  min="0" max="999.999" step=".001">
                                                            <small class="info help-block">
                                                                <b>Input Nilai Rekam Jejak</b> Max Length : 20.</small>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="nilai_akhir_individu" class="ontrol-label">Klasifikasi <i class="required">*</i>
                                                            </label>
                                                            <select class="form-control" aria-label="Klasifikasi Potensi" name="klasifikasi_seleksi_kompetensi">
                                                            <option value="">- Klasifikasi Seleksi Kompetensi -</option>
                                                            <option value="Memenuhi Syarat" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_seleksi_kompetensi == 'Memenuhi Syarat' ? 'selected' : '') : '' ?>>Memenuhi Syarat</option>
                                                            <option value="Masih Memenuhi Syarat" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_seleksi_kompetensi == 'Masih Memenuhi Syarat' ? 'selected' : '') : '' ?>>Masih Memenuhi Syarat</option>
                                                            <option value="Kurang Memenuhi Syarat" <?= $asn_talenta_penilaian ? ($asn_talenta_penilaian->klasifikasi_seleksi_kompetensi == 'Kurang Memenuhi Syarat' ? 'selected' : '') : '' ?>>Kurang Memenuhi Syarat</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_9-1" data-toggle="tab">SKP</a></li>
                                            <li><a href="#tab_9-2" data-toggle="tab">Kekurangan Jam Kerja</a></li>
                                            <li><a href="#tab_9-3" data-toggle="tab">Riwayat Hukdis</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_9-1">
                                            <div style="overflow-y: scroll;height: 420px;">
                                            <div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                                                <div class="box-body" id="skp">
                                                <table class="table animate__animated animate__fadeIn table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Jabatan</th>
            <th>Lokasi Kerja</th>
            <th>Tahun</th>
            <th>Nilai</th>
            <th>File Target</th>
            <th>File Realisasi</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=0; foreach ($riwayat_skp as $key => $value) { $i++;?>
        <tr>
            <td><?= $i ?></td>
            <td><?= $value->jabatan ?></td>
            <td><?= $value->unit_kerja ?></td>
            <td><span class="label label-info"><?= $value->tahun ?></td>
            <td>
                <span class="label label-primary"><?= $value->skp ?></span>
            </td>
            <td>
                <a href="<?= $value->target_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 30px;"></a>
            </td>
            <td>
                <a href="<?= $value->realisasi_file ?>" target="__blank"><img src="https://icon-library.com/images/pdf-download-icon-transparent-background/pdf-download-icon-transparent-background-16.jpg" data-toggle="tooltip" data-placement="top" title="Ijazah" style="max-height: 30px;"></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
<div class="small-box" style="background-color: #cfbaff;">
    <div class="inner">
        <?php  
            $skp_terakhir   = $riwayat_skp[array_key_last($riwayat_skp)-1]->skp;

            if ($skp_terakhir < 60) {
                $predikat  = 'Dibawah ekspektasi';
            } elseif ($skp_terakhir < 80) {
                $predikat  = 'Sesuai ekspektasi';
            } else {
                $predikat  = 'Diatas Ekspektasi';
            }

            $bobot_skp    = (30/100)*($skp_terakhir*100/120);
        ?>
        <h3><?= $skp_terakhir ?></h3>
        <p><?= $predikat ?></p>
    </div>
    <div class="icon">
        <i class="fa fa-graduation-cap"></i>
    </div>
    <a href="#" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>
<div class="small-box" style="background-color: #f7d0ed;">
    <div class="inner">
        <h3><?= $bobot_skp ?> %</h3>
        <p>Bobot</p>
    </div>
    <div class="icon">
        <i class="fa fa-star"></i>
    </div>
    <a href="#" class="small-box-footer">
    More info <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>
</div>
                                            </div>
                                            </div>
                                            <div class="tab-pane" id="tab_9-2">
                                                <div style="overflow-y: scroll;height: 420px;">
                                                    <div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
                                                        <div class="box-body" id="ujikompetensi">
                                                            <div class="form-group">
                                                                    <label for="jml_absen_setahun" class="control-label">Jumlah Kekurangan Jam Kerja Setahun <i class="required">*</i>
                                                                    </label>
                                                                    <div class="row">
                                                                        <div class="col-sm-6"><input class="form-control" type="number" name="jumlah_jam" placeholder="Jumlah Jam" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->jumlah_jam : null ?>"></div>
                                                                        <div class="col-sm-6"><input class="form-control" type="number" name="jumlah_menit" placeholder="Jumlah Menit" value="<?= $asn_talenta_penilaian ? $asn_talenta_penilaian->jumlah_menit : null ?>"></div>
                                                                    </div>
                                                                    <!-- <input type="time" class="form-control" name="jml_absen_setahun" id="jml_absen_setahun" placeholder=""> -->
                                                                    <small class="info help-block">
                                                                    <b>Input Jumlah Kekurangan Jam Kerja Setahun</b> Max Length : 20.</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane" id="tab_9-3">
                                            <div style="overflow-y: scroll;height: 420px;">
<div class="col-sm-8 animate__animated animate__fadeIn animate__slow">
    <div class="box-body" id="hukumandisiplin">
    <table class="table animate__animated animate__fadeIn table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Tingkat Hukuman Disiplin</th>
                                                                <th>Nomor SK</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $berat = 0;$sedang = 0; $ringan = 0;
                                                            ?>
                                                            <?php $i=0; foreach ($riwayat_hukdis as $key => $value) { $i++;?>
                                                            <?php 
                                                                if ($value->tingkat_hukuman_disiplin == 'Hukuman Disiplin Berat') {
                                                                    $berat++;
                                                                } elseif ($value->tingkat_hukuman_disiplin == 'Hukuman Disiplin Ringan') {
                                                                    $ringan++;
                                                                } elseif ($value->tingkat_hukuman_disiplin == 'Hukuman Disiplin Sedang') {
                                                                    $sedang++;
                                                                }
                                                            ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value->tingkat_hukuman_disiplin ?></td>
                                                                <td><?= $value->nomor_sk ?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
    </div>
</div>
<div class="col-sm-4">
                                                    <div class="small-box" style="background-color: #cfbaff;">
                                                        <div class="inner">
                                                            <?php  
                                                                if (count($riwayat_hukdis) == 0) {
                                                                    $predikat  = 'Nihil';
                                                                    $nilai     = 100;
                                                                    $bobot_hukdis    = (30);
                                                                } else {
                                                                    if ($ringan > 0) {
                                                                        $nilai     = 80;
                                                                        $bobot_hukdis    = (30*0.8);
                                                                    }
                                                                    if ($sedang > 0) {
                                                                        $nilai     = 60;
                                                                        $bobot_hukdis    = (30*0.6);
                                                                    }
                                                                    if ($berat > 0) {
                                                                        $nilai     = 40;
                                                                        $bobot_hukdis    = (30*0.4);
                                                                    }
                                                                }

                                                                
                                                            ?>
                                                            <h3><?= $nilai ?></h3>
                                                            <p><?= $predikat ?></p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-graduation-cap"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Nilai Humanan Disiplin <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                    <div class="small-box" style="background-color: #f7d0ed;">
                                                        <div class="inner">
                                                            <h3><?= $bobot_hukdis ?> %</h3>
                                                            <p>Bobot</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                        <a href="#" class="small-box-footer">
                                                        Bobot Hukuman Disiplin <i class="fa fa-arrow-circle-left"></i>
                                                        </a>
                                                    </div>
                                                </div>
</div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            
                            </div>    

                        </div>

                    </div>
                    

                    <input type="hidden" id="bobot_skp" name="bobot_skp" value="<?= $bobot_skp ?>">
                    <input type="hidden" id="bobot_hukdis" name="bobot_hukdis" value="<?= $bobot_hukdis ?>">

                    <input type="hidden" id="nama" name="nama" value="<?= $detail_asn['nama'] ?>">
                    <input type="hidden" id="nip" name="nip" value="<?= $detail_asn['nip'] ?>">
                    <input type="hidden" id="golongan" name="golongan" value="<?= $detail_asn['golongan'] ?>">
                    <input type="hidden" id="pangkat" name="pangkat" value="<?= $detail_asn['pangkat'] ?>">
                    <input type="hidden" id="jabatan" name="jabatan" value="<?= $detail_asn['jabatan'] ?>">
                    <input type="hidden" id="eselon" name="eselon" value="<?= $detail_asn['eselon'] ?>">
                    <input type="hidden" id="lokasi_kerja" name="lokasi_kerja" value="<?= $detail_asn['lokasi_kerja'] ?>">
                    <input type="hidden" id="unit_kerja" name="unit_kerja" value="<?= $detail_asn['unit_kerja'] ?>">
                    <input type="hidden" id="opd" name="opd" value="<?= $detail_asn['opd'] ?>">
                    <input type="hidden" id="avatar" name="avatar" value="<?= $detail_asn['foto'] ?>">


                        
                        
                                                    
                                                    <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>

                            <div class="custom-button-wrapper">

                                                        </div>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                <?= form_close(); ?>
                        </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>