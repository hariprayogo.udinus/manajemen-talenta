<?php 
public function get_detail_by_nip(){
    $nip_data 	= $this->input->get('nip');
    $is_atasan_plt 	= $this->input->get('is_atasan_plt');

    $post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 

    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, 'http://103.101.52.17:9000/oauth/token'); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
    curl_setopt($ch, CURLOPT_POST,           1 ); 
    curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
    $result=curl_exec ($ch); 
     
    curl_close($ch); 

    $obj = json_decode($result);
    $token_type=$obj->token_type;
    $access_token= $obj->access_token;
    $tokennya = $obj->token_type.' '.$obj->access_token;
    $headers2 = array();
    $headers2[] = 'Authorization: Bearer '.$access_token;
    //$headers2[] = 'Authorization: Bearer '.$obj->access_token;
    #echo $access_token;


    $curl2 = curl_init();

    curl_setopt($curl2, CURLOPT_URL, "http://103.101.52.17:9000/api/pegawai?keyword=$nip_data");


    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers2);
        
    $result22 = curl_exec($curl2);
    curl_close($curl2);


    $profile2 = json_decode($result22, TRUE);

    $groups         = $this->session->userdata('groups');
    $opd        	= $this->session->userdata('opd');
    $username       = $this->session->userdata('username');
    $lokasi_kerja   = $this->session->userdata('lokasi_kerja');
    $unit_kerja     = $this->session->userdata('unit_kerja');

    $arr 	= array(
                        'golongan' => NULL,
                        'jabatan' => NULL,
                        'lokasi_kerja' => NULL,
                        'nama' => NULL,
                        'nip' => NULL,
                        'no_hp' => NULL,
                        'opd' => NULL,
                        'pangkat' => NULL,
                        'unit_kerja' => NULL
                      );

    if ($is_atasan_plt == 'Y') {
        $user = $this->db->where('username', $nip_data)
                         ->get('aauth_users')
                         ->row();

        $user_plt = $this->db->where('id_user', $user->id)
                                    ->get('user_plt')
                                    ->row();

        $array 	= array(
                        'golongan' => $profile2['data'][0]['golongan'],
                        'jabatan' => $user_plt->jabatan,
                        'lokasi_kerja' => $user_plt->lokasi_kerja,
                        'nama' => $profile2['data'][0]['nama'],
                        'nip' => $profile2['data'][0]['nip'],
                        'no_hp' => $profile2['data'][0]['no_hp'],
                        'opd' => $user_plt->opd,
                        'pangkat' => $profile2['data'][0]['pangkat'],
                        'unit_kerja' => $user_plt->unit_kerja
                      );
    } else {
        if ($profile2['data'] != []) {
            $array 	= $profile2['data'][0];
        } else {
            $array = $arr;
        }
        
    }
    

    echo json_encode($array);
}

?>

<script type="text/javascript">
    function isi_otomatis(){
        var nip = $("#nip_atasan").val();
        var is_atasan_plt = $('input[name="is_atasan_plt"]:checked').val();
        if (nip.length > 17) {
          $.ajax({
              url: '<?= base_url('administrator/atasan_langsung/get_detail_by_nip') ?>',
              data:"nip="+nip+"&is_atasan_plt="+is_atasan_plt,
          }).done(function(data) {
              var json = data,
              obj = JSON.parse(json);
              
              $('#nama_atasan').val(obj.nama);
              $('#jabatan_atasan').val(obj.jabatan);
              $('#lokasi_kerja_atasan').val(obj.lokasi_kerja);
              $('#pangkat_atasan').val(obj.pangkat);
              $('#golongan_atasan').val(obj.golongan);
              $('#nip_verifikator').val(obj.nip);
              $('#nama_verifikator').val(obj.nama);
              $('#jabatan_verifikator').val(obj.jabatan);
              $('#lokasi_kerja_verifikator').val(obj.lokasi_kerja);
              $('#pangkat_verifikator').val(obj.pangkat);
              $('#golongan_verifikator').val(obj.golongan);
          })
        } else {
            if (nip == 'walikota') {
              $.ajax({
                  url: '<?= base_url('administrator/atasan_langsung/get_walikota') ?>',
                  data:"nip="+nip ,
              }).done(function(data) {
                  var json = data,
                  obj = JSON.parse(json);
                  $('#nama_atasan').val(obj.nama);
                  $('#jabatan_atasan').val(obj.jabatan);
                  $('#lokasi_kerja_atasan').val(obj.lokasi_kerja);
                  $('#pangkat_atasan').val('-');
                  $('#golongan_atasan').val('-');
                  $('#nip_verifikator').val(obj.nip);
                  $('#nama_verifikator').val(obj.nama);
                  $('#jabatan_verifikator').val(obj.jabatan);
                  $('#lokasi_kerja_verifikator').val(obj.lokasi_kerja);
                  $('#pangkat_verifikator').val('-');
                  $('#golongan_verifikator').val('-');
              })
            }
            $('#nama_atasan').val(null);
            $('#jabatan_atasan').val(null);
            $('#lokasi_kerja_atasan').val(null);
            $('#pangkat_atasan').val(null);
            $('#golongan_atasan').val(null);
            $('#nip_verifikator').val(null);
            $('#nama_verifikator').val(null);
            $('#jabatan_verifikator').val(null);
            $('#lokasi_kerja_verifikator').val(null);
            $('#pangkat_verifikator').val(null);
            $('#golongan_verifikator').val(null);
        }
    }

    function isi_otomatis_verifikator(){
        var nip = $("#nip_verifikator").val();
        if (nip.length > 17) {
          $.ajax({
              url: '<?= base_url('administrator/atasan_langsung/get_detail_by_nip') ?>',
              data:"nip="+nip ,
          }).done(function(data) {
              var json = data,
              obj = JSON.parse(json);
              $('#nama_verifikator').val(obj.nama);
              $('#jabatan_verifikator').val(obj.jabatan);
              $('#lokasi_kerja_verifikator').val(obj.lokasi_kerja);
              $('#pangkat_verifikator').val(obj.pangkat);
              $('#golongan_verifikator').val(obj.golongan);
          })
        }
    }

    $("input").keyup(function(){
   //do
});



public function get_detail_by_nip(){
		$nip_data 	= $this->input->get('nip');
		$is_atasan_plt 	= $this->input->get('is_atasan_plt');
	
		$post = array('grant_type' => 'password','client_id' => '8','client_secret' => 'QajTJfWDaVApl2Z90B1Mnnm8z0J2s79tmUM38Gnq','username' => 'diskominfo_kepala_opd','password' => 'semaranghebat'); 
	
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, 'http://103.101.52.17:9000/oauth/token'); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
		curl_setopt($ch, CURLOPT_POST,           1 ); 
		curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($post));  
		$result=curl_exec ($ch); 
		 
		curl_close($ch); 
	
		$obj = json_decode($result);
		$token_type=$obj->token_type;
		$access_token= $obj->access_token;
		$tokennya = $obj->token_type.' '.$obj->access_token;
		$headers2 = array();
		$headers2[] = 'Authorization: Bearer '.$access_token;
		//$headers2[] = 'Authorization: Bearer '.$obj->access_token;
		#echo $access_token;
	
	
		$curl2 = curl_init();
	
		curl_setopt($curl2, CURLOPT_URL, "http://103.101.52.17:9000/api/pegawai?keyword=$nip_data");
	
	
		curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers2);
			
		$result22 = curl_exec($curl2);
		curl_close($curl2);
	
	
		$profile2 = json_decode($result22, TRUE);
	
		// $nip        		= $this->session->userdata('nip');
		// $nama 				= $this->session->userdata('nama');
		// $opd        		= $this->session->userdata('opd');
		// $unit_kerja        	= $this->session->userdata('unit_kerja');
		// $lokasi_kerja       = $this->session->userdata('lokasi_kerja');
		// $jabatan        	= $this->session->userdata('jabatan');
		// $pangkat        	= $this->session->userdata('pangkat');
		// $golongan        	= $this->session->userdata('golongan');
		// $no_hp        		= $this->session->userdata('no_hp');
		// $avatar        		= $this->session->userdata('avatar');
		// $status        		= $this->session->userdata('status');

	
		$arr 	= array(
							'golongan' 		=> NULL,
							'jabatan' 		=> NULL,
							'lokasi_kerja'	=> NULL,
							'nama' 			=> NULL,
							'nip' 			=> NULL,
							'no_hp' 		=> NULL,
							'opd' 			=> NULL,
							'pangkat' 		=> NULL,
							'unit_kerja' 	=> NULL,
							'avatar' 		=> NULL,
							'status' 		=> NULL
						  );
	
			if ($profile2['data'] != []) {
				$array 	= $profile2['data'][0];
			} else {
				$array = $arr;
			}

	
		echo json_encode($array);
	}
</script>

